import React from 'react';
import {
    Card,
    CardHeader,
    ListGroup,
    ListGroupItem,
    Container,
    Alert,
    Row,
    Col
} from "shards-react";
import Baby from "../rest/Baby";
import Error from "../rest/Error";
// import "node_modules/video-react/dist/video-react.css"; // import css

export default class Video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: localStorage.getItem("video_ip"),
            // src2: 'http://192.168.43.68:8080/shot.jpg',
            id: Date.now(),
            baby_id: localStorage.getItem("baby_id"),
            notifications: null,
            notification_class: "mb-0 bg-danger",
            baby: { "birth_date": "" }

        }
        this.babyQuery = this.babyQuery.bind(this);
        this.baby_obj = new Baby();
        this.error_valuer = new Error();
        this.dismiss_notification = this.dismiss_notification.bind(this);

    }

    handleImageLoad = () => {

        this.setState({ id: Date.now() });
        console.error('image loaded');
    };
    componentDidMount() {
        this.babyQuery();
    }

    async babyQuery() {
        console.log(this.state.baby_id)
        const response_data = await this.baby_obj.get_baby(this.state.baby_id)

        const status = response_data[0];
        const data = response_data[1];


        let error = this.error_valuer.evaluate_status(status, response_data['message']);

        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return false
        }
        
        const baby = JSON.parse(data['data']);

        if (baby.photo === null)
            baby.photo = require("../images/avatars/default.png")
        this.setState({
            baby: baby
        })

        return true;

    }


    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }


    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }

    render() {
        return (
            <>
                {this.state.notifications != null && this.renderNotifications()}
                {!JSON.parse(localStorage.getItem("dynamic_id")) &&
                    <Container style={{display: 'flex', justifyContent: 'center'}} fluid className="main-content-container px-4 justify-content-center align-items-center" >
                        <Row noGutters className="page-header py-4">
                            <Col lg="12" >
                                <Card small className="mb-4 pt-3" >
                                    <CardHeader className="border-bottom text-center"  >
                                        <div className="mb-3 mx-auto">
                                            <img
                                                className="rounded-circle"
                                                src={this.state.baby.photo}
                                                alt={this.state.name}
                                                width="110"
                                                height="110"
                                            />
                                        </div>
        <h4 className="mb-0"><a href="/baby/profile" id="" onClick="">{this.state.baby.name} {this.state.baby.sex}</a></h4>
                                    </CardHeader>
                                    <ListGroup flush>
                                        <ListGroupItem className="px-4">
                                            <strong className="text-muted d-block mb-2">
                                                Data de Nascimento
                      </strong>

                                            <span>{this.state.baby.birth_date.replace('T', ' ').replace('Z', ' ')}</span>

                                        </ListGroupItem>
                                    </ListGroup>
                                </Card>
                                <img
                                    onLoad={this.handleImageLoad}
                                    src={this.state.src}
                                    width="1000px"
                                    height="600px"
                                />
                            </Col>
                        </Row>


                    </Container>
                }
                {JSON.parse(localStorage.getItem("dynamic_id")) &&
                    <img
                        onLoad={this.handleImageLoad}
                        src={`${this.state.src}?${this.state.id}`}
                    />
                }



            </>
        )
    }
}