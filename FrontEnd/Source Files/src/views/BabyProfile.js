import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";

import { url } from "../rest/Const";
import BabyDetails from "../components/component-baby-profile/BabyDetails";
import BabyParentsDetails from "../components/component-baby-profile/BabyParentsDetails";
import BabyTasksDetails from "../components/component-baby-profile/BabyTasksDetails";
import BabyStuffDetails from "../components/component-baby-profile/BabyStuffDetails";
import AddNewTask from "./AddNewTask";

import {
  Container,
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button
} from "shards-react";


export default class BabyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      id: localStorage.getItem('baby_id_prof'),
      add_task: false,
      botton_message : "Adicionar Tarefa"
    }
    this.handleClick = this.handleClick.bind(this)
    this.deleteBaby = this.deleteBaby.bind(this)
  }

  handleClick(){
    if(this.state.add_task){
      console.log("hide")
      this.setState({add_task: false, botton_message : "Adicionar Tarefa"})
    }else{
      console.log("show")
      this.setState({add_task: true,  botton_message : "Cancelar"})
    }
  }

  deleteBaby() {
    return fetch(url + '/remove_person/' + this.state.id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      }
    })
      .then(response => {
        this.status = response.status;
        
        return response.json()
      })
      .then(() => {
          window.location.href = '/'
      })
  }


  render() {
    return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle title="Perfil do Bébé" md="12" className="ml-sm-auto mr-sm-auto" />
        </Row>
        <Row>
          <Col lg="4">
            <BabyDetails id={this.state.id}/>
            <Button  size="sm" className="mb-2" onClick={this.handleClick}>
              <i className="material-icons mr-1">assignment</i> {this.state.botton_message}
            </Button>
            <Button  size="sm" className="mb-2" onClick={this.deleteBaby} style={{backgroundColor:'red', marginLeft: '20px', border: '0'}}>
              <i className="material-icons mr-1">restore_from_trash</i> Remove
            </Button>
            <AddNewTask show={this.state.add_task}/>
            
          </Col>
          <Col lg="8">
            <BabyStuffDetails id={this.state.id} />
            <BabyParentsDetails id={this.state.id} />
            <BabyTasksDetails id={this.state.id} />
          </Col>
        </Row>
      </Container>

    )
  }
}
