import React from 'react';
import {
    Card,
    CardHeader,
    ListGroup,
    ListGroupItem,
    Container,
    Alert,
    Row,
    Col
} from "shards-react";
import Baby from "../rest/Baby";
import Error from "../rest/Error";
import UsersOverview from "../components/blog/UsersOverview";
import Sensor from '../rest/Sensor';
// import "node_modules/video-react/dist/video-react.css"; // import css

export default class Video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: localStorage.getItem("video_ip"),
            // src2: 'http://192.168.43.68:8080/shot.jpg',
            id: Date.now(),
            baby_id: localStorage.getItem("baby_id"),
            notifications: null,
            notification_class: "mb-0 bg-danger",
            baby: { birth_date: "" }

        }
        this.babyQuery = this.babyQuery.bind(this);
        this.baby_obj = new Baby();
        this.error_valuer = new Error();
        this.dismiss_notification = this.dismiss_notification.bind(this);
        this.firstData = this.firstData.bind(this);
        this.sensor_obj = new Sensor();
        this.initial_data_temperature = [];
        this.initial_data_sound = [];
        this.sensor_name = "temperature";

    }

    handleImageLoad = () => {

        this.setState({ id: Date.now() });
        console.error('image loaded');
    };
    componentDidMount() {
        this.babyQuery();
        this.firstData();
    }

    async babyQuery() {
        console.log(this.state.baby_id)
        const response_data = await this.baby_obj.get_baby(this.state.baby_id)

        const status = response_data[0];
        const data = response_data[1];


        let error = this.error_valuer.evaluate_status(status, response_data['message']);

        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return false
        }

        const baby = JSON.parse(data['data']);

        if (baby.photo === null)
            baby.photo = require("../images/avatars/default.png")
        this.setState({
            baby: baby
        })

        return true;

    }

    async firstData() {

        const sensor = this.sensor_name;
        const bed = 1;
        const n = 24;

        const response_data_temperature = await this.sensor_obj.latest_values("temperature",bed,n)
        const response_data_sound = await this.sensor_obj.latest_values("sound",bed,n)

        const status_temperature = response_data_temperature[0];
        const data_temperature = response_data_temperature[1];
    
        let error = this.error_valuer.evaluate_status(status_temperature, response_data_temperature['message']);

        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return false
        }


        const status_sound = response_data_sound[0];
        const data_sound = response_data_sound[1];

        
        let error2 = this.error_valuer.evaluate_status(status_sound, response_data_sound['message']);

        if (error2 != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error2
            })
            window.scrollTo(0, 0);
            return false
        }

        
        
        data_temperature['data']["temperature"].map((d=>{
            this.initial_data_temperature.push(d.value)
        }))

        data_sound['data']["sound"].map((d=>{
            this.initial_data_sound.push(d.value)
        }))
        
        
    }


    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }


    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }

    render() {
        return (
            <>
                {this.state.notifications != null && this.renderNotifications()}
                <Container fluid className="main-content-container px-4" >
                    <Row noGutters className="page-header py-4" form >
                        <Col md="4" key={this.state.baby.id} >
                            <Card small className="mb-4 pt-3">
                                <CardHeader className="border-bottom text-center">
                                    <div className="mb-3 mx-auto">
                                        <img
                                            className="rounded-circle"
                                            src={this.state.baby.photo}
                                            alt={this.state.baby.name}
                                            width="110"
                                            height="110"
                                        />
                                    </div>
                                    <h4 className="mb-0"><a href="/baby/profile" >{this.state.baby.name} {this.state.baby.sex}</a></h4>
                                </CardHeader>
                                <ListGroup flush>
                                    <ListGroupItem className="px-4">
                                        <strong className="text-muted d-block mb-2">
                                            Data de Nascimento
                            </strong>

                                        <span>{this.state.baby.birth_date.replace('T', '\t').replace('Z', '')}</span>

                                    </ListGroupItem>

                                </ListGroup>
                            </Card>
                        </Col>


                    </Row>
                    <Row noGutters className="page-header py-4" form>
                        <Col md="6" className="mb-4">
                            <UsersOverview unity="ºC" title="Temperatura" initial_data={this.initial_data_temperature} sensor="temperature" bed="1" n="1"  ></UsersOverview>
                        </Col>
                        <Col md="6" className="mb-4">
                            <UsersOverview unity="db" title="Som" initial_data={this.initial_data_sound} sensor="sound" bed="1" n="1" ></UsersOverview>
                        </Col>

                    </Row>
                </Container>
            </>
        )
    }
}