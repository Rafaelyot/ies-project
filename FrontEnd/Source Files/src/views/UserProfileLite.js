import React, { Component } from "react";
import { Container, Row, Col, Button } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import UserDetails from "../components/user-profile-lite/UserDetails";
import UserAccountDetails from "../components/user-profile-lite/UserAccountDetails";
import UserAccountDetails2 from "../components/user-profile-lite/UserAccountDetails2";

import UserUpdate from "../components/user-profile-lite/UserUpdate";


class UserProfileLite extends Component{

    constructor(props){
      super(props)
    this.id = localStorage.getItem("worker_id")
      
      this.state = {
        show_update : false
      }

      this.update_page = this.update_page.bind(this)
    }

    update_page(){
      this.setState(
        {
          show_update : !this.state.show_update
        }
      )
    }


    render() {
      let updates = this.state.show_update ? <UserUpdate callBack={this.update_page} id={this.id}/> : (<Button outline size="md" className="mb-2" onClick={this.update_page}>
      <i className="material-icons mr-1" >edit3</i>Editar Informações</Button>);
      let aDetails = this.state.show_update ? <UserAccountDetails id={this.id} /> : <UserAccountDetails2 id={this.id} />

    return (
        <Container fluid className="main-content-container px-4">
          <Row noGutters className="page-header py-4">
            <PageTitle title="User Profile" subtitle="Overview" md="12" className="ml-sm-auto mr-sm-auto centered" />
          </Row>
          <Row>
            <Col lg="4">
              <UserDetails id={this.id} control={this.state.show_update}  />
              {updates}
            </Col>
            <Col lg="8">
            {aDetails}
            </Col>
          </Row>
        </Container>
      );
    }}
export default UserProfileLite;
