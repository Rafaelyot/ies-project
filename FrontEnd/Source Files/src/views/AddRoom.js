
import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";
import Error from "../rest/Error";
import getDataForm from "../rest/getDataForm";
import Room from "../rest/Room";

import {
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormInput,
    FormGroup,
    FormSelect,
    Button,
    Container,
    DatePicker,
    Alert
} from "shards-react";
import { thisTypeAnnotation } from "@babel/types";



export default class AddRoom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error_number: 500,
            message: "Erro",
            submitted: false,
            feedback_message: "Algo correu mal !",
            date: new Date(),
            notifications: null,
            notification_class: "mb-0 bg-danger",
            render_room: false

        }
        this.query_addRoom = this.query_addRoom.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.dismiss_notification = this.dismiss_notification.bind(this);
        this.renderNotifications = this.renderNotifications.bind(this);
        this.error_valuer = new Error();
        this.room_obj = new Room();
    }



    async query_addRoom(event) {

        event.preventDefault();
        let data = getDataForm(event.target);



        let response = await this.room_obj.add_room(data);
        let status = response[0];
        let response_data = response[1];


        let error = this.error_valuer.evaluate_status(status, response_data['message']);
        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return false
        }

        this.setState({
            notification_class: "mb-0 bg-success",
            notifications: response_data['message']
        })

        window.scrollTo(0, 0);
        setTimeout(function () {
            window.location.href = '/'
        }, 2000);
        return true


    }

    handleDateChange(value) {
        this.setState({
            ...this.state,
            ...{ date: new Date(value) }
        });
    }



    renderForm() {
        return (
            <Col lg="12" md="12">
                <ListGroup flush>
                    <ListGroupItem className="p-3">
                        <Row>
                            <Col>
                                <Form onSubmit={this.query_addRoom} action="?" >
                                    <Row form>
                                        <Col md="6" className="form-group">
                                            <label htmlFor="feFloor">Piso < span style={{ color: 'red' }} >*</span> </label>
                                            <FormInput
                                                name="floor"
                                                id="feFloor"
                                                type="text"
                                                placeholder="Piso"
                                                onChange={this.onChange}
                                            />
                                        </Col>
                                        <Col md="6">
                                            <label htmlFor="feRoom">Numero do quarto < span style={{ color: 'red' }} >*</span> </label>
                                            <FormInput
                                                name="room_number"
                                                id="feRoom"
                                                type="text"
                                                placeholder="Numero do quarto"
                                            />
                                        </Col>


                                        <Button type="submit">Adicionar Quarto</Button>
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
            </Col>
        )
    }

    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }


    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }

    render() {
        return (
            <>
                {this.state.notifications !== null && this.renderNotifications()}

                <Container fluid className="main-content-container px-4 pb-4">

                    {/* Page Header */}
                    <Row noGutters className="page-header py-4">
                        <PageTitle sm="4" title="Adicionar Novo Quarto" subtitle="Baby Care" className="text-sm-left" />
                    </Row>

                    <Row>

                        {this.renderForm()}

                    </Row>
                </Container>
            </>
        )
    }

}

