
import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";
import Error from "../rest/Error";
import getDataForm from "../rest/getDataForm";
import Parent from "../rest/Parent";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Button,
  Container,
  DatePicker,
  Alert
} from "shards-react";



export default class AddParent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error_number: 500,
      message: "Erro",
      submitted: false,
      feedback_message: "Algo correu mal !",
      date: new Date(),
      value: "",
      notifications: null,
      notification_class: "mb-0 bg-danger",
      photo: "",
      photo_name: "Fotografia...",

    }
    this.query_addWorker = this.query_addWorker.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.photo_fetcher = this.photo_fetcher.bind(this);
    this.error_valuer = new Error();
    this.parent_obj = new Parent();
  }



  async query_addWorker(event) {

    event.preventDefault();
    let data = getDataForm(event.target);

    if (localStorage.getItem("photo") !== null)
      data['photo'] = localStorage.getItem("photo")


    let response = await this.parent_obj.add_parent(data);
    let status = response[0];
    let response_data = response[1];
    

    let error = this.error_valuer.evaluate_status(status, response_data['message']);
    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return false
    }

    this.setState({
      notification_class: "mb-0 bg-success",
      notifications: response_data['message']
    })

    window.scrollTo(0, 0);
    setTimeout(function () {
      window.location.href = '/'
    }, 2000);
    return true


  }

  handleDateChange(value) {
    this.setState({
      ...this.state,
      ...{ date: new Date(value) }
    });
  }




  componentDidMount() {
    localStorage.setItem("photo", null)
  }



  photo_fetcher(event) {
    let target = event.target;

    let reader = new FileReader(target);
    let dataURL = "";
    reader.onload = function () {
      dataURL = reader.result;
      try {
        localStorage.setItem("photo", dataURL)
      }
      catch (e) {
        console.log(e)
        this.setState({
          notifications: "Imagem demasido grande!",
          notification_class: "mb-0 bg-danger"

        })
        window.scrollTo(0, 0);
      }

    }.bind(this);
    reader.readAsDataURL(target.files[0]);
    this.setState({
      photo_name: target.files[0].name
    })


  }


  renderForm() {
    return (
      <Col lg="12" md="12">
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form onSubmit={this.query_addWorker} action="?" >
                  <Row form>
                    <Col md="12" className="form-group">
                      <label htmlFor="feName">Nome < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput
                        name="name"
                        id="feName"
                        type="text"
                        placeholder="Nome"
                      />
                    </Col>
                    <Col md="2">
                      <label htmlFor="feBirth_date">Data de Nascimento < span style={{ color: 'red' }} >*</span> </label>
                      <br></br>
                      <DatePicker
                        name="birth_date"
                        id="feBirth_date"
                        dropdownMode="select"
                        size="md"
                        md="12"
                        selected={this.state.date}
                        onChange={this.handleDateChange}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={1}
                        timeCaption="time"
                        dateFormat="yyyy-MM-d h:mm"
                      />

                    </Col>
                    <Col md="5">
                      <label htmlFor="feSex">Género < span style={{ color: 'red' }} >*</span> </label>
                      <FormSelect id="feSex" name="sex">
                        <option>Choose...</option>
                        <option>Feminino</option>
                        <option>Masculino</option>
                      </FormSelect>

                    </Col>
                    <Col md="5">
                      <label htmlFor="fePhoto">Fotografia </label>
                      <div className="custom-file mb-3">
                        <input type="file" className="custom-file-input" id="customFile2" onChange={this.photo_fetcher} accept="image/*" />
                        <label className="custom-file-label" htmlFor="customFile2">{this.state.photo_name}</label>
                      </div>


                    </Col>
                  </Row>

                  <FormGroup>
                    <label htmlFor="feAddress">Morada</label>
                    <FormInput id="feAddress" placeholder="Morada" name="address" />
                  </FormGroup>

                  <Row form>
                    <Col md="6" className="form-group">
                      <label htmlFor="feContact">Contacto < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput id="feContact" placeholder="Contacto" name="phone_number">
                      </FormInput>
                    </Col>
                  </Row>
                  <br></br>
                  <Button type="submit">Adicionar Progenitor</Button>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Col>
    )
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  render() {
    return (
      <>
        {this.state.notifications !== null && this.renderNotifications()}

        <Container fluid className="main-content-container px-4 pb-4">

          {/* Page Header */}
          <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Adicionar Novo progenitor" subtitle="Baby Care" className="text-sm-left" />
          </Row>

          <Row>

            {this.renderForm()}

          </Row>
        </Container>
      </>
    )
  }

}

