
import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";
import Error from "../rest/Error";
import getDataForm from "../rest/getDataForm";
import Bed from "../rest/Bed";
import Room from "../rest/Room";
import Sensor from "../rest/Sensor";
import {
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormInput,
    Button,
    Container,
    Alert,
    FormSelect
} from "shards-react";
import RoomChart from "./RoomChart";





export default class AddBed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error_number: 500,
            message: "Erro",
            submitted: false,
            feedback_message: "Algo correu mal !",
            date: new Date(),
            notifications: null,
            notification_class: "mb-0 bg-danger",
            render_room: false,
            floors: [],
            rooms_by_floor: {},
            selected_floor: null,
            selected_room_number: null,
            floor: -1,
            room_number: -1



        }

        this.dismiss_form = false
        this.data = [];

        this.query_setRoom = this.query_setRoom.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.dismiss_notification = this.dismiss_notification.bind(this);
        this.renderNotifications = this.renderNotifications.bind(this);
        this.fetch_rooms = this.fetch_rooms.bind(this);
        this.onChange = this.onChange.bind(this);
        this.firstData = this.firstData.bind(this);
        this.error_valuer = new Error();
        this.bed_obj = new Bed();
        this.room_obj = new Room();
        this.sensor_obj = new Sensor();
    }

    async query_setRoom(event) {

        event.preventDefault();
        let data = getDataForm(event.target);

        if (!this.state.render_room) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: "Dados invalidos!"
            })
            window.scrollTo(0, 0);
            return false
        }

        let new_data = {
            "floor": data['floor'].match(/\d/g).join(""),
            "room_number": data['roomNumber'].replace(/^\D+/g, ''),
        }
        this.setState({
            floor: new_data.floor,
            room_number: new_data.room_number
        })
        this.dismiss_form = true;


        return false;


    }

    async firstData() {
        const sensores = ["temperature", "humidity", "infra_reds", "sound"]
        for (let i = 0; i < sensores.length; i++) {
            let response_data = await this.sensor_obj.lastest_values_room(sensores[i], this.state.floor, this.state.room_number, 24);

            const status = response_data[0];
            const data = response_data[1];

            let error2 = this.error_valuer.evaluate_status(status, response_data['message']);

            if (error2 != null) {
                this.setState({
                    notification_class: "mb-0 bg-danger",
                    notifications: error2
                })
                window.scrollTo(0, 0);
                return false
            } 
            
            let temp = [];
            data['data'][sensores[i]].map((b) => {
                temp.push(b.value)
            })

            this.initial_data[i] = temp;
            
        }


    }


    componentDidMount() {
        this.fetch_rooms()
    }

    async fetch_rooms() {
        let all_rooms = await this.room_obj.get_rooms();
        let status = all_rooms[0];
        let data = all_rooms[1];

        let error = this.error_valuer.evaluate_status(status, data['message']);

        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return
        }

        let rooms = JSON.parse(data['data']);

        let floors = [];
        let rooms_by_floor = {}
        rooms.map((room) => {
            if (!floors.includes(room.floor)) {
                floors.push(room.floor)
                rooms_by_floor[room.floor] = []
            }

            rooms_by_floor[room.floor].push(room.room_number)
        })

        floors.sort();

        this.setState({
            floors: floors,
            rooms_by_floor: rooms_by_floor,
        })


    }


    handleDateChange(value) {
        this.setState({
            ...this.state,
            ...{ date: new Date(value) }
        });
    }

    onChange(event) {
        let value = event.target.value;

        if (event.target.id == "feFloor") {

            if (value == "Choose...") {

                this.setState({
                    render_room: false
                })

            } else {

                this.setState({
                    render_room: true,
                    selected_floor: event.target.options[event.target.selectedIndex].id,
                    selected_room_number: undefined
                })

            }

        } else if (event.target.id == "feRoomNumber") {
            if (value == "Choose...") {

                this.setState({
                    render_room: false
                })

            } else {

                this.setState({
                    render_room: true,
                    selected_room_number: event.target.options[event.target.selectedIndex].id
                })

            }
        }
    }

    renderFloor() {

        return (
            <Col md="6">
                <label htmlFor="fePiso">Piso < span style={{ color: 'red' }} >*</span> </label>
                <FormSelect id="fePiso" name="piso" onChange={this.onChange}>
                    <option >Choose...</option>
                    {
                        this.state.floors.map((floor) => {
                            return (
                                <option key={floor} id={floor}>
                                    {floor}º Piso
                    </option>
                            )
                        })

                    }
                </FormSelect>
            </Col>
        )
    }

    renderRoomNumber() {
        return (
            <>
                <label htmlFor="feRoomNumber">Numero do Quarto < span style={{ color: 'red' }} >*</span> </label>
                <FormSelect id="feRoomNumber" name="roomNumber" onChange={this.onChange}>
                    <option >Choose...</option>

                    {

                        this.state.rooms_by_floor[this.state.selected_floor].map((room_number) => {
                            return (
                                <option key={room_number} id={room_number}>
                                    Quarto {room_number}
                                </option>
                            )
                        })

                    }
                </FormSelect>
            </>
        )
    }


    renderForm() {
        return (
            <Col lg="12" md="12">
                <ListGroup flush>
                    <ListGroupItem className="p-3">
                        <Row>
                            <Col>
                                <Form onSubmit={this.query_setRoom} action="?" >
                                    <Row form>
                                        <Col md="6" className="form-group">
                                            <label htmlFor="feFloor"> Piso < span style={{ color: 'red' }} >*</span> </label>
                                            <FormSelect id="feFloor" name="floor" onChange={this.onChange}>
                                                <option >Choose...</option>
                                                {
                                                    this.state.floors.map((floor) => {
                                                        return (
                                                            <option key={floor} id={floor}>
                                                                {floor}º Piso
                                                            </option>
                                                        )
                                                    })

                                                }
                                            </FormSelect>
                                        </Col>
                                        <Col md="6" className="form-group">
                                            {this.state.render_room && this.renderRoomNumber()}
                                        </Col>

                                        <Button type="submit">Selecionar Quarto</Button>
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
            </Col>
        )
    }

    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }


    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }


    render_charts() {
        return (
        <>
                <Col md="12">
                    <span>{this.state.floor}º Piso</span><br></br>
                    <span>Quarto {this.state.room_number}</span><br></br><br></br>
                </Col>
                <Col md="6" className="mb-4">
                    <RoomChart unity="ºC" title="Temperatura" sensor="temperature" floor={this.state.floor} room={this.state.room_number} n="1" ></RoomChart>
                </Col>
                <Col md="6" className="mb-4">
                    <RoomChart unity="db" title="Ruido" sensor="sound" floor={this.state.floor} room={this.state.room_number} n="1"></RoomChart>
                </Col>
                <Col md="6" className="mb-4">
                    <RoomChart unity="%" title="Humidade" sensor="humidity" floor={this.state.floor} room={this.state.room_number} n="1" ></RoomChart>
                </Col>
                <Col md="6" className="mb-4">
                    <RoomChart unity="%" title="Infravermelhos" sensor="infra_reds" floor={this.state.floor} room={this.state.room_number} n="1"></RoomChart>
                </Col>
            </>
        )
    }


    render() {
        return (
            <>
                {this.state.notifications !== null && this.renderNotifications()}

                <Container fluid className="main-content-container px-4 pb-4">

                    {/* Page Header */}
                    <Row noGutters className="page-header py-4">
                        <PageTitle sm="4" title="Selecionar Quarto" subtitle="Baby Care" className="text-sm-left" />
                    </Row>

                    <Row noGutters className="page-header py-4" form >

                        {!this.dismiss_form && this.renderForm()}
                        {this.dismiss_form && this.render_charts()}

                    </Row>
                </Container>
            </>
        )
    }

}

