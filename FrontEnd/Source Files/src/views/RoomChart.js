import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Card, CardHeader, CardBody, Button, Container, Alert } from "shards-react";
import { url } from "../rest/Const";
import Error from "../rest/Error";
import Chart from "../utils/chart";


function generateRandomColor() {
  var red = Math.random() * 256
  var green = Math.random() * 256
  var blue = Math.random() * 256

  // mix the color
  let mix = {
    'red': 0,
    'green': 150,
    'blue': 255
  }

  if (mix != null) {
    red = (red + mix.red) / 2 >> 0;
    green = (green + mix.green) / 2 >> 0;
    blue = (blue + mix.blue) / 2 >> 0;
  }

  return "rgba(" + red + "," + green + "," + blue + ",0.3)"
}

class RoomChart extends React.Component {
  constructor(props) {
    super(props);


    this.canvasRef = React.createRef();

    this.get_temperature = this.get_temperature.bind(this);
    this.stop_data_fetch = this.stop_data_fetch.bind(this);

    this.error_valuer = new Error();
    this.dismiss_notification = this.dismiss_notification.bind(this);


    let dataset_template = {
      label: "Quarto 414",
      fill: "start",
      data: [],
      //backgroundColor: "rgba(0,123,255,0.1)",
      //borderColor: "rgba(0,123,255,1)",
      //pointBackgroundColor: "#ffffff",
      //pointHoverBackgroundColor: "rgb(0,123,255)",
      borderWidth: 0.5,
      pointRadius: 0,
      pointHoverRadius: 1
    }

    let template = {
      title: "WAW",
      labels: Array.from(new Array(24), (_, i) => (i)),
      datasets: []

    }


    let number = 1; //Este parametro deve ser passado por argumento ou por cache

    let quartos = [
      "Quarto 414", "Quarto 415", "Quarto 416"
    ]


    for (let i = 0; i < number; i++) {


      let d = Object.create(dataset_template);
      d.data = Object.create([])
      let color = generateRandomColor()
      d.backgroundColor = color
      d.borderColor = color
      d.pointHoverBackgroundColor = color
      d.label = quartos[i % quartos.length]
      if (this.props.initial_data !== undefined)
        d.data = Object.create(this.props.initial_data)


      if (i % 2 == 0)
        d.pointBorderColor = color

      template.datasets.push(d)

    }


    this.state = {
      chartDatas: template,
      data_streaming: true,
      notifications: null,
      notification_class: "mb-0 bg-danger",
    }



  }


  componentDidMount() {
    this.interval = setInterval(() => this.get_temperature(), 1000);
  }



  componentWillUnmount() {
    clearInterval(this.interval);
  }

  get_temperature() {
    const { unity } = this.props
    const chartOptions = {
      ...{
        responsive: true,
        legend: {
          position: "top"
        },
        elements: {
          line: {
            // A higher value makes the line look skewed at this ratio.
            tension: 0.3
          },
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [
            {
              gridLines: false,
              ticks: {
                callback(tick, index) {
                  // Jump every 7 values on the X axis labels to avoid clutter.
                  return index % 1 !== 0 ? "" : tick + " h";
                }
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                suggestedMax: 25,
                callback(tick) {
                  if (tick === 0) {
                    return tick;
                  }
                  // Format the amounts using Ks for thousands.
                  return `${tick} ` + unity;
                }
              }
            }
          ]
        },
        hover: {
          mode: "nearest",
          intersect: false
        },
        tooltips: {
          custom: false,
          mode: "nearest",
          intersect: false
        }
      },
      ...this.props.chartOptions
    };
    let chart_object = {
      labels: this.state.chartDatas.labels,
      datasets: this.state.chartDatas.datasets
    }

    for (let i = 0; i < this.state.chartDatas.datasets.length; i++) {

      fetch(url + '/last_values_per_room/' + this.props.sensor + '/' + this.props.floor + '/' + this.props.room + '/' + this.props.n + '/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + localStorage.getItem('token')
        },

      }).then(response => {
        this.status = response.status
        this.response_data = response.json()
        return this.response_data
      }).then(data => {

        const dataset = this.state.chartDatas.datasets[i]



        let error = this.error_valuer.evaluate_status(this.status, this.response_data['message']);

        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          window.scrollTo(0, 0);
          return false
        }

        if (dataset.data.length >= 24) {

          for (let i = 1; i < dataset.data.length; i++) {
            dataset.data[i - 1] = dataset.data[i]
          }
          dataset.data.pop();
        }

        console.log(data)

        for (let i = 0; i < this.props.n; i++) {
          if (data['data'][this.props.sensor].length === 0)
            continue
          chart_object.datasets[i].data.push(data['data'][this.props.sensor][i].value)
          console.log(data['data'][this.props.sensor][i].value)
        }


        localStorage.setItem("token", data["token"])


      })
    }
    //console.log(chart_object)
    this.setState({ chartDatas: chart_object })
    if (this.state.data_streaming) {
      try {
        if (this.BlogUsersOverview != undefined)
          this.BlogUsersOverview.destroy();

        this.BlogUsersOverview = new Chart(this.canvasRef.current, {
          type: "LineWithLine",
          data: chart_object,
          options: chartOptions
        });

        this.BlogUsersOverview.render();

      } catch (err) {
      }
    }

  }

  stop_data_fetch() {
    this.setState({ data_streaming: !this.state.data_streaming })
    if (this.state.data_streaming) {
      this.componentDidMount()
    } else {
      this.componentWillUnmount()
    }
  }


  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }



  render() {
    const { title } = this.props;
    try {
      return (
        <>
          {this.state.notifications != null && this.renderNotifications()}
          <Card small className="h-100">
            <CardHeader className="border-bottom">
              <h6 className="m-0">{title}</h6>
              <Button
                size="sm"
                className="d-flex btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0"
                onClick={this.stop_data_fetch}>

                {this.state.data_streaming && "Parar"}
                {!this.state.data_streaming && "Recomeçar"}
              </Button>

            </CardHeader>
            <CardBody className="pt-0">
              {/* <Row className="border-bottom py-2 bg-light">
            <Col sm="6" className="d-flex mb-2 mb-sm-0">
              <RangeDatePicker />
            </Col>
            <Col>
               <Button
                size="sm"
                className="d-flex btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0"
              >
                View Full Report &rarr;
              </Button> 
            </Col>
          </Row> */}
              <canvas
                height="120"
                ref={this.canvasRef}
                style={{ maxWidth: "100% !important" }}
              />
            </CardBody>
          </Card>
        </>
      );
    } catch (err) {


    }
  }
}

RoomChart.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The chart dataset.
   */
  chartData: PropTypes.object,
  /**
   * The Chart.js options.
   */
  chartOptions: PropTypes.object
};

RoomChart.defaultProps = {
  //title: "Temperatura",
  chartData: {
    labels: Array.from(new Array(24), (_, i) => (i)),
    datasets: [
      {
        label: "Quarto 414",
        fill: "start",
        data: [
          //   1694,
          //   1599,
          //   1539,
          //   1533,
          //   1961,
          //   1606,
          //   1724,
          //   1782,
          //   1695,
          //   1977,
          //   1797,
          //   1750,
          //   1999,
          //   1842,
          //   1871,
          //   1603,
          //   1873,
          //   1964,
          //   1964,
          //   1826,
          //   1634,
          //   1907,
          //   1613,
          //   1768,
          //   1845,
          //   1528,
          //   1987,
          //   1883,
          //   1807,
          //   1980,
          1,


        ],
        backgroundColor: "rgba(0,123,255,0.1)",
        borderColor: "rgba(0,123,255,1)",
        pointBackgroundColor: "#ffffff",
        pointHoverBackgroundColor: "rgb(0,123,255)",
        borderWidth: 1.5,
        pointRadius: 0,
        pointHoverRadius: 3
      },
      // {
      //   label: "Quarto 415",
      //   fill: "start",
      //   data: [
      //     1778,
      //     1947,
      //     1578,
      //     1581,
      //     1816,
      //     1581,
      //     1512,
      //     1745,
      //     1586,
      //     1841,
      //     1720,
      //     1717,
      //     1604,
      //     1835,
      //     1596,
      //     1819,
      //     1718,
      //     1563,
      //     1722,
      //     1735,
      //     1540,
      //     1559,
      //     1906,
      //     1581,
      //     1653,
      //     1620,
      //     1721,
      //     1671,
      //     1520,
      //     1556,

      //   ],
      //   backgroundColor: "rgba(255,65,105,0.1)",
      //   borderColor: "rgba(255,65,105,1)",
      //   pointBackgroundColor: "#ffffff",
      //   pointHoverBackgroundColor: "rgba(255,65,105,1)",
      //   borderDash: [3, 3],
      //   borderWidth: 1,
      //   pointRadius: 0,
      //   pointHoverRadius: 2,
      //   pointBorderColor: "rgba(255,65,105,1)"
      // }
    ]
  }
};

export default RoomChart;
