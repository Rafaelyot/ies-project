import React from 'react';
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Container,
  Alert,
  Row,
  Col
} from "shards-react";
import Baby from "../rest/Baby";
import Error from "../rest/Error";
// import "node_modules/video-react/dist/video-react.css"; // import css

export default class Video extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      src: null ? localStorage.getItem("video_phone_ip") === undefined :  localStorage.getItem("video_phone_ip"),
      // src2: 'http://192.168.43.68:8080/shot.jpg',
      id: Date.now(),
      baby_id: localStorage.getItem("baby_id"),
      notifications: null,
      notification_class: "mb-0 bg-danger",
      babies: [],
      step: 10,
      start: 0,
      end: 10,

    }
    this.babiesQuery = this.babiesQuery.bind(this);
    this.baby_obj = new Baby();
    this.error_valuer = new Error();
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.handleScroll = this.handleScroll.bind(this);

  }

  handleImageLoad = () => {

    this.setState({ id: Date.now() });
    console.error('image loaded');
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, false);
    this.babiesQuery();
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, false);
  }


  handleScroll() {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
      this.babiesQuery();
    } else {
    }
  }


  async babiesQuery() {

    const response_data = await this.baby_obj.get_babies(this.state.start, this.state.end)

    this.setState({
      start: this.state.start + this.state.step,
      end: this.state.end + this.state.step
    })

    const status = response_data[0];
    const data = response_data[1];


    let error = this.error_valuer.evaluate_status(status, response_data['message']);

    if (error != null || data['data'] === undefined) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return false
    }

    let persons_ = JSON.parse(data['data'])

    let state_persons = this.state.babies;
    persons_.map((baby) => {
      if (baby.photo === null)
        baby.photo = require("../images/avatars/default.png")
      state_persons.push(baby)
    })

    this.setState({
      babies: state_persons
    })

    return true;

  }


  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  handleImageLoad = () => {

    this.setState({ id: Date.now() });
    console.error('image loaded');
};


  render() {
    let video_src = require("../images/baby.jpg");
    if (this.state.src !== null)
      video_src = this.state.src + '?'+ this.state.id;
    console.log(video_src)
    return (
      <>
        {this.state.notifications != null && this.renderNotifications()}
        <Container fluid className="main-content-container px-4" >
          <Row noGutters className="page-header py-4" form >
            {
              this.state.babies.map((p) => {
                return (
                  <Col md="4" key={p.id}>
                    <Card small className="mb-4 pt-3">
                      <CardHeader className="border-bottom text-center">
                        <div className="mb-3 mx-auto">
                          <img
                            className="rounded-circle"
                            src={p.photo}
                            alt={this.name}
                            width="110"
                            height="110"
                          />
                        </div>
                        <h4 className="mb-0"><a href="/baby/profile" id={p.id} onClick={this.baby_profile}>{p.name} {p.sex}</a></h4>
                      </CardHeader>
                      <img
                        onLoad={this.handleImageLoad}
                        src={video_src}
                        alt={this.name}
                        style={{ "max-width": "100%", "max-height": "100%" }}
                      />
                    </Card>
                  </Col>
                )
              })
            }


          </Row>
        </Container>
      </>
    )
  }
}
