import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";

import ParentDetails from "../components/component-parent-profile/ParentDetails";
import ParentStuffDetails from "../components/component-parent-profile/ParentSuffDetails";


import {
    Container,
    Row,
    Col
  } from "shards-react";

export default class ParentProfile extends Component{

    constructor(props) {
        super(props)
        this.state = {
          id: localStorage.getItem('parent_id_prof'),
        }
      }

      render() {
        return (
          <Container fluid className="main-content-container px-4">
            <Row noGutters className="page-header py-4">
              <PageTitle title="Perfil do Progenitor" md="12" className="ml-sm-auto mr-sm-auto" />
            </Row>
            <Row>
              <Col lg="4">
                <ParentDetails id={this.state.id}/>
              </Col>
              <Col lg="8">
                <ParentStuffDetails id={this.state.id} />
              </Col>
            </Row>
          </Container>
    
        )
      }
    }

