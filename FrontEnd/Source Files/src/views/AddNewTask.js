import {
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormInput,
    FormGroup,
    FormSelect,
    Button,
    Container,
    DatePicker,
    Card,
    CardHeader
} from "shards-react";
import React, { Component } from "react";
import AddTask from "../rest/AddTask";
import BabyTasksDetails from "../components/component-baby-profile/BabyTasksDetails";
import Error from "../rest/Error";



export default class teAddNewTask extends Component {

    constructor(props) {
        super(props)
        this.state = {
            error_number: 500,
            message: "Erro",
            submitted: false,
            feedback_message: "Algo correu mal !",
            dateStart: new Date(),
            dateEnd: new Date()
        }
        this.renderQueryInfo = this.renderQueryInfo.bind(this)
        this.query_addTask = this.query_addTask.bind(this)
        this.handleDateChangeStart = this.handleDateChangeStart.bind(this)
        this.handleDateChangeFinish = this.handleDateChangeFinish.bind(this)
    }

    async query_addTask(event) {

        let addTask_Event = await new AddTask().add_task(event)
        const status = addTask_Event[0]
        const data = addTask_Event[1]


        let feedback_msg = this.state.feedback_message;
        if (status == 200) {
            feedback_msg = "Sucesso !"
            window.location.href = ""
            //new BabyTasksDetails().render()
        }

        this.setState(
            {
                error_number: status,
                message: data['message'],
                submitted: true,
                feedback_message: feedback_msg
            }
        )
        localStorage.setItem("token", data['token'])
    }

    handleDateChangeStart(value) {
        this.setState({
            ...this.state,
            ...{ dateStart: new Date(value) }
        });
    }

    handleDateChangeFinish(value) {
        this.setState({
            ...this.state,
            ...{ dateEnd: new Date(value) }
        });
    }


    renderQueryInfo() {
        let t_color = "green";
        if (this.state.message.split(" ")[1] == "invalidas!"){
            t_color = "red";
        }

        return (
            <Col lg="12" md="12" xs="12">
                <p style={{marginLeft:'300px', color: t_color, marginTop:'15px'}}>{this.state.message}</p>
            </Col>
        )
    }

    render() {
        const { show } = this.props
        if (!show) {
            return null
        }
        return (
            <Col lg="14" md="14" >
        <Card small className="mb-4">
          <CardHeader className="border-bottom" style={{backgroundColor: '#b46022'}}>
            <h6 className="m-0" style={{color:'white'}}>{"Nova Tarefa"}
          </h6>

          </CardHeader>
                <ListGroup flush >
                    <ListGroupItem className="p-3">
                        <Row>
                            <Col>
                                <Form onSubmit={this.query_addTask} action="#" >
                                    <Row form>
                                        <Col md="6" className="form-group">
                                            <label htmlFor="feName">Título < span style={{ color: 'red' }} >*</span> </label>
                                            <FormInput
                                                name="title"
                                                id="feName"
                                                type="text"
                                                placeholder="Insira Título"
                                            />
                                        </Col>
                                    </Row>
                                    <Row form>
                                        <Col md="6" className="form-group">
                                            <label htmlFor="feName">Descrição < span style={{ color: 'red' }} >*</span> </label>
                                            <FormInput
                                                name="description"
                                                id="feName"
                                                type="text"
                                                placeholder="Insira Descrição"
                                            />
                                        </Col>
                                        <Col md="6" className="form-group">
                                            <label htmlFor="feName">Nº de Funcionário< span style={{ color: 'red' }} >*</span> </label>
                                            <FormInput
                                                name="worker"
                                                id="feName"
                                                type="number"
                                                placeholder="Insira Funcionário"
                                            />
                                        </Col>
                                    </Row>
                                    <Row form>
                                        <Col md="6">
                                            <label htmlFor="feBirth_date">Início < span style={{ color: 'red' }} >*</span> </label>
                                            <br></br>
                                            <DatePicker
                                                name="time_start"
                                                id="finicio"
                                                dropdownMode="select"
                                                size="md"
                                                md="12"
                                                selected={this.state.dateStart}
                                                onChange={this.handleDateChangeStart}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={1}
                                                timeCaption="time"
                                                dateFormat="yyyy-MM-d h:mm"
                                            />
                                        </Col>
                                        

                                    </Row>


                                    <Button md="12" outline type="submit" style={{ marginTop: '10px' }}>Agendar</Button>
                                    <Row>
                                        {this.state.submitted && this.renderQueryInfo()}
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
        </Card>
            </Col>
        )
    }

}