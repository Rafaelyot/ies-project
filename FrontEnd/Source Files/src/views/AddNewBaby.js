
import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";
import Error from "../rest/Error";
import Baby from "../rest/Baby";
import Parent from "../rest/Parent";
import Room from "../rest/Room";
import Bed from "../rest/Bed";
import Autosuggest from 'react-autosuggest';
import getDataForm from "../rest/getDataForm";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Button,
  Container,
  DatePicker,
  Alert
} from "shards-react";



export default class AddNewBaby extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error_number: 500,
      message: "Erro",
      submitted: false,
      feedback_message: "Algo correu mal !",
      date: new Date(),
      floors: [],
      selected_floor: -1,
      rooms_by_floor: {},
      select_room_number: -1,
      render_room_number: false,
      render_bed: false,
      available_beds: [],
      selected_bed: -1,
      value: "",
      parents: [],
      selected_parents: [],
      notifications: null,
      notification_class: "mb-0 bg-danger",
      photo: "",
      photo_name: "Fotografia..."

    }
    this.query_addBaby = this.query_addBaby.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.renderFloor = this.renderFloor.bind(this);
    this.fetch_rooms = this.fetch_rooms.bind(this);
    this.renderRoomNumber = this.renderRoomNumber.bind(this);
    this.onChange = this.onChange.bind(this);
    this.get_available_beds = this.get_available_beds.bind(this);
    this.onChangeAuto = this.onChangeAuto.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.remove_entry = this.remove_entry.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.photo_fetcher = this.photo_fetcher.bind(this);
    this.error_valuer = new Error();
  }



  async query_addBaby(event) {

    //let addBaby_Event = await new AddBaby().add_baby(event)

    event.preventDefault();
    let data = getDataForm(event.target);

    if (data['bed'] !== undefined) {
      data.bed = data['bed'].replace(/^\D+/g, '');
      let parents = []
      this.state.selected_parents.map((p) => parents.push(p.id))
      data['parents'] = parents
      if (localStorage.getItem("photo") !== null)
        data['photo'] = localStorage.getItem("photo")


    }

    let baby_obj = new Baby();
    let response = await new baby_obj.add_baby(data);

    let status = response[0];
    let response_data = response[1];


    let error = this.error_valuer.evaluate_status(status, response_data['message']);

    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return false
    }


    this.setState({
      notification_class: "mb-0 bg-success",
      notifications: response_data['message']
    })
    window.scrollTo(0, 0);

    setTimeout(function () {
      window.location.href = '/'
    }, 2000);
    
    return true


  }

  handleDateChange(value) {
    this.setState({
      ...this.state,
      ...{ date: new Date(value) }
    });
  }

  async fetch_rooms() {
    let room_obj = new Room();
    let all_rooms = await room_obj.get_all_rooms();
    let status = all_rooms[0];
    let data = all_rooms[1];

    let error = this.error_valuer.evaluate_status(status, data['message']);

    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return
    }

    let rooms = JSON.parse(data['data']);
    
    let floors = [];
    let rooms_by_floor = {}
    rooms.map((room) => {
      if (!floors.includes(room.floor)) {
        floors.push(room.floor)
        rooms_by_floor[room.floor] = []
      }

      rooms_by_floor[room.floor].push(room.room_number)
    })

    floors.sort();

    this.setState({
      floors: floors,
      rooms_by_floor: rooms_by_floor,
    })


  }

  async get_available_beds() {
    let bed_obj = new Bed();
    const floor = this.selected_floor;
    const room_number = this.select_room_number;
    let available_beds = await bed_obj.get_available_beds_by_floor_and_number(floor, room_number);
    let status = available_beds[0];
    let data = available_beds[1];

    let error = this.error_valuer.evaluate_status(status, data['message']);

    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return
    }

    let beds = JSON.parse(data['data']);
    
    let beds_id = []
    beds.map((bed) => {
      beds_id.push(bed.id)
    })

    

    if (beds_id.length > 0)
      this.setState({
        available_beds: beds_id
      })

    this.no_beds = false


  }

  componentDidMount() {
    this.fetch_rooms();
    this.no_beds = true;
    localStorage.setItem("photo", null)
  }

  onChange(event) {
    //render_room_number: false,
    //  render_bed : false
    let target = event.target;
    let id = target.id;
    let value = target.value
    let value_id = target.options[target.selectedIndex].id;


    if (id == "fePiso") {

      if (value == "Choose...") {
        this.setState({
          render_room_number: false,
          render_bed: false,
          available_beds: []
        })
        this.selected_floor = -1
        this.select_room_number = -1
        this.selected_bed = -1
      }
      else {
        this.setState({
          render_room_number: true,
          available_beds: []
        })
        this.selected_floor = value_id;
      }
    } else if (id == "feRoomNumber") {

      if (value == "Choose...") {
        this.setState({
          render_bed: false,
        })
        this.select_room_number = -1;
        this.selected_bed = -1;
      }
      else {
        this.setState({
          render_bed: true,
          available_beds: []
        })
        this.select_room_number = value_id
      }
    } else {
      if (value == "Choose...") {
        this.selected_bed = -1
      } else {
        this.selected_bed = value_id

      }


    }

  }

  renderSuggestion(suggestion, { query }) {
    //const suggestionText = `${suggestion.first} ${suggestion.last}`;
    const parentName = suggestion.name;
    const parentId = suggestion.id;

    return (
      <span className="suggestion-content">
        <span className="name">
          {
            <span key={parentId}>{parentName}</span>
          }
        </span>
      </span>
    );
  }


  getSuggestionValue(parent) {

    let parents = this.state.selected_parents;

    if (!parents.map(a => a.id).includes(parent.id))
      parents.push(parent);

    this.setState({
      selected_parents: parents
    })

    return ""
  }

  async onSuggestionsFetchRequested({ value }) {


    let parent_obj = new Parent();

    const all_data = await parent_obj.get_parents(value);

    const status = all_data[0];
    const data = all_data[1];

    let error = this.error_valuer.evaluate_status(status, data['message']);

    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return
    }


    let values = []

    JSON.parse(data['data']).map((p) => {

      values.push({ "name": p.name, "id": p.id , "sex" : p.sex , "phone_number" : p.phone_number})
    })


    this.setState({
      parents: values,
    })


  };


  onSuggestionsClearRequested() {
    this.setState({
      parents: []
    });
  };


  onChangeAuto(event, { newValue }) {
    this.setState({
      value: newValue
    });
  }

  remove_entry(event) {
    let target_id = event.target.id;

    let parents = this.state.selected_parents;
    for (let i = 0; i < parents.length; i++) {
      let id = parents[i].id;

      if (id == target_id) {
        parents.splice(i, 1);
        break;
      }
    }

    this.setState({
      selected_parents: parents
    })
  }


  photo_fetcher(event) {
    let target = event.target;

    let reader = new FileReader(target);
    let dataURL = "";
    reader.onload = function () {
      dataURL = reader.result;
      try {
        localStorage.setItem("photo", dataURL)
      }
      catch (e) {
        console.log(e)
        this.setState({
          notifications: "Imagem demasido grande!",
          notification_class: "mb-0 bg-danger"

        })
        window.scrollTo(0, 0);
      }

    }.bind(this);
    reader.readAsDataURL(target.files[0]);
    this.setState({
      photo_name: target.files[0].name
    })


  }


  renderTable() {
    return (
      <Col md="12">
        <table className="table mb" >
          <thead className="bg-light" >
            <tr style={{ backgroundColor: '#007bff' }}>
              <th scope="col" className="border-0" style={{ color: 'white' }}>
                #
                  </th>
              <th scope="col" className="border-0" style={{ color: 'white' }}>
                Name
                  </th>
              <th scope="col" className="border-0" style={{ color: 'white' }}>
                Sex
                  </th>
              <th scope="col" className="border-0" style={{ color: 'white' }}>
                Phone Number
                </th>
              <th scope="col" className="border-0" style={{ color: 'white' }}>
                Remover
                </th>
            </tr>
          </thead>
          <tbody>
            {this.state.selected_parents.map((parent, index) => {
              return (
                <tr key={parent.id} id={parent.id}>
                  <td>{index + 1}</td>
                  <td>{parent.name}</td>
                  <td>{parent.sex}</td>
                  <td>{parent.phone_number}</td>
                  <th scope="col" className="border-0" style={{ color: 'white' }}>
                    <Button className="" outline id={parent.id} onClick={this.remove_entry}>&#10006;</Button>
                  </th>

                </tr>
              )
            })}


          </tbody>
        </table>
      </Col>
    )
  }

  renderParentForm() {

    const value = this.state.value;

    const inputProps = {
      placeholder: 'Search...',
      value,
      onChange: this.onChangeAuto
    };

    return (
      <Col md="4">
        <label htmlFor="feParents">Progenitores < span style={{ color: 'red' }} >*</span> </label>
        <Autosuggest
          suggestions={this.state.parents}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          inputProps={inputProps}


        />
      </Col>
    )
  }




  renderFloor() {

    return (
      <Col md="4">
        <label htmlFor="fePiso">Piso < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="fePiso" name="piso" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.floors.map((floor) => {
              return (
                <option key={floor} id={floor}>
                  {floor}º Piso
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }

  renderRoomNumber() {
    return (
      <Col md="4">
        <label htmlFor="feRoomNumber">Numero do Quarto < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="feRoomNumber" name="roomNumber" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.rooms_by_floor[this.selected_floor].map((room_number) => {
              return (
                <option key={room_number} id={room_number}>
                  Quarto {room_number}
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }

  renderBed() {

    if (this.state.available_beds.length == 0 || this.no_beds)
      this.get_available_beds();
    return (
      <Col md="4">
        <label htmlFor="feBed">Numero da Cama < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="feBed" name="bed" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.available_beds.map((bed_id) => {
              return (
                <option key={bed_id} id={bed_id}>
                  Cama {bed_id}
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }



  renderQueryInfo() {
    return (
      <Col lg="12" md="12">
        <Error error_number={this.state.error_number}
          message={this.state.message} feedback_message={this.state.feedback_message}
          page_url='/add-new-baby' />
      </Col>
    )
  }

  renderForm() {
    return (
      <Col lg="12" md="12">
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form onSubmit={this.query_addBaby} action="?" >
                  <Row form>
                    <Col md="12" className="form-group">
                      <label htmlFor="feName">Nome < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput
                        name="name"
                        id="feName"
                        type="text"
                        placeholder="Nome"
                      />
                    </Col>
                    <Col md="2">
                      <label htmlFor="feBirth_date">Data de Nascimento < span style={{ color: 'red' }} >*</span> </label>
                      <br></br>
                      <DatePicker
                        name="birth_date"
                        id="feBirth_date"
                        dropdownMode="select"
                        size="md"
                        md="12"
                        selected={this.state.date}
                        onChange={this.handleDateChange}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={1}
                        timeCaption="time"
                        dateFormat="yyyy-MM-d h:mm"
                      />

                    </Col>
                    <Col md="5">
                      <label htmlFor="feSex">Género < span style={{ color: 'red' }} >*</span> </label>
                      <FormSelect id="feSex" name="sex">
                        <option>Choose...</option>
                        <option>Feminino</option>
                        <option>Masculino</option>
                      </FormSelect>

                    </Col>
                    <Col md="5">
                      <label htmlFor="fePhoto">Fotografia </label>
                      <div className="custom-file mb-3">
                        <input type="file" className="custom-file-input" id="customFile2" onChange={this.photo_fetcher} accept="image/*" />
                        <label className="custom-file-label" htmlFor="customFile2">{this.state.photo_name}</label>
                      </div>


                    </Col>
                  </Row>

                  <FormGroup>
                    <label htmlFor="feAddress">Morada</label>
                    <FormInput id="feAddress" placeholder="Morada" name="address" />
                  </FormGroup>

                  <Row form>
                    <Col md="6" className="form-group">
                      <label htmlFor="feWeight">Peso < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput id="feWeight" placeholder="Peso" name="weight" />
                    </Col>
                    <Col md="6" className="form-group">
                      <label htmlFor="feHeight">Altura < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput id="feHeight" placeholder="Altura" name="height">
                      </FormInput>
                    </Col>

                    {this.renderFloor()}
                    {this.state.render_room_number && this.renderRoomNumber()}
                    {this.state.render_bed && this.renderBed()}
                    {this.renderParentForm()}
                    {this.state.selected_parents.length > 0 && this.renderTable()}
                  </Row>
                  <br></br>
                  <Button type="submit">Adicionar Bebe</Button>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Col>
    )
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  render() {
    return (
      <>
        {this.state.notifications !== null && this.renderNotifications()}

        <Container fluid className="main-content-container px-4 pb-4">

          {/* Page Header */}
          <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Adicionar Novo Bebe" subtitle="Baby Care" className="text-sm-left" />
          </Row>

          <Row>

            {!this.state.submitted && this.renderForm()}
            {this.state.submitted && this.renderQueryInfo()}

          </Row>
        </Container>
      </>
    )
  }

}

