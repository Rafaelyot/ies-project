import React, { Component, useCallback } from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Container,
  Alert,
  Row,
  Col
} from "shards-react";
import Error from "../rest/Error";
import Baby from "../rest/Baby";



export default class Babies extends Component {

  constructor(props) {
    super(props);

    this.state = {
      avatar: "",
      name: "",
      step: 10,
      start: 0,
      end: 10,
      persons: [],
      information_label: "Progenitores",
      notifications : null,
      notification_class : "mb-0 bg-danger"

    }

    this.baby_obj = new Baby();
    this.get_query = this.get_query.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.baby_profile = this.baby_profile.bind(this);
    this.error_valuer = new Error();


  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, false);
    this.get_query();
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, false);
  }


  handleScroll() {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
      this.get_query();
    } else {
    }
  }

  async get_query() {

    let persons = await this.baby_obj.get_babies(this.state.start, this.state.end);
    this.setState({
      start: this.state.start + this.state.step,
      end: this.state.end + this.state.step
    })

    const status = persons[0];
    const response_data = persons[1];

    let error = this.error_valuer.evaluate_status(status, response_data['message']);

    if (error != null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: error
      })
      window.scrollTo(0, 0);
      return false
    }

    let persons_ = JSON.parse(response_data['data'])

    let state_persons = this.state.persons;
    persons_.map((baby) => {
      if (baby.photo === null)
        baby.photo = require("../images/avatars/default.png")
      state_persons.push(baby)
    })

    this.setState({
      persons: state_persons
    })



  }
  baby_profile(id) {
    localStorage.setItem('baby_id_prof', id);
    window.location.href = '/baby/profile'
    return true;
  }


  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }


  render() {
    return (
      <>
      {this.state.notifications != null && this.renderNotifications()}
      <Container fluid className="main-content-container px-4" >
        <Row noGutters className="page-header py-4" form >
          {
            this.state.persons.map((p) => {
              return (
                <Col md="4" key={p.id}  onClick={() => this.baby_profile(p.id)}>
                  <Card small className="mb-4 pt-3">
                    <CardHeader className="border-bottom text-center">
                      <div className="mb-3 mx-auto">
                        <img
                          className="rounded-circle"
                          src={p.photo}
                          alt={this.name}
                          width="110"
                          height="110"
                        />
                      </div>
                      <h4 className="mb-0"><a href="/baby/profile" >{p.name} {p.sex}</a></h4>
                    </CardHeader>
                    <ListGroup flush>
                      <ListGroupItem className="px-4">
                        <strong className="text-muted d-block mb-2">
                          Data de Nascimento
                      </strong>

                        <span>{p.birth_date.replace('T', '\t').replace('Z', '')}</span>

                      </ListGroupItem>
                      <ListGroupItem className="p-4">
                        <strong className="text-muted d-block mb-2">
                          {this.state.information_label}
                        </strong>
                        <ListGroup flush>
                          {p.parents.map((parent => {
                            return (
                              <ListGroupItem className="px-4" key={parent.id}>{parent.name}</ListGroupItem>
                            )
                          }))}
                        </ListGroup>

                      </ListGroupItem>
                    </ListGroup>
                  </Card>
                </Col>
              )
            })
          }


        </Row>
      </Container>
      </>
    )
  }
}


