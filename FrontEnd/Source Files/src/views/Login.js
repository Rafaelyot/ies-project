import React, { Component } from "react";

import PageTitle from "../components/common/PageTitle";
import LoginQuery from "../rest/LoginQuery";
import Error from "../rest/Error";
import {    
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormInput,
    Button,
    Container,
    Card,
    CardHeader,
    Alert
} from "shards-react";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'Inserir as credenciais',
            error_number: 500,
            message: 'Erro',
            feedback_message: 'Algo correu mal !',
            submitted: false,
            notifications: null,
            notification_class: "mb-0 bg-danger",
        }
        this.query_Login = this.query_Login.bind(this);
        this.error_valuer = new Error();
        this.dismiss_notification = this.dismiss_notification.bind(this);
    }

    async query_Login(event) {
        let login_event = await new LoginQuery().login(event)

        const status = login_event[0]
        const data = login_event[1]
        
        let error = this.error_valuer.evaluate_status(status, data['message']);

        if (error != null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: error
            })
            window.scrollTo(0, 0);
            return false
        }


        this.setState({
            notification_class: "mb-0 bg-success",
            notifications: data['message']
        })

        window.scrollTo(0, 0);
        setTimeout(function () {
            window.location.href = '/'
            
        }, 2000);
        return true

    }

    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }


    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }

    renderForm() {
        return (
            <Col>
                <Form onSubmit={this.query_Login}>
                    <Row form>
                        {/* UserName */}
                        <Col md="6" className="form-group">
                            <label htmlFor="feFirstName">Nome de utilizador</label>
                            <FormInput
                                id="feFirstName"
                                placeholder="Nome de utilizador"
                                name="username"
                            />
                        </Col>
                        {/* PassWord */}
                        <Col md="6" className="form-group">
                            <label htmlFor="feLastName">Password</label>
                            <FormInput
                                id="feLastName"
                                type="password"
                                placeholder="Password"
                                name="password"
                            />
                        </Col>
                    </Row>
                    <Button theme="accent" type="submit" >Iniciar Sessão</Button>
                    &nbsp;
                                                <Button theme="accent">Criar Conta</Button>
                </Form>
            </Col>
        )
    }




    render() {
        return (
            <>
            {this.state.notifications !== null && this.renderNotifications()}
            <Container fluid className="main-content-container px-4">
                <Row noGutters className="page-header py-4">
                    <PageTitle title="Login" s md="12" className="ml-sm-auto mr-sm-auto" />
                </Row>
                <Row>
                    <Col lg="12">
                        <Card small className="mb-4">
                            <CardHeader className="border-bottom">
                                <h6 className="m-0">{this.state.title}</h6>
                            </CardHeader>
                            <ListGroup flush>
                                <ListGroupItem className="p-3">
                                    <Row>
                                        {!this.state.submitted && this.renderForm()}

                                    </Row>
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>
                </Row>
            </Container>
            </>
        )
    }

}
