export default function() {
  return [
    {
      title: "Página inicial",
      to: "/main_page",
      htmlBefore: '<i class="material-icons">edit</i>',
    },
    /*
    {
      title: "Blog Posts",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/blog-posts",
    },
    */
    {
      title: "Novo bebé",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add-new-baby",
    },


    {
      title: "Novo trabalhador",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add_worker",
    },
    {
      title: "Novo progenitor",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add_parent",
    },
    {
      title: "Novo Quarto",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add_room",
    },
    {
      title: "Nova Cama",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add_bed",
    },
    {
      title: "Bébés",
      htmlBefore: '<i class="material-icons">face</i>',
      to: "/babies",
    },
    //{
    //  title: "Conta",
    //  htmlBefore: '<i class="material-icons">person</i>',
    //  to: "/user-profile-lite",
    //},
    {
      title: "Streaming",
      htmlBefore: '<i class="fas fa-video"></i>',
      to: "/all_videos",
    },
   // {
   //   title: "Condições do quarto",
   //   htmlBefore: '<i class="fas fa-chart-area"></i>',
   //   to: "/chart_room",
   // },
    

    // {
    //   title: "Errors",
    //   htmlBefore: '<i class="material-icons">error</i>',
    //   to: "/errors",
    // }
  ];
}
