import React, { Component } from 'react';

class REST extends Component {
  constructor(props) {
    super(props);
    this.state = { };

  }


  componentDidMount() {


    fetch('http://localhost:8000/get_baby/14', {
      REQUEST_METHOD : 'GET',
      headers : {
        'Authorization' : 'Token 328576b48b97f114a4df336a2b27199ed6aa74b7'
      }
    })
      .then(res => res.json())
      .then((data) => {
        this.setState({ baby: data.data })
      })
      .catch(console.log)
  }

  render() {
    return (
      <div>
        <p><b>New Resource created in the server as shown below</b></p>
        <p>Data : {this.state.baby}</p>
      </div>
    )
  }
}

export default REST;



