import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import {
  Card,
  CardHeader,
  Button,
  ListGroup,
  ListGroupItem,
  Progress,
  Col,
  Row
} from "shards-react";
import picBabySearch from './search2.png'
import picMain from './header-pediatria.jpg';
import picGraph from './graph.png'
import picStream from './stream2.png'
import picTask from './task.png'

class MainComponent extends Component{

      constructor(props){
        console.log("on it")
        super(props)
        this.user_id = props.id

        this.state = {

          }

          //this.get_worker(this.user_id);
      }

      get_worker(id){ // TODO: veririfcar se da NULL, pk lança erro de syntax acho
        let workerInfo;
        return fetch(url + '/get_worker/'+id, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + localStorage.getItem('token')
            },
          })
        .then(response => response.json())
        .then(data =>  {
        console.log("AQUI "+data["data"])
        workerInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
        this.setState({name : workerInfo.name,
                      jobTitle : workerInfo.role
        })
    })
    }

  render(){
    let stylecard = {
      backgroundImage: "url(" + picMain + ")",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    }
    return (
      <Card big className="mb-4 pt-3" >
        <CardHeader className="border-bottom text-center" style={stylecard}>

          <h1 className="mb-0" style={{marginTop: "50px",marginLeft: "900px", color: "blue", height:"50px "}}><strong>BabyCare</strong></h1>
          
          <span className="text-muted d-block mb-2" style={{marginLeft: "900px", color: "blue", fontSize:"20px "}}>Tudo o que precisa de saber para o bem estar dos mais pequenos , num só lugar</span>
        </CardHeader>
        <ListGroup flush bg="6">
          <ListGroupItem className="p-4">
            <Card big className="mb-4 pt-3">
                <CardHeader className="border-bottom">
                    <h6 className="m-0">{"Pesquisa Rápida"} </h6> 
                </CardHeader>
                <ListGroup flush>
                    <ListGroupItem className="p-4">
                        
                        <Row>
                        <Col lg="6" >
                        <img
                        className="card-post__image"
                        src={picBabySearch}
                        alt={"picBabySearch"}
                        style={{ display:"block", marginLeft:"auto", marginRight: "auto", height: "300px" }}
                        />
                        </Col>
                        <Col>
                        <h3 style={{marginTop:"50px"}}>Procura em Tempo Real</h3>
                        <span className="text-muted d-block mb-2">Informação Atualizada a cada Instante</span>
                        <h3 style={{marginTop:"50px"}}>Variado Leque de Informação</h3>
                        <span className="text-muted d-block mb-2">Saiba as os Dados Essenciais num Instante</span>
                        
                        </Col>
                        </Row>
                    </ListGroupItem>
                    
                </ListGroup>
            </Card>
          </ListGroupItem>
          <ListGroupItem className="p-4">
            <Card big className="mb-4 pt-3">
                <CardHeader className="border-bottom">
                    <h6 className="m-0">{"Gráficos"} </h6> 
                </CardHeader>
                <ListGroup flush>
                    <ListGroupItem className="p-4">
                        
                        <Row>
                        <Col lg="6" >
                        <img
                        className="card-post__image"
                        src={picGraph}
                        alt={"picBabySearch"}
                        style={{ display:"block", marginLeft:"auto", marginRight: "auto", height: "200px" }}
                        />
                        </Col>
                        <Col>
                        <h3 style={{marginTop:"50px"}}>Ilustração de Informação</h3>
                        <span className="text-muted d-block mb-2">Gŕaficos a ilustrar informação em direto de cada quarto</span>
                        <h3 style={{marginTop:"50px"}}>Dados de Sensores</h3>
                        <span className="text-muted d-block mb-2">Informação sobre humidade, ruído sonoro,
                                                          radiação infravermelha e temperatura de cada quarto</span>
                        
                        </Col>
                        </Row>
                    </ListGroupItem>
                    
                </ListGroup>
            </Card>
          </ListGroupItem>
          
          <ListGroupItem className="p-4">
            <Card big className="mb-4 pt-3">
                <CardHeader className="border-bottom">
                    <h6 className="m-0">{"Registo de tarefas"} </h6> 
                </CardHeader>
                <ListGroup flush>
                    <ListGroupItem className="p-4">
                        
                        <Row>
                        <Col lg="6" >
                        <img
                        className="card-post__image"
                        src={picTask}
                        alt={"picTask"}
                        style={{ display:"block", marginLeft:"auto", marginRight: "auto", height: "200px" }}
                        />
                        </Col>
                        <Col>
                        <h3 style={{marginTop:"50px"}}>Calendarização de Tarefas</h3>
                        <span className="text-muted d-block mb-2">Registo das várias tarefas sobre um bebé </span>
                        
                        </Col>
                        </Row>
                    </ListGroupItem>
                    
                </ListGroup>
            </Card>
          </ListGroupItem>
          <ListGroupItem className="p-4">
            <Card big className="mb-4 pt-3">
                <CardHeader className="border-bottom">
                    <h6 className="m-0">{"Transmissão em direto da cama do bebé"} </h6> 
                </CardHeader>
                <ListGroup flush>
                    <ListGroupItem className="p-4">
                        
                        <Row>
                        <Col lg="6" >
                        <img
                        className=""
                        src={picStream}
                        alt={"picStream"}
                        style={{display:"block", marginLeft:"auto", marginRight: "auto",  height: "300px" }}
                        />
                        </Col>
                        <Col>
                        <h3 style={{marginTop:"50px"}}>Transmissão</h3>
                        <span className="text-muted d-block mb-2">Vídeo em Direto da Cama do Bebé </span>
                        <h3 style={{marginTop:"50px"}}>Rápida captação do comportamento do bebé</h3>
                        <span className="text-muted d-block mb-2">Qualquer comportamento do bebé pode ser logo captado pela câmara</span>
                        
                        </Col>
                        </Row>
                    </ListGroupItem>
                    
                </ListGroup>
            </Card>
          </ListGroupItem>
        </ListGroup>
      </Card>
    );
  }
}




export default MainComponent;
