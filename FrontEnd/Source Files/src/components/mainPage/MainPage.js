import React, { Component } from "react";
import { Container, Row, Col, Button } from "shards-react";

import PageTitle from "../common/PageTitle";



import { render } from "react-dom";
import MainComponent from "./MainComponent";



class UserProfileLite extends Component{

    constructor(props){
      super(props)
      this.id = 2

      this.state = {
        show_update : false
      }

      this.update_page = this.update_page.bind(this)
    }

    update_page(){
      console.log("papa is here");
      this.setState(
        {
          show_update : !this.state.show_update
        }
      )
    }


    render() {

    return (
        <Container fluid className="main-content-container px-4">
          <Row noGutters className="page-header py-4">
            <PageTitle title="Inicio" subtitle="Sem Sessão Iniciada" md="12" className="ml-sm-auto mr-sm-auto centered" />
          </Row>
          <Row>
            <Col lg="12">
              <MainComponent id={this.id} control={this.state.show_update}  />

            </Col>
            <Col lg="8">

            </Col>
          </Row>
        </Container>
      );
    }}
export default UserProfileLite;
