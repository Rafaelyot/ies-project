import React, { Component } from "react";
import { url } from "../../../rest/Const";
import {
  Form,
  InputGroup
} from "shards-react";
import { Redirect } from "react-router-dom";
import Autosuggest from 'react-autosuggest';
import './theme.css';


//const getSuggestionValue = suggestion => suggestion.name;


export default class Default extends Component {
  constructor(props) {
    super(props);

    this.state = {
      babies: [],
      value: ''
    }

    //this.get_babies = this.get_babies.bind(this);
    //this.handleClick = this.handleClick.bind(this);
    this.query = this.query.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.person_type = null;

  }

  async query(name) {
    const response = await fetch(url + "/get_personsByName/" + name, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })

    const status = await response.status
    const json = await response.json()

    const token = json['token']
    if (token != undefined)
      localStorage.setItem('token', token)

    let data = []

    if (json['data'] != undefined) {
      data = JSON.parse(json['data'])
      this.person_type = data['type']
    }

    return data
  }

  renderSuggestion(suggestion, { query }) {
    //const suggestionText = `${suggestion.first} ${suggestion.last}`;
    const babyName = suggestion.name;
    const babyId = suggestion.id;
    const type = suggestion.type;
    let src_ = require("../../../images/avatars/default.png");
    if (suggestion.photo !== null)
      src_ = suggestion.photo;
    
    return (
      <span className="suggestion-content">
        <><img src={src_} width="40px" height="40px"></img>
        </>
        <span className="name">
          {

            <span key={babyId}>{babyName} ({type})</span>

          }
        </span>
      </span>
    );
  }

  getSuggestionValue(person) {

    if (person.type === 'Baby') {
      window.location.href = '/baby/profile'
      localStorage.setItem('baby_id_prof', person.id)
    }
    else if (person.type === "Worker") {
      window.location.href = "/user-profile-lite"
      localStorage.setItem('worker_id', person.id)
    }
    else {
      window.location.href='/parent/profile'
      localStorage.setItem('parent_id_prof', person.id)
    }
    return person.name
  }

  async onSuggestionsFetchRequested({ value }) {

    const data = await this.query(value);



    let values = []

    data.map((baby) => {
      values.push(baby)
    })


    this.setState({ babies: values })

  };


  onSuggestionsClearRequested = () => {
    this.setState({
      babies: []
    });
  };


  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  render() {

    const { babies, value } = this.state;

    const inputProps = {
      placeholder: 'Search...',
      value,
      onChange: this.onChange
    };
    return (

      <Form className="main-navbar__search w-100 d-none d-md-flex d-lg-flex" >
        <InputGroup seamless className="ml-3">
          {/*<InputGroupAddon  type="prepend">
            <InputGroupText>
              <i className="material-icons">search</i>
            </InputGroupText>
    </InputGroupAddon>*/}

          <Autosuggest
            suggestions={this.state.babies}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
          //theme = {theme}
          />
          {/*<FormInput
            className="navbar-search"
            placeholder="Search for something..."
            onChange={this.get_babies}
          />*/}


        </InputGroup>

      </Form>



    )
  }

}
