import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import Error from "../../rest/Error";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  Container,
  Alert,
  Button
} from "shards-react";

class ParentStuffDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      title: "props.title",
      fname: "Primeiro Nome do Progenitor",
      lname: "Último Nome do Progenitor",
      address: "Morada do Progenitor",
      phone_number: "Contacto do Progenitor",
      dbbirth: null,
      dbsex: null,
      bbirth: "Data de Nascimento",
      bsex: "Sexo do Progenitor",
      notification_class: "mb-0 bg-danger",
      notifications: null,
      edit: false
    }

    this.get_parent = this.get_parent.bind(this);
    this.renderSucessNotification = this.renderSucessNotification.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.error_valuer = new Error();
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.onChangeContact = this.onChangeContact.bind(this);
    this.updateParent = this.updateParent.bind(this);
    this.setEdit = this.setEdit.bind(this);

  }

  componentDidMount() {
    this.get_parent(this.props.id);
  }

  onChangeAddress(value) {
      this.setState({address: value})
  }

  onChangeContact(value) {
      this.setState({phone_number: value})
  }


  get_parent(id) { // TODO: veririfcar se da NULL, pk lança erro de syntax acho

    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }


    let parentInfo;
    console.log("Stuff parent" + id)
    return fetch(url + '/get_parent_by_id/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {
        this.status = response.status;
        
        return response.json()
      })
      .then(data => {
        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }

        parentInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
        let parentDate = parentInfo.birth_date
        let dateD = parentDate.split("T")[0].split("-")
        let dateH = parentDate.split("T")[1].split("Z")[0]
        this.setState({
          fname: parentInfo.name.split(" ")[0],
          lname: parentInfo.name.split(" ")[1],
          address: parentInfo.address,
          phone_number: parentInfo.phone_number,
          bbirth: dateD[2] + "-" + dateD[1] + "-" + dateD[0] + " " + dateH,
          bsex: parentInfo.sex == "M" ? "Masculino" : "Feminino",
          dbbirth: parentDate,
          dbsex: parentInfo.sex

        })

      })
  }

  updateParent(){
    let msg = {
        name: this.state.fname + ' '+this.state.lname ,
        birth_date: this.state.dbbirth,
        sex: this.state.dbsex,
        address: this.state.address,
        phone_number: this.state.phone_number
    }
    console.log(msg)
    return fetch(url + '/update_parent/' + this.props.id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + localStorage.getItem('token')
        },
        body: JSON.stringify(msg)
      })
        .then(response => {
          this.status = response.status;
          
          return response.json()
        })
        .then(data => {
          let status = this.status;
  
          let error = this.error_valuer.evaluate_status(status, data['message']);
          
          if (error != null) {
            this.setState({
              notification_class: "mb-0 bg-danger",
              notifications: error,
            })
            return false
          } else {
            this.setState({
              edit: false
            })
            return true
          }
        })
    }

  setEdit() {
    this.setState({
      edit: true
    })
  }
  

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }

  renderSucessNotification(){
    return (
        <Col lg="12" md="12">
                <p style={{marginLeft:'300px', color: 'green', marginTop:'15px'}}>Editado com sucesso</p>
            </Col>
    )
  }


  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class} style={{borderRadius:"10px"}}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  render() {
        
    return (
        <Card small className="mb-4">
          <CardHeader className="border-bottom" style={{ backgroundColor: '#007bff' }}>
            <h6 className="m-0" style={{ color: 'white' }}>{this.props.title}</h6>
          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      {/* First Name */}
                      <Col md="5" xs="6" className="form-group">
                        <label htmlFor="idBFirstName">Primeiro Nome</label>
                        <FormInput
                          id="idBFirstName"
                          placeholder="First Name"
                          value={this.state.fname}
                          onChange={() => { }}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="5" xs="6" className="form-group">
                        <label htmlFor="idBLastName">Last Name</label>
                        <FormInput
                          id="idBLastName"
                          placeholder="Last Name"
                          value={this.state.lname}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    {<Row form>
                    {this.state.edit && 
                      <Col md="2" xs="3" className="form-group">
                        <label htmlFor="idBH">Morada<i className="material-icons mr-1" >edit3</i></label>
                        <FormInput
                          type="text"
                          id="idBH"
                          placeholder="Insira a morada"
                          value={this.state.address}
                          onChange={(e) => {this.onChangeAddress(e.target.value)}}
                        />
                      </Col>}
                    {!this.state.edit && 
                    <Col md="2" xs="3" className="form-group">
                        <label htmlFor="idBH">Morada</label>
                        <FormInput
                          type="text"
                          id="idBH"
                          placeholder="Insira a morada"
                          value={this.state.address}
                          onChange={() => {}}
                        />
                      </Col>}
                      {this.state.edit && 
                      <Col md="2" xs="3" className="form-group">
                        <label htmlFor="idBW">Contacto<i className="material-icons mr-1" >edit3</i></label>
                        <FormInput
                          type="text"
                          id="idBW"
                          placeholder="Insira contato"
                          value={this.state.phone_number}
                          onChange={(e) => {this.onChangeContact(e.target.value)}}
                        />
                      </Col> }
                      {!this.state.edit &&
                      <Col md="2" xs="3" className="form-group">
                      <label htmlFor="idBW">Contacto</label>
                      <FormInput
                        type="text"
                        id="idBW"
                        placeholder="Insira contato"
                        value={this.state.phone_number}
                        onChange={() => {}}
                      />
                    </Col>
                      }
                      <Col md="2" xs="6" className="form-group">
                        <label htmlFor="idBBirth">Data de Nascimento</label>
                        <FormInput
                          type="text"
                          id="idBBirth"
                          placeholder="Babys BirthDate"
                          value={this.state.bbirth}
                          onChange={() => { }}
                        />
                      </Col>
                      <Col md="2" xs="4" className="form-group">
                        <label htmlFor="idBSex">Sexo</label>
                        <FormInput
                          type="text"
                          id="idBSex"
                          placeholder="Babys Sex"
                          value={this.state.bsex}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    }
                  </Form>
                  {!this.state.edit &&
                  <Button outline size="md" className="mb-2" onClick={() => this.setEdit()} >
                    <i className="material-icons mr-1" >edit3</i>Editar Informações
                  </Button>
                  }
                  {this.state.edit && 
                  <Button outline size="md" className="mb-2"   theme="success" onClick={() => this.updateParent()} >
                        <i className="material-icons mr-1" >edit3</i>Confirmar
                    </Button>}
                    {this.state.notifications !== null && this.renderNotification} 
                  
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
          <ListGroup>

          </ListGroup>
        </Card>
    )

  }

};

ParentStuffDetails.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

ParentStuffDetails.defaultProps = {
  title: "Informações Básicas"
};

export default ParentStuffDetails;
