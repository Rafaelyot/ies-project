import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import React, { Component } from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Container,
  Alert,
  Button
} from "shards-react";
import Error from "../../rest/Error";

class ParentDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      avatar: "",
      name : " ",
      metaTaskTitle : "Progenitor dos bebés",
      taskName: "Colocar nome do bebe aqui",
      notification_class: "mb-0 bg-danger",
      notifications: null,
      babies: []
    }

    this.get_babies(props.id);
    this.get_info(props.id);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.error_valuer = new Error();
    this.redirect_baby = this.redirect_baby.bind(this);
  }

  get_info(id) {
    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }
    let parentInfo;
    console.log("Details baby " + id)
    return fetch(url + '/get_parent_by_id/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {   
        this.status = response.status;
        return response.json() 
      }) 
      .then(data => {
        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        console.log(error)
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }
        parentInfo = JSON.parse(data["data"])
        if(parentInfo.photo === null)
        parentInfo.photo = require("../../images/avatars/default.png")
        this.setState({
          name: parentInfo.name,
          avatar: parentInfo.photo
        })

      })
  }

  get_babies(id) {
    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }
    let babiesInfo;
    console.log("Details baby " + id)
    return fetch(url + '/get_babyByParent/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {   
        this.status = response.status;
        return response.json() 
      }) 
      .then(data => {
        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        console.log(error)
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }
        babiesInfo = JSON.parse(data["data"])

        let state_babies = this.state.babies
        babiesInfo.map((baby) =>  {
          state_babies.push(baby)
        })

        this.setState({
          babies: state_babies
        })

      })
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }

  redirect_baby(id) {
    console.log("baby profile "+id);
    localStorage.setItem('baby_id_prof', id);
    window.location.href = '/baby/profile'
    return true;
  }
  

  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class} style={{borderRadius:"10px"}}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  render() {


    return (
      <>
      {this.state.notifications !== null && this.renderNotifications()}
      <Card small className="mb-4 pt-3">
      <CardHeader className="border-bottom text-center">
        <div className="mb-3 mx-auto">
          <img
            className="rounded-circle"
            src={this.state.avatar}
            alt={this.state.name}
            width="110"
            height="110"
          />
        </div>
        <h4 className="mb-0">{this.state.name}</h4>
      </CardHeader>
      <ListGroup flush>
        <ListGroupItem className="px-4">
          <strong className="text-muted d-block mb-2">
            {this.state.metaTaskTitle}
          </strong>
  
          <span>{this.state.babies.map((baby) => {
            return(
              <ListGroupItem className="px-4" key={baby.id} onClick={() => this.redirect_baby(baby.id)}> {baby.name}</ListGroupItem>
            )
          })}</span>
  
        </ListGroupItem>
      </ListGroup>
    </Card>
    </>
  )
  }
}


ParentDetails.propTypes = {
  /**
   * The user details object.
   */
  userDetails: PropTypes.object
};


export default ParentDetails;