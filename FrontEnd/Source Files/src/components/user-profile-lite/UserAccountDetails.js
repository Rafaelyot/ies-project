import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button
} from "shards-react";

class UserAccountDetails extends Component{
     constructor(props){
       super(props)
       this.user_id = props.id
       this.bools = props.bools
  
       this.state = {
          name: "",
          birth : "",
          sex : "",
          local : "",
          phone : ""
        }

        this.get_worker(this.user_id);
     }


     get_worker(id){ // TODO: veririfcar se da NULL, pk lança erro de syntax acho
      let workerInfo;
      return fetch(url + '/get_worker/'+id, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Token ' + localStorage.getItem('token')
          },
        })
      .then(response => response.json())
      .then(data =>  {
      console.log("AQUI "+data["data"])
      workerInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
      console.log(workerInfo);
      let babyDate = workerInfo.birth_date
      let dateD = babyDate.split("T")[0]
      let dateH = babyDate.split("T")[1].split("Z")[0]
      this.setState({name : workerInfo.name,
                    birth : dateD + " " +  dateH,
                    local : workerInfo.address,
                    sex : workerInfo.sex,
                    phone : workerInfo.phone_number.split("+")[1]
      })
  })
  }



    render(){
      return (
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">{"Detalhes"}
          </h6>

          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      {/* First Name */}
                      <Col md="6" className="form-group">
                        <label htmlFor="idWName">Nome</label>
                        <FormInput
                          id="idWName"
                          placeholder="Nome"
                          value={this.state.name}
                          onChange={() => {}}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="6" className="form-group">
                        <label htmlFor="idWBirth">Data de Nascimento</label>
                        <FormInput
                          id="idWBirth"
                          placeholder="dd-mm-aaaa hh:mm:ss"
                          value={this.state.birth}
                          onChange={() => {}}
                        />
                      </Col>
                    </Row>
                     <Row form>
                      {/* First Name */}
                      <Col md="1" xs="2" className="form-group">
                        <label htmlFor="idWSex">Sexo</label>
                        <FormInput
                          id="idWSex"
                          placeholder="Genero"
                          value={this.state.sex}
                          onChange={() => {}}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="3" xs="5" className="form-group">
                        <label htmlFor="idWLoc">Morada</label>
                        <FormInput
                          id="idWLoc"
                          placeholder="Rua"
                          value={this.state.local}
                          onChange={() => {}}
                        />
                      </Col>
                      <Col md="2" xs="5" className="form-group">
                        <label htmlFor="idWPhone">Contacto</label>
                        <FormInput
                          id="idWPhone"
                          placeholder="Rua"
                          value={this.state.phone}
                          onChange={() => {}}
                        />
                      </Col>

                    </Row>

                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Card>
      )
    }
}

UserAccountDetails.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

UserAccountDetails.defaultProps = {
  title: "Account Details"
};

export default UserAccountDetails;
