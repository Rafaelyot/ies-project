import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import {
  Card,
  CardHeader,
  Button,
  ListGroup,
  ListGroupItem,
  Progress
} from "shards-react";

class UserDetails extends Component{

      constructor(props){
        super(props)
        this.user_id = props.id

        this.state = {
          name: "Nome do Utilizador",
          avatar: require("./../../images/avatars/2.jpg"),
          jobTitle: "Função Desempenhada"
          }

          this.get_worker(this.user_id);
      }

      get_worker(id){ // TODO: veririfcar se da NULL, pk lança erro de syntax acho
        let workerInfo;
        return fetch(url + '/get_worker/'+id, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + localStorage.getItem('token')
            },
          })
        .then(response => response.json())
        .then(data =>  {
        console.log("AQUI "+data["data"])
        workerInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
        this.setState({name : workerInfo.name,
                      jobTitle : workerInfo.role
        })
    })
    }

  render(){
    return (
      <Card small className="mb-4 pt-3">
        <CardHeader className="border-bottom text-center">
          <div className="mb-3 mx-auto">
            <img
              className="rounded-circle"
              src={this.state.avatar}
              alt={this.state.name}
              width="110"
            />
          </div>
          <h4 className="mb-0">{this.state.name}</h4>
          <span className="text-muted d-block mb-2">{this.state.jobTitle}</span>
        </CardHeader>
        <ListGroup flush>
          <ListGroupItem className="p-4">



          </ListGroupItem>
        </ListGroup>
      </Card>
    );
  }
}

UserDetails.propTypes = {
  /**
   * The user details object.
   */
  UserDetails: PropTypes.object
};


export default UserDetails;
