import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button
} from "shards-react";

class UserUpdate extends Component{
     constructor(props){
       super(props)
       this.user_id = props.id

       this.state = {
          cbFunction : props.callBack,
          local : "",
          phone : "",
          
          is_phone_error : false,
          is_local_error : false
          
        }
        this.updateWorker = this.updateWorker.bind(this)

        this.get_worker(this.user_id);
     }


     get_worker(id){ // TODO: veririfcar se da NULL, pk lança erro de syntax acho
      let workerInfo;
      return fetch(url + '/get_worker/'+id, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Token ' + localStorage.getItem('token')
          },
        })
      .then(response => response.json())
      .then(data =>  {
      console.log("AQUI "+data["data"])
      workerInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
      let babyDate = workerInfo.birth_date
      let dateD = babyDate.split("T")[0]
      let dateH = babyDate.split("T")[1].split("Z")[0]
      this.setState({name : workerInfo.name,
                    birth : dateD + " " +  dateH,
                    local : workerInfo.address,
                    sex : workerInfo.sex,
                    phone : workerInfo.phone_number.split("+")[1]
      })
  })
  }
  
    updateWorker(){
        console.log("ola" + document.getElementById("idWPhone").value)
        let temp_phone =  document.getElementById("idWPhone").value
        console.log(">> " + document.getElementById("idWPhone").placeholder )
        let temp_local = document.getElementById("idWLoc").value

        let final_phone = ""
        let final_local = ""
        if (temp_phone.length == 0 && temp_local.length == 0){
            return;
        }

        if (temp_phone.length == 9 || temp_phone.length == 0){
            console.log("adeus")
            this.state.is_phone_error = false
            final_phone = temp_phone.length == 9 ? ("+" + temp_phone) : ("+" + document.getElementById("idWPhone").placeholder)
        } else {
            this.state.is_phone_error = true
            console.log("nice")
        }

        if (temp_local.length == 0 || temp_local.length > 6){
            this.state.is_local_error = false
            final_local = temp_local.length > 6 ? (temp_local) : (document.getElementById("idWLoc").placeholder)

            console.log("right size")
        } else {
            this.state.is_local_error = true
        }
        
        console.log("<<" + final_local + "- " + final_phone)
        if (!this.state.is_local_error && !this.state.is_phone_error){
            
            let data = {"phone_number" : final_phone, "address": final_local}
            return fetch(url + '/update_worker/'+this.user_id, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body : JSON.stringify(data),
          })
        .then(response => response.json())
        .then(data =>  {
        
        this.props.callBack();

        
            })
        }
        

        /* this.setState({

        }) */
    }

    render(){
        let error_phone = this.state.is_phone_error ? (<label style={{color:'red'}}>Número deve ter 9 digitos!</label>) : null
        let error_local = this.state.is_local_error ? (<label style={{color:'red'}}>Morada muito Curta!</label>) : null
        

      return (
        <Card small className="mb-4">
          <CardHeader className="border-bottom" style={{backgroundColor: '#b46022'}}>
            <h6 className="m-0" style={{color:'white'}}>{"Editar"}
          </h6>

          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                     <Row form>
                      <Col md="5" xs="8" className="form-group">
                        <label htmlFor="idWLoc">Morada</label>
                        <FormInput
                          id="idWLoc"
                          placeholder={this.state.local}
                          onChange={() => {}}
                        />
                        {error_local}
                      </Col>
                      <Col md="4" xs="8" className="form-group">
                        <label htmlFor="idWPhone">Contacto</label>
                        <FormInput
                          id="idWPhone"
                          type = "number"
                          placeholder={this.state.phone}
                          onChange={() => {}}
                        />
                        {error_phone}
                      </Col>
                      <Col>
                      <label style={{color:'white'}}>Contacto</label>
                      <Button outline size="md" className="mb-2" onClick={this.updateWorker}>
                        <i className="material-icons mr-1">assignment</i> Confirmar
                      </Button>
                      </Col>

                    </Row>

                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Card>
      )
    }
}

UserUpdate.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

UserUpdate.defaultProps = {
  title: "Account Details"
};

export default UserUpdate;
