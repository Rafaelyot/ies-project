import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import Error from "../../rest/Error";
import {
    Card,
    CardHeader,
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormGroup,
    FormInput,
    FormSelect,
    FormTextarea,
    Button,
    Container,
    Alert
} from "shards-react";
import BabyTask from "./BabyTask";



class BabyTasksDetails extends Component {

    constructor(props) {
        super(props)

        //this.get_baby = this.get_baby.bind(this) // Using Function.prototype.bind in render creates a new function each time the component renders, which may have performance implications (see below).
        this.task_counter = 0;
        this.myId = this.props.id;

        this.state = {
            dict : {},
            tasks: [],
            notification_class: "mb-0 bg-danger",
            notifications: null
        }
        this.checkVality = this.checkVality.bind(this);
        this.editar_toggle = this.editar_toggle.bind(this);

        
        this.get_task = this.get_task.bind(this);
        this.renderNotifications = this.renderNotifications.bind(this);
        this.dismiss_notification = this.dismiss_notification.bind(this);
        this.reloadPage = this.reloadPage.bind(this);
        this.error_valuer = new Error();

    }

    componentDidMount() {
        this.get_task(this.props.id);
        
    }

    reloadPage(){
        this.get_task(this.myId);
    }

    get_task(id) { 

        if (id == null) {
            this.setState({
                notification_class: "mb-0 bg-danger",
                notifications: "Erro inesperado"
            })
            return
        }

        let babyInfo;
        console.log("here task " + id)
        return fetch(url + '/get_tasks_by_baby/' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
        })
            .then(response => {
                this.status = response.status;
                return response.json()
            })
            .then(data => {

                let status = this.status;

                let error = this.error_valuer.evaluate_status(status, data['message']);

                if (error != null) {
                    this.setState({
                        notification_class: "mb-0 bg-danger",
                        notifications: error
                    })
                    return false
                }

                babyInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
                let my_tasks = []
                babyInfo.forEach(b => {
                    let t_id = b.id
                    let day_time_start = b.time_start.split("T")[0]
                    let hour_time_start = b.time_start.split("T")[1].split("Z")[0]
                    let day_time_end = "";
                    let hour_time_end = "";
                    let mid_end = "";
                    if (b.time_end != null){
                         day_time_end = b.time_end.split("T")[0]
                         hour_time_end = b.time_end.split("T")[1].split("Z")[0]
                         mid_end = " ";
                    }
                    
                    console.log("baby: " + b.id)
                    my_tasks.push(
                        {
                            title: b.title,
                            worker: b.worker,
                            description: b.description,
                            time_start: day_time_start + " " + hour_time_start,
                            time_end: day_time_end + mid_end + hour_time_end,
                            id : b.id
                        }
                    )


                })
                this.setState({ tasks: my_tasks })
            })
    }

    


    checkVality(data_f){
       // console.log("fim: " + data_f);

      //  console.log("data: " + data_h + "; " + new Date());
      //  console.log(new Date() < new Date(data_f));
        this.task_counter++;
        this.state.dict["id_button_msg_"+this.task_counter] = ["Editar", false];
        let bool = this.state.dict["id_button_msg_"+this.task_counter];
        console.log("check: ", bool[0] + "; "+ this.task_counter + " <-> " + this.state.dict);
        return (new Date() < new Date(data_f))
    }

    editar_toggle(obj){
        console.log("botoon id: "+ obj.target.id.split("_")[2]);
        console.log("dict: " + this.state.dict);
        /* let botao = document.getElementById("id_button_msg_" + obj.target.id.split("_")[2]);
        console.log(":: " + botao); */
        /* let bool = this.state.dict["id_button_msg_"+obj.target.id.split("_")[2]];
        console.log("bool: ", bool); */
        let desc = document.getElementById("idTDesc_" + obj.target.id.split("_")[2]);
        console.log("value. " + desc.value)
        desc.defaultValue = desc.value;
        desc.value = "";

        /* this.setState({
            
        }) */
    }

    dismiss_notification() {
        this.setState({
            notifications: null
        })
    }

    renderNotifications() {

        return (
            <Container fluid className="px-0">
                <Alert className={this.state.notification_class} style={{ borderRadius: "10px" }}>
                    <i className="fa fa-info mx-2"></i> {this.state.notifications}
                    <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
                </Alert>
            </Container>
        )
    }


    render() {
        let { tasks } = this.state;
        if (tasks.length == 0) {
            tasks = [
                {
                    title: "Não tem tarefas registadas",
                    worker: "",
                    description: "",
                    time_start: "",
                    time_end: "",
                }
            ]
        }
        return (
            <>
                {this.state.notifications !== null && this.renderNotifications()}
                <Card small className="mb-4" >
                <CardHeader className="border-bottom" style={{backgroundColor: '#b55f04'}}>
                    <h6 className="m-0" style={{color:'white'}}>{this.props.title}</h6>
                </CardHeader>
                <ListGroup flush >

                    {tasks.map(l => 
                            <BabyTask key={l.id} ids={l.id} time_end={l.time_end} title={l.title} worker={l.worker} description={l.description} time_start={l.time_start} cbFunction={this.reloadPage}> 

                            </BabyTask>
                        
                    )}
                </ListGroup>
                </Card>
            </>
        )
    }

};

BabyTasksDetails.propTypes = {
    /**
     * The component's title.
     */
    title: PropTypes.string
};

BabyTasksDetails.defaultProps = {
    title: "Tarefas"
};

export default BabyTasksDetails;
