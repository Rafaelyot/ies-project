import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import React, { Component } from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Container,
  Alert,
  Button
} from "shards-react";
import Error from "../../rest/Error";

class BabyDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      avatar: "",
      name : " ",
      metaTaskTitle : "Tarefa mais Recente",
      taskName: "Nome da Tarefa",
      metaNotesTitle : "Notas",
      metaValue : "Bebé muito irrequieto e que com soluços constantes",
      notification_class: "mb-0 bg-danger",
      notifications: null
    }
    this.baby_id = props.id
    this.get_info(props.id);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.videoStreaming = this.videoStreaming.bind(this);
    this.chartStreaming = this.chartStreaming.bind(this);
    this.error_valuer = new Error();
  }

  get_info(id) {
    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }
    let babyInfo;
    console.log("Details baby " + id)
    return fetch(url + '/get_baby/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {   
        this.status = response.status;
        return response.json() 
      }) 
      .then(data => {
        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        console.log(error)
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }
        babyInfo = JSON.parse(data["data"])
        if(babyInfo.photo === null)
          babyInfo.photo = require("../../images/avatars/default.png")
        this.setState({
          name: babyInfo.name,
          avatar: babyInfo.photo
        })

      })
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }

  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class} style={{borderRadius:"10px"}}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  videoStreaming() {
    window.location.href="/video"
    localStorage.setItem("baby_id",this.baby_id)
    localStorage.setItem("video_ip","http://images.ctfassets.net/wy4h2xf1swlt/asset_34390/71fda9c64e8c96ec5ae0b9f1433fa3fa/Hospital-birth-options_v2_iStock_59749190_LARGE-min.jpg")
    localStorage.setItem("dynamic_id",false)
  }

  chartStreaming() {
    window.location.href="/chart_per_baby"
    localStorage.setItem("baby_id",this.baby_id)
  }

  render() {
    return (
      <>
      {this.state.notifications !== null && this.renderNotifications()}
      <Card small className="mb-4 pt-3">
      <CardHeader className="border-bottom text-center">
        <div className="mb-3 mx-auto">
          <img
            className="rounded-circle"
            src={this.state.avatar}
            alt={this.state.name}
            width="110"
            height="110"
          />
        </div>
        <h4 className="mb-0">{this.state.name}</h4><br></br>
        <span className="mb-0"><Button onClick={this.videoStreaming}><i class="fas fa-video"></i></Button></span>&nbsp;&nbsp;&nbsp;
        <span className="mb-0"><Button onClick={this.chartStreaming}><i class="fas fa-chart-area"></i></Button></span>
      </CardHeader>
    </Card>
    </>
  )
  }
}


BabyDetails.propTypes = {
  /**
   * The user details object.
   */
  userDetails: PropTypes.object
};


export default BabyDetails;
