import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import {
    Card,
    CardHeader,
    ListGroup,
    ListGroupItem,
    Row,
    Col,
    Form,
    FormGroup,
    FormInput,
    FormSelect,
    FormTextarea,
    Button
} from "shards-react";



class BabyTask extends Component {

    constructor(props) {
        super(props)

        //this.get_baby = this.get_baby.bind(this) // Using Function.prototype.bind in render creates a new function each time the component renders, which may have performance implications (see below).
        this.id = this.props.ids;
        

        this.state = {
            
            time_end : this.props.time_end,
            title : this.props.title,
            worker : this.props.worker,
            description : this.props.description,
            time_start : this.props.time_start,
            temp_description : undefined,
            edit : "Editar",
            submitBottom : null,
            delete : "Apagar",
            call_back : this.props.cbFunction,
            finTime : "Finalizar"
        }
        console.log("CONTRUTOR: " + this.id);
        this.checkVality = this.checkVality.bind(this);
        this.editar_toggle = this.editar_toggle.bind(this);
        this.submitUpdate = this.submitUpdate.bind(this);
        this.deletetask = this.deletetask.bind(this);
        this.finalize = this.finalize.bind(this);


        
        this.changeDescription = this.changeDescription.bind(this);
    }


    checkVality(data_f){
        return data_f == "";
        /* return (new Date() < new Date(data_f)) */
    }

    checkDesc(t_var){
        if (t_var.length < 6){
            return false
        }
        return true
    }

    changeDescription(value) {
        this.setState({
            description: value
        })
    }

    editar_toggle(obj){
       /*  console.log("undefined: " + (this.state.temp_description == undefined) ) */
        let estado_ed = this.state.edit == "Editar" ? "Cancelar" : "Editar";
        console.log("id: " + this.id);
        
        if (estado_ed == "Cancelar"){
            document.getElementById("idTDesc_" + this.id).value = "";
            this.setState({
                edit : estado_ed,
                submitBottom : <Button size="md" onClick={this.submitUpdate}><i className="material-icons mr-1" >email</i>Submeter</Button>,
                delete : "Apagar",
                finTime : "Finalizar"
            })
        } else {
            this.setState({
                edit : estado_ed,
                submitBottom : null,
                delete : "Apagar",
                finTime : "Finalizar"
            })
        }

        
    }

    submitUpdate(){
        console.log("id SUB: " + this.id);
        let estado_ed = this.state.edit == "Editar" ? "Cancelar" : "Editar";

            console.log("value: " + document.getElementById("idTDesc_" + this.id).value);
            if ( document.getElementById("idTDesc_" + this.id).value != ""){
                console.log("not nada")
                if (this.checkDesc(document.getElementById("idTDesc_" + this.id).value)){
                    let data = {"description" : this.state.description}
                        fetch(url + '/update_task/'+this.id , {
                        method: 'PUT',
                        headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + localStorage.getItem('token')
                        },
                        body : JSON.stringify(data),
                    })

                    }
            }
            console.log("closing");
            this.setState({
                submitBottom : null,
                edit : estado_ed,
                delete : "Apagar",
                finTime : "Finalizar"
            })
    }


    deletetask(){
        if (this.state.delete == "Apagar"){
            console.log("Apagar?");
            this.setState({
                delete : "Confirmar",
                finTime : "Finalizar"
            })

        } else {
            console.log("Apagar!");
            return fetch(url + '/remove_task/'+this.id , {
                        method: 'DELETE',
                        headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + localStorage.getItem('token')
                        }
                    }).then(response => {
                        this.status = response.status;
                        console.log("resposta: " + response.json())
                        return response.json()
                      })
                    .then(
                        this.state.call_back()
                    
                    )
            /* this.setState({
                delete : "Apagar"
            }) */
        }

    }

    finalize(){
        if (this.state.finTime == "Finalizar"){
            console.log("Fim?");
            this.setState({
                delete : "Apagar",
                finTime : "Confirmar"
            })

        } else {
            console.log("Fim!");
            let d = new Date();
            let data = {"time_end" : d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes()}
             fetch(url + '/update_task/'+this.id , {
                        method: 'PUT',
                        headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + localStorage.getItem('token')
                        },
                        body : JSON.stringify(data)
                    })
            this.setState({
                finTime : "Finalizar",
                time_end : d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes()
            })
        }
    }


    render() {
        return (
            <ListGroupItem className="p-3" >

            <Card small className="mb-4">
                <CardHeader  className="border-bottom" style={{backgroundColor: 'rgb(234, 164, 102)'}}>
                    <Row form>
                        <Col md="5" xs="5">
                            <h3 className="m-0" style={{color:'white'}}>{this.state.title}</h3>
                        </Col>
                        <Col md="2" xs="2">
                        {this.checkVality(this.state.time_end) && <Button outline size="md" theme="secondary" className="mb-2" id={"id_button_"} onClick={this.editar_toggle} ><i className="material-icons mr-1" >assignment</i> <b id={"id_button_msg_"}></b>{this.state.edit}</Button>}
                        </Col>
                        <Col md="2" xs="2">
                            {this.state.submitBottom}
                        </Col>
                        <Col md="2" xs="2">
                        {this.checkVality(this.state.time_end) && <Button size="md" theme="danger" className="mb-2" id={"id_button_"} onClick={this.deletetask} ><i className="material-icons mr-1" >delete</i> <b id={"id_button_delete_"}></b>{this.state.delete}</Button>}
                        </Col>
                    </Row>
                    </CardHeader>
                <ListGroup flush >
                    <ListGroupItem className="p-3">
                        <Row>
                            <Col>
                                <Form>
                                    <Row form>
                                       
                                        <Col md="3" xs="6" className="form-group">
                                            <label htmlFor={"idTWorker_"}>Responsável</label>
                                            <FormInput
                                                id={"idTWorker_"}
                                                placeholder="Worker"
                                                value={this.state.worker}
                                                onChange={() => { }}
                                                type="text"
                                                
                                                
                                            />
                                        </Col>
                                        
                                        <Col md="8" xs="6" className="form-group">
                                            <label style={{color:"white"}} >Finalizar</label>
                                            {this.checkVality(this.state.time_end) && <Button style={{float: "right"}} size="bg" theme="warning" className="mb-2" id={"id_button_finalize_"} onClick={this.finalize} ><i className="material-icons mr-1" >done</i> <b id={"id_button_delete_"}></b>{this.state.finTime}</Button>}
                                        </Col>
                                    </Row>
                                    {<Row form>
                                        {/* First Name */}
                                        {this.state.submitBottom !== null && 
                                            <Col md="5" className="form-group">
                                            <label htmlFor={"idTDesc_" + this.id}>Descrição</label><i className="material-icons mr-1" >edit3</i>
                                            <FormInput
                                                id={"idTDesc_" + this.id}
                                                placeholder="Description"
                                                value={this.state.description}
                                                onChange={(e) => { this.changeDescription(e.target.value)}}
                                            />
                                        </Col>
                                        }
                                        { this.state.submitBottom === null && 
                                             <Col md="5" className="form-group">
                                             <label htmlFor={"idTDesc_" + this.id}>Descrição</label> 
                                             <FormInput
                                                 id={"idTDesc_" + this.id}
                                                 placeholder="Description"
                                                 value={this.state.description}
                                                 onChange={() => { }}
                                                 
                                             />
                                         </Col>
                                        }
                                        {/* Last Name */}
                                        <Col md="3" xs="6" className="form-group">
                                            <label htmlFor={"idTBegin_"}>Início</label>
                                            <FormInput
                                                id={"idTBegin_"}
                                                placeholder="Task Begin"
                                                value={this.state.time_start}
                                                onChange={() => { }}
                                            />
                                        </Col>
                                        <Col md="3" xs="6" className="form-group">
                                            <label htmlFor={"idTFinish_"}>Fim</label>
                                            <FormInput
                                                id={"idTFinish_"}
                                                placeholder="Task End"
                                                value={this.state.time_end}
                                                onChange={() => { }}
                                            />
                                        </Col>
                                    </Row>
                                    }
                                </Form>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>

            </Card>
        </ListGroupItem>

        )
    }

};

BabyTask.propTypes = {
    /**
     * The component's title.
     */
    title: PropTypes.string
};


export default BabyTask;
