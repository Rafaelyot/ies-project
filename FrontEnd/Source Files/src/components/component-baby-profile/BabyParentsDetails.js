import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import Error from "../../rest/Error";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button,
  Container,
  Alert
} from "shards-react";

class BabyParentsDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      title: props.title,
      mom_name: "",
      dad_name: "",
      mom_contact: "",
      dad_contact: "",
      notification_class: "mb-0 bg-danger",
      notifications: null
    }

    this.get_parent = this.get_parent.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.error_valuer = new Error();

  }

  componentDidMount() {
    this.get_parent(this.props.id);
  }

  get_parent(id) {
    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }
    let babyInfo;
    console.log("here parents " + id)
    return fetch(url + '/get_parents_by_baby/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {
       this.status = response.status;

        return  response.json()
      })
      .then(data => {

        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        console.log(error)
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }


        babyInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
        console.log("my array " + babyInfo.length)
        if (babyInfo != null && babyInfo.length != 0) {
          if (babyInfo[0].sex == 'M') {
            this.setState({
              mom_name: babyInfo[1].name,
              dad_name: babyInfo[0].name,
              mom_contact: babyInfo[1].phone_number.split("+")[1],
              dad_contact: babyInfo[0].phone_number.split("+")[1],
            })
          } else {
            this.setState({
              mom_name: babyInfo[0].name,
              dad_name: babyInfo[1].name,
              mom_contact: babyInfo[0].phone_number.split("+")[1],
              dad_contact: babyInfo[1].phone_number.split("+")[1],
            })
          }
        }


      })
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }

  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class} style={{borderRadius:"10px"}}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }


  render() {
    return (
      <>
        {this.state.notifications !== null && this.renderNotifications()}
        <Card small className="mb-4">
          <CardHeader className="border-bottom" style={{ backgroundColor: '#007bff' }}>
            <h6 className="m-0" style={{ color: 'white' }}>{this.props.title}</h6>
          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      {/* First Name */}
                      <Col md="5" className="form-group">
                        <label htmlFor="ifFName">Nome do Pai</label>
                        <FormInput
                          id="ifFName"
                          placeholder="Father's Name"
                          value={this.state.dad_name}
                          onChange={() => { }}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="3" xs="6" className="form-group">
                        <label htmlFor="idFContact">Contacto</label>
                        <FormInput
                          id="idFContact"
                          placeholder="Father's Contact"
                          value={this.state.dad_contact}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    {<Row form>
                      {/* First Name */}
                      <Col md="5" className="form-group">
                        <label htmlFor="ifMName">Nome da Mãe</label>
                        <FormInput
                          id="ifMName"
                          placeholder="Mother's Name"
                          value={this.state.mom_name}
                          onChange={() => { }}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="3" xs="6" className="form-group">
                        <label htmlFor="idMContact">Contacto</label>
                        <FormInput
                          id="idMContact"
                          placeholder="Mother's Contact"
                          value={this.state.mom_contact}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    }
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
          <ListGroup>

          </ListGroup>
        </Card>
      </>
    )
  }

};

BabyParentsDetails.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

BabyParentsDetails.defaultProps = {
  title: "Informações dos Progenitores"
};

export default BabyParentsDetails;
