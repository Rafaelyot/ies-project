import React, { Component } from "react";
import PropTypes from "prop-types";
import { token, url } from "../../rest/Const";
import Error from "../../rest/Error";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button,
  Container,
  Alert
} from "shards-react";

class BabyStuffDetails extends Component {

  constructor(props) {
    super(props)

    //this.get_baby = this.get_baby.bind(this) // Using Function.prototype.bind in render creates a new function each time the component renders, which may have performance implications (see below).

    this.state = {
      title: "props.title",
      fname: "Primeiro Nome do Bebé",
      lname: "Segundo Nome do Bebé",
      bheight: "Nome do Pai",
      bweight: "Contacto do Pai",
      bbirth: "Data de Nascimento",
      bsex: "Sexo do Bebé",
      notification_class: "mb-0 bg-danger",
      notifications: null
    }

    this.get_baby = this.get_baby.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.dismiss_notification = this.dismiss_notification.bind(this);
    this.error_valuer = new Error();

  }

  componentDidMount() {
    this.get_baby(this.props.id);
  }


  get_baby(id) { // TODO: veririfcar se da NULL, pk lança erro de syntax acho

    if (id == null) {
      this.setState({
        notification_class: "mb-0 bg-danger",
        notifications: "Erro inesperado"
      })
      return
    }


    let babyInfo;
    console.log("Stuff baby" + id)
    return fetch(url + '/get_baby/' + id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('token')
      },
    })
      .then(response => {
        this.status = response.status;
        
        return response.json()
      })
      .then(data => {
        let status = this.status;

        let error = this.error_valuer.evaluate_status(status, data['message']);
        
        if (error != null) {
          this.setState({
            notification_class: "mb-0 bg-danger",
            notifications: error
          })
          return false
        }

        babyInfo = JSON.parse(JSON.parse(JSON.stringify(data["data"])))
        let babyDate = babyInfo.birth_date
        let dateD = babyDate.split("T")[0].split("-")
        let dateH = babyDate.split("T")[1].split("Z")[0]
        console.log("d: " + dateH)
        this.setState({
          fname: babyInfo.name.split(" ")[0],
          lname: babyInfo.name.split(" ")[1],
          bheight: babyInfo.height,
          bweight: babyInfo.weight,
          bbirth: dateD[2] + "-" + dateD[1] + "-" + dateD[0] + " " + dateH,
          bsex: babyInfo.sex == "M" ? "Masculino" : "Feminino"

        })

      })
  }

  dismiss_notification() {
    this.setState({
      notifications: null
    })
  }

  renderNotifications() {

    return (
      <Container fluid className="px-0">
        <Alert className={this.state.notification_class} style={{borderRadius:"10px"}}>
          <i className="fa fa-info mx-2"></i> {this.state.notifications}
          <i className="fas fa-times pull-right" style={{ float: 'right' }} onClick={this.dismiss_notification} ></i>
        </Alert>
      </Container>
    )
  }

  render() {
    return (
      <>
        {this.state.notifications !== null && this.renderNotifications()}
        <Card small className="mb-4">
          <CardHeader className="border-bottom" style={{ backgroundColor: '#007bff' }}>
            <h6 className="m-0" style={{ color: 'white' }}>{this.props.title}</h6>
          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      {/* First Name */}
                      <Col md="5" xs="6" className="form-group">
                        <label htmlFor="idBFirstName">Primeiro Nome</label>
                        <FormInput
                          id="idBFirstName"
                          placeholder="First Name"
                          value={this.state.fname}
                          onChange={() => { }}
                        />
                      </Col>
                      {/* Last Name */}
                      <Col md="5" xs="6" className="form-group">
                        <label htmlFor="idBLastName">Last Name</label>
                        <FormInput
                          id="idBLastName"
                          placeholder="Last Name"
                          value={this.state.lname}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    {<Row form>

                      <Col md="2" xs="3" className="form-group">
                        <label htmlFor="idBH">Altura (cm)</label>
                        <FormInput
                          type="text"
                          id="idBH"
                          placeholder="Babys Height"
                          value={this.state.bheight}
                          onChange={() => { }}
                        />
                      </Col>
                      <Col md="2" xs="3" className="form-group">
                        <label htmlFor="idBW">Peso (g)</label>
                        <FormInput
                          type="text"
                          id="idBW"
                          placeholder="Babys Weight"
                          value={this.state.bweight}
                          onChange={() => { }}
                        />
                      </Col>
                      <Col md="2" xs="6" className="form-group">
                        <label htmlFor="idBBirth">Data de Nascimento</label>
                        <FormInput
                          type="text"
                          id="idBBirth"
                          placeholder="Babys BirthDate"
                          value={this.state.bbirth}
                          onChange={() => { }}
                        />
                      </Col>
                      <Col md="2" xs="4" className="form-group">
                        <label htmlFor="idBSex">Sexo do Bébe</label>
                        <FormInput
                          type="text"
                          id="idBSex"
                          placeholder="Babys Sex"
                          value={this.state.bsex}
                          onChange={() => { }}
                        />
                      </Col>
                    </Row>
                    }
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
          <ListGroup>

          </ListGroup>
        </Card>
      </>
    )

  }

};

BabyStuffDetails.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

BabyStuffDetails.defaultProps = {
  title: "Informações Básicas"
};

export default BabyStuffDetails;
