import React, { Component } from "react";
import AddBaby from "../../rest/Baby"
import "react-datepicker/dist/react-datepicker.css";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Button
} from "shards-react";


export default class CompleteForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      submitted : false
    }
    this.query_addBaby = this.query_addBaby.bind(this)
  }



  async query_addBaby(event) {

    let addBaby_Event = new AddBaby().add_baby(event)

    this.setState(
      { data: await addBaby_Event ,
      submitted : true
      }
    )



    }




  render() {

    return (
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <Row>
            <Col>
              <Form onSubmit={this.query_addBaby} action="#" >
                <Row form>
                  <Col md="12" className="form-group">
                    <label htmlFor="feName">Nome < span style={{ color: 'red' }} >*</span> </label>
                    <FormInput
                      name="name"
                      id="feName"
                      type="text"
                      placeholder="Nome"
                    />
                  </Col>
                  <Col md="6">
                    <label htmlFor="feBirth_date">Data de Nascimento < span style={{ color: 'red' }} >*</span> </label>
                    <FormInput
                      name="birth_date"
                      id="feBirth_date"
                      type="text"
                      placeholder="Date de nascimento"
                    />
                  </Col>
                  <Col md="6">
                    <label htmlFor="feSex">Género < span style={{ color: 'red' }} >*</span> </label>
                    <FormSelect id="feSex" name="sex">
                      <option>Choose...</option>
                      <option>Feminino</option>
                      <option>Masculino</option>
                    </FormSelect>

                  </Col>
                </Row>

                <FormGroup>
                  <label htmlFor="feAddress">Morada</label>
                  <FormInput id="feAddress" placeholder="Morada" name="address" />
                </FormGroup>

                <Row form>
                  <Col md="6" className="form-group">
                    <label htmlFor="feWeight">Peso < span style={{ color: 'red' }} >*</span> </label>
                    <FormInput id="feWeight" placeholder="Peso" name="weight" />
                  </Col>
                  <Col md="6" className="form-group">
                    <label htmlFor="feHeight">Altura < span style={{ color: 'red' }} >*</span> </label>
                    <FormInput id="feHeight" placeholder="Altura" name="height">
                    </FormInput>
                  </Col>
                </Row>
                <Button type="submit">Adicionar Bebe</Button>
              </Form>
            </Col>
          </Row>
        </ListGroupItem>
      </ListGroup>
    )

  }
}

//const CompleteFormExample = () => (
//  <ListGroup flush>
//    <ListGroupItem className="p-3">
//      <Row>
//        <Col>
//          <Form onSubmit={e => { add_baby(e) }} action="#" >
//            <Row form>
//              <Col md="12" className="form-group">
//                <label htmlFor="feName">Nome < span style={{ color: 'red' }} >*</span> </label>
//                <FormInput
//                  name="name"
//                  id="feName"
//                  type="text"
//                  placeholder="Nome"
//                />
//              </Col>
//              <Col md="6">
//                <label htmlFor="feBirth_date">Data de Nascimento < span style={{ color: 'red' }} >*</span> </label>
//                <FormInput
//                  name="birth_date"
//                  id="feBirth_date"
//                  type="text"
//                  placeholder="Date de nascimento"
//                />
//              </Col>
//              <Col md="6">
//                <label htmlFor="feSex">Género < span style={{ color: 'red' }} >*</span> </label>
//                <FormSelect id="feSex" name="sex">
//                  <option>Choose...</option>
//                  <option>Feminino</option>
//                  <option>Masculino</option>
//                </FormSelect>
//
//              </Col>
//            </Row>
//
//            <FormGroup>
//              <label htmlFor="feAddress">Morada</label>
//              <FormInput id="feAddress" placeholder="Morada" name="address" />
//            </FormGroup>
//
//            <Row form>
//              <Col md="6" className="form-group">
//                <label htmlFor="feWeight">Peso < span style={{ color: 'red' }} >*</span> </label>
//                <FormInput id="feWeight" placeholder="Peso" name="weight" />
//              </Col>
//              <Col md="6" className="form-group">
//                <label htmlFor="feHeight">Altura < span style={{ color: 'red' }} >*</span> </label>
//                <FormInput id="feHeight" placeholder="Altura" name="height">
//                </FormInput>
//              </Col>
//            </Row>
//            <Button type="submit">Adicionar Bebe</Button>
//          </Form>
//        </Col>
//      </Row>
//    </ListGroupItem>
//  </ListGroup>
//);


//CompleteFormExample.propTypes = {
//
//  error_number: PropTypes.string,
//  message: PropTypes.string
//
//};
//
//CompleteFormExample.defaultProps = {
//
//  error_number: "500",
//  message: "Erro"
//};

// export default CompleteFormExample