import React, { Component } from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainNavbar from "../components/layout/MainNavbar/MainNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import MainFooter from "../components/layout/MainFooter";
import LoginQuery from "../rest/LoginQuery";
class DefaultLayout extends Component {
  constructor(props) {
    super(props);

    this.LoginQuery = new LoginQuery();

  }

  async componentDidMount() {
    let response_data = await this.LoginQuery.login_status();
    
    let status = response_data[0];
    let data = response_data[1];
    //
    localStorage.setItem("login",JSON.stringify(status != 401))
    
    
    this.forceUpdate()
  

  }
  render() {
    const { children, noNavbar, noFooter } = this.props;
    return (
      <Container fluid>
        <Row>
          <MainSidebar />
          <Col
            className="main-content p-0"
            lg={{ size: 10, offset: 2 }}
            md={{ size: 9, offset: 3 }}
            sm="12"
            tag="main"
          >
            {!noNavbar && <MainNavbar />}
            {children}
            {!noFooter && <MainFooter />}
          </Col>
        </Row>
      </Container>
    )
  }
}


DefaultLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default DefaultLayout;
