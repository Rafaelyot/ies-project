import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import Graphics from "./views/Graphics";
import UserProfileLite from "./views/UserProfileLite";
import AddNewBaby from "./views/AddNewBaby";
import Errors from "./views/Errors";
import ComponentsOverview from "./views/ComponentsOverview";
import Babies from "./views/Babies";
import BlogPosts from "./views/BlogPosts";
import Login from "./views/Login";
import Logout from "./views/Logout";
import Video from "./video/Video";
import BabyProfile from "./views/BabyProfile";
import Default from "./components/layout/MainNavbar/NavbarSearch";
import MainPage from "./components/mainPage/MainPage";
import BabyTask from "./components/component-baby-profile/BabyTask";
import AddWorker from "./views/AddWorker";
import AddParent from "./views/AddParent";
import AddRoom from "./views/AddRoom";
import AddBed from "./views/AddBed";
import ParentProfile from "./views/ParentProfile"
import BabyChart from "./views/BabyChart";
import AllVideos from "./views/AllVideos";
import Chart_room from "./views/Chart_room";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/main_page" />
  },
  {
    path: "/main_page",
    layout: DefaultLayout,
    component: MainPage
  },
  {
    path: "/graphics",
    layout: DefaultLayout,
    component: Graphics
  },
  //{
  //  path: "/user-profile-lite",
  //  layout: DefaultLayout,
  //  component: UserProfileLite
  //},
  {
    path: "/add-new-baby",
    layout: DefaultLayout,
    component: AddNewBaby
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  },
  {
    path: "/login",
    layout: DefaultLayout,
    component: Login
  },
  {
    path: "/logout",
    layout: DefaultLayout,
    component: Logout
  },
  {
    path: "/video",
    layout: DefaultLayout,
    component: Video
  },
  {
    path: "/baby/profile",
    layout: DefaultLayout,
    component: BabyProfile
  },
  {
    path: "/babies",
    layout: DefaultLayout,
    component: Babies
  },
  {
    path: "/add_worker",
    layout: DefaultLayout,
    component: AddWorker
  }
  ,
  {
    path: "/add_parent",
    layout: DefaultLayout,
    component: AddParent
  },
  {
    path: "/add_room",
    layout: DefaultLayout,
    component: AddRoom
  },
  {
    path: "/add_bed",
    layout: DefaultLayout,
    component: AddBed
  },
  {
    path: "/video_baby",
    layout: DefaultLayout,
    component : Video
  },
  {
    path: '/parent/profile',
    layout: DefaultLayout,
    component: ParentProfile
  },
  {
    path: '/chart_per_baby',
    layout: DefaultLayout,
    component : BabyChart
  },
  {
    path: '/all_videos',
    layout : DefaultLayout,
    component : AllVideos
  },
 // {
 //   path: '/chart_room',
 //   layout : DefaultLayout,
 //   component : Chart_room
 // }
];
