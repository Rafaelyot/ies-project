
import React, { Component } from "react";
import PageTitle from "../components/common/PageTitle";
import Error from "../rest/Error";
import AddBaby from "../rest/Baby";
import Parent from "../rest/Parent";
import Room from "../rest/Room";
import Bed from "../rest/Bed";
import Autosuggest from 'react-autosuggest';
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Button,
  Container,
  DatePicker
} from "shards-react";



export default class AddNewBaby extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error_number: 500,
      message: "Erro",
      submitted: false,
      feedback_message: "Algo correu mal !",
      date: new Date(),
      floors: [],
      selected_floor: -1,
      rooms_by_floor: {},
      select_room_number: -1,
      render_room_number: false,
      render_bed: false,
      available_beds: [],
      selected_bed: -1,
      value: "",
      parents: []

    }
    this.query_addBaby = this.query_addBaby.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.renderFloor = this.renderFloor.bind(this);
    this.fetch_rooms = this.fetch_rooms.bind(this);
    this.renderRoomNumber = this.renderRoomNumber.bind(this);
    this.onChange = this.onChange.bind(this);
    this.get_available_beds = this.get_available_beds.bind(this);
    this.onChangeAuto = this.onChangeAuto.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
  }



  async query_addBaby(event) {

    let addBaby_Event = await new AddBaby().add_baby(event)

    const status = addBaby_Event[0]
    const data = addBaby_Event[1]



    let feedback_msg = this.state.feedback_message;
    if (status == 200) {
      feedback_msg = "Sucesso !"
      window.location.href = '/'
    }

    this.setState(
      {
        error_number: status,
        message: data['message'],
        submitted: true,
        feedback_message: feedback_msg
      }
    )
    localStorage.setItem("token", data['token'])

  }

  handleDateChange(value) {
    this.setState({
      ...this.state,
      ...{ date: new Date(value) }
    });
  }

  async fetch_rooms() {
    let room_obj = new Room();
    let all_rooms = await room_obj.get_all_rooms();
    let status = all_rooms[0];
    let data = all_rooms[1];

    let rooms = JSON.parse(data['data']);

    let floors = [];
    let rooms_by_floor = {}
    rooms.map((room) => {
      if (!floors.includes(room.floor)) {
        floors.push(room.floor)
        rooms_by_floor[room.floor] = []
      }

      rooms_by_floor[room.floor].push(room.room_number)
    })

    floors.sort();

    this.setState({
      floors: floors,
      rooms_by_floor: rooms_by_floor,
    })


  }

  async get_available_beds() {
    let bed_obj = new Bed();
    const floor = this.selected_floor;
    const room_number = this.select_room_number;
    let available_beds = await bed_obj.get_available_beds_by_floor_and_number(floor, room_number);
    let status = available_beds[0];
    let data = available_beds[1];

    let beds = JSON.parse(data['data']);

    let beds_id = []
    beds.map((bed) => {
      beds_id.push(bed.id)
    })
    beds_id.sort();
    if (beds_id.length > 0)
      this.setState({
        available_beds: beds_id
      })

    this.no_beds = false


  }

  componentDidMount() {
    this.fetch_rooms();
    this.no_beds = true;
  }

  onChange(event) {
    //render_room_number: false,
    //  render_bed : false
    let target = event.target;
    let id = target.id;
    let value = target.value
    let value_id = target.options[target.selectedIndex].id;


    if (id == "fePiso") {

      if (value == "Choose...") {
        this.setState({
          render_room_number: false,
          render_bed: false,
          available_beds: []
        })
        this.selected_floor = -1
        this.select_room_number = -1
        this.selected_bed = -1
      }
      else {
        this.setState({
          render_room_number: true,
          available_beds: []
        })
        this.selected_floor = value_id;
      }
    } else if (id == "feRoomNumber") {

      if (value == "Choose...") {
        this.setState({
          render_bed: false,
        })
        this.select_room_number = -1;
        this.selected_bed = -1;
      }
      else {
        this.setState({
          render_bed: true,
          available_beds: []
        })
        this.select_room_number = value_id
      }
    } else {
      if (value == "Choose...") {
        this.selected_bed = -1
      } else {
        this.selected_bed = value_id

      }


    }

  }

  renderSuggestion(suggestion, { query }) {
    //const suggestionText = `${suggestion.first} ${suggestion.last}`;
    const parentName = suggestion.name;
    const parentId = suggestion.id;
    
    return (
      <span className="suggestion-content">
        <span className="name">
          {
            <span key={parentId}>{parentName}</span>
          }
        </span>
      </span>
    );
  }


  getSuggestionValue(parent) {
    console.log(parent)
    return parent;
  }

  async onSuggestionsFetchRequested({ value }) {


    let parent_obj = new Parent();

    const all_data = await parent_obj.get_parents(value);

    const status = all_data[0];
    const data = all_data[1];
    


    let values = []

    JSON.parse(data['data']).map((p) => {
      
      values.push({ "name": p.name, "id": p.id })
    })


    this.setState({ parents: values })
    console.log(values)
    
  };


  onSuggestionsClearRequested() {
    this.setState({
      parents: []
    });
  };


  onChangeAuto(event, { newValue }) {
    this.setState({
      value: newValue
    });
  }

  renderParentForm() {

    const value = this.state.value;

    const inputProps = {
      placeholder: 'Search...',
      value,
      onChange: this.onChangeAuto
    };

    return (
      <Col md="4">
        <label htmlFor="feParents">Progenitores < span style={{ color: 'red' }} >*</span> </label>
        <Autosuggest
          suggestions={this.state.parents}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          inputProps={inputProps}
        //theme = {theme}
        />
      </Col>
    )
  }


  renderFloor() {

    return (
      <Col md="4">
        <label htmlFor="fePiso">Piso < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="fePiso" name="piso" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.floors.map((floor) => {
              return (
                <option key={floor} id={floor}>
                  {floor}º Piso
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }

  renderRoomNumber() {
    return (
      <Col md="4">
        <label htmlFor="feRoomNumber">Numero do Quarto < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="feRoomNumber" name="roomNumber" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.rooms_by_floor[this.selected_floor].map((room_number) => {
              return (
                <option key={room_number} id={room_number}>
                  Quarto {room_number}
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }

  renderBed() {

    if (this.state.available_beds.length == 0 || this.no_beds)
      this.get_available_beds();
    return (
      <Col md="4">
        <label htmlFor="feBed">Numero da Cama < span style={{ color: 'red' }} >*</span> </label>
        <FormSelect id="feBed" name="Bed" onChange={this.onChange}>
          <option >Choose...</option>
          {
            this.state.available_beds.map((bed_id) => {
              return (
                <option key={bed_id} id={bed_id}>
                  Cama {bed_id}
                </option>
              )
            })

          }
        </FormSelect>
      </Col>
    )
  }



  renderQueryInfo() {
    return (
      <Col lg="12" md="12">
        <Error error_number={this.state.error_number}
          message={this.state.message} feedback_message={this.state.feedback_message}
          page_url='/add-new-baby' />
      </Col>
    )
  }

  renderForm() {
    return (
      <Col lg="12" md="12">
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form onSubmit={this.query_addBaby} action="#" >
                  <Row form>
                    <Col md="12" className="form-group">
                      <label htmlFor="feName">Nome < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput
                        name="name"
                        id="feName"
                        type="text"
                        placeholder="Nome"
                      />
                    </Col>
                    <Col md="6">
                      <label htmlFor="feBirth_date">Data de Nascimento < span style={{ color: 'red' }} >*</span> </label>
                      <br></br>
                      <DatePicker
                        name="birth_date"
                        id="feBirth_date"
                        dropdownMode="select"
                        size="md"
                        md="12"
                        selected={this.state.date}
                        onChange={this.handleDateChange}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={1}
                        timeCaption="time"
                        dateFormat="yyyy-MM-d h:mm"
                      />

                    </Col>
                    <Col md="6">
                      <label htmlFor="feSex">Género < span style={{ color: 'red' }} >*</span> </label>
                      <FormSelect id="feSex" name="sex">
                        <option>Choose...</option>
                        <option>Feminino</option>
                        <option>Masculino</option>
                      </FormSelect>

                    </Col>
                  </Row>

                  <FormGroup>
                    <label htmlFor="feAddress">Morada</label>
                    <FormInput id="feAddress" placeholder="Morada" name="address" />
                  </FormGroup>

                  <Row form>
                    <Col md="6" className="form-group">
                      <label htmlFor="feWeight">Peso < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput id="feWeight" placeholder="Peso" name="weight" />
                    </Col>
                    <Col md="6" className="form-group">
                      <label htmlFor="feHeight">Altura < span style={{ color: 'red' }} >*</span> </label>
                      <FormInput id="feHeight" placeholder="Altura" name="height">
                      </FormInput>
                    </Col>

                    {this.renderFloor()}
                    {this.state.render_room_number && this.renderRoomNumber()}
                    {this.state.render_bed && this.renderBed()}
                    {this.renderParentForm()}
                  </Row>
                  <br></br>
                  <Button type="submit">Adicionar Bebe</Button>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Col>
    )
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">

        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Adicionar Novo Bebe" subtitle="Baby Care" className="text-sm-left" />
        </Row>

        <Row>

          {!this.state.submitted && this.renderForm()}
          {this.state.submitted && this.renderQueryInfo()}

        </Row>
      </Container>
    )
  }

}

