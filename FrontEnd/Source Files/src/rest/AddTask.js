import getDataForm from "./getDataForm"
import { Component } from 'react';
import { token, url } from "../rest/Const";



export default class AddTask extends Component {
    constructor(props) {
        super(props);
    }

    async add_task(event) {
        event.preventDefault();

        let data = getDataForm(event.target)
        
        data['baby'] = localStorage.getItem('baby_id_prof')

        console.log("add new task "+data['baby'])
        
        const response = await fetch(url + '/add_task', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body: JSON.stringify(data),
        })

        const status = await response.status
        const json = await response.json()
        return [status, json]


    }

}