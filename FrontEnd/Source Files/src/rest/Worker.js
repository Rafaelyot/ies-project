import { Component } from 'react';
import { token, url } from "./Const";

export default class Worker extends Component {
    constructor(props) {
        super(props);
    }

    async add_worker(data) {

        if (data['sex'] === 'Feminino')
            data['sex'] = 'F'
        else if (data['sex'] === 'Masculino')
            data['sex'] = 'M'
            
        const response = await fetch(url + '/add_worker', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body: JSON.stringify(data),
        })

        const status = await response.status
        const json = await response.json()
        return [status, json]


    }

}
