import getDataForm from "./getDataForm"
import { Component } from 'react';
import { token, url } from "./Const";

export default class Room extends Component {
    constructor(props) {
        super(props);
    }

    async   get_all_rooms() {

        const response = await fetch(url + '/get_available_rooms', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            }
        })
        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token", json['token'])

        return [status, json]


    }

    async get_rooms() {
        const response = await fetch(url + '/get_rooms', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            }
        })
        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token", json['token'])

        return [status,json]

    }

    async add_room(data) {
        const response = await fetch(url + '/add_room', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body: JSON.stringify(data),
        })
        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token", json['token'])

        return [status, json]
    }



}
