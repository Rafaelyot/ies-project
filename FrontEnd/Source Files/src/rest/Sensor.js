import { Component } from 'react';
import { token, url } from "./Const";

export default class Sensor extends Component {
    constructor(props) {
        super(props);
    }

    async latest_values(sensor,bed,n) {

        const response = await fetch(url + '/last_values_per_baby/'+sensor+'/'+bed+'/'+n+'/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            
        })

        const status = await response.status
        const json = await response.json()
        return [status, json]


    }

}
