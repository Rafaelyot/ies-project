import getDataForm from "./getDataForm"
import { Component } from 'react';
import { token, url } from "./Const";

export default class Baby extends Component {
    constructor(props) {
        super(props);
    }

    async add_baby(data) {
        //event.preventDefault();

        //let data = getDataForm(event.target)

        if (data['sex'] === 'Feminino')
            data['sex'] = 'F'
        else if (data['sex'] === 'Masculino')
            data['sex'] = 'M'

        const response = await fetch(url + '/add_baby', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body: JSON.stringify(data),
        })

        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token",json['token'])
        return [status, json]


    }

    async get_babies(start, end) {

        const response = await fetch(url + '/get_babies/' + start + "/" + end, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },

        })

        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token",json['token'])
        return [status, json]


    }

    async get_baby(id) {
        const response = await fetch(url + '/get_baby/' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },

        })

        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token",json['token'])
        return [status, json]
    }




}
