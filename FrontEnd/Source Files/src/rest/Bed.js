import getDataForm from "./getDataForm"
import { Component } from 'react';
import { token, url } from "./Const";

export default class Bed extends Component {
    constructor(props) {
        super(props);
    }

    async get_available_beds_by_floor_and_number(floor,room_number) {

        let data = {
            'floor' : floor,
            'room_number' : room_number
        }

        const response = await fetch(url + '/get_available_beds_by_floor_and_number/'+floor+"/"+room_number, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            }
        })

        
        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token",json['token'])
        return [status, json]


    }


    async add_bed(data) {
        const response = await fetch(url + '/add_bed', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body : JSON.stringify(data)
        })

        
        const status = await response.status
        const json = await response.json()
        if (status === 200)
            localStorage.setItem("token",json['token'])
        return [status, json]


    }

}
