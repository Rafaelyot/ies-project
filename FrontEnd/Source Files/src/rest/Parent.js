import { Component } from 'react';
import {  url } from "./Const";

export default class Parent extends Component {
    constructor(props) {
        super(props);
        //this.get_parents = this.get_parents.bind(this);
    }

    async add_parent(data) {

        if (data['sex'] === 'Feminino')
            data['sex'] = 'F'
        else if (data['sex'] === 'Masculino')
            data['sex'] = 'M'
            
        const response = await fetch(url + '/add_parent', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            body: JSON.stringify(data),
        })

        const status = await response.status
        const json = await response.json()
        return [status, json]


    }

    async get_parents(name) {

    
        const response = await fetch(url + '/get_parentByName/'+name, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
            
        })

        const status = await response.status
        const json = await response.json()
        return [status, json]


    }

}
