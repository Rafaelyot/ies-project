import getDataForm from "./getDataForm"
import { Component } from 'react';
import { url } from "./Const";

export default class LoginQuery extends Component {
    constructor(props) {
        super(props);
    }

    async login(event) {
        event.preventDefault();

        let data = getDataForm(event.target)

        const response = await fetch(url + '/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })

        const status = await response.status
        const json = await response.json()

        if (status === 200)
            localStorage.setItem("token",json['token'])

        return [status, json]



    }

    async login_status() {

        const response = await fetch(url + '/login_status', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + localStorage.getItem('token')
            },
        })

        const status = await response.status
        const json = await response.json()
        
        return [status, json]



    }

    

}
