import logging
import json
from influxdb import InfluxDBClient
from flask import Flask, request, g as variables
from credentials import *


logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)


@app.route('/add_data', methods=['POST'])
def add_data():
    data = json.loads(request.data)

    logging.info(f"Received new data: {data}")

    data_to_add = []
    for sample in data:
        data_to_add.append({
            "measurement": sample["sensor"],
            "time": sample["time"],
            "fields": {key: sample[key] for key in sample if key != "time" and key != "sensor"}
        })

    add_status = variables.client_db.write_points(data_to_add)

    return {
        'status': add_status,
    }


@app.before_request
def connect_db():
    variables.client_db = InfluxDBClient(host=db_url, port=db_port)

    if db_name not in [db['name'] for db in variables.client_db.get_list_database()]:
        variables.client_db.create_database(db_name)

    variables.client_db.switch_database(db_name)


# TODO -> FAZER UM MECANISMO PARA VERIFICAR O SENDER

if __name__ == '__main__':
    app.run(port=5001, host='0.0.0.0')
