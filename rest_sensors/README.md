# Criação das imagens docker necessárias
```bash
$ sudo docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /home/renatogroffe/Desenvolvimento/Portainer/data:/data portainer/portainer     # caso ainda não se possua o portainer
$ docker build -t=sensors_rest .
$ docker run influxdb
```


# Executar localmente sem ser em docker
```bash
$ virtualenv venv
$ source venv/bin/activate
$ python3 app.py
```
