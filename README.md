# BabyCare
#### Sistema para monitorização recém-nascidos em ambiente de Pediatria

## Índice
 1. [Objetivos](#objs)
 1. [*User stories*](#user_stories)
 1. [Arquitetura](#arq)
    1. [*Sensors representation*](#sensors_repr)
    1. [*Server to receive data from the sensors*](#sensors_midd)
    1. [*Server REST API for Sensor Middleware*](#sensors_rest)
    1. [Base de dados](#database)
    1. [*Service API for Presentation Layer*](#rest)
    1. [*Presentation Layer*](#presentation)
 1. [Servidor](#server)
 1. [Cliente](#client)
   1. [Escolha da tecnologia](#client_tec)
   1. [Funcionalidades](#client_func)
 1. [*Deployment*](#dep)  
 1. [Diretórios](#dirs)
 1. [Equipa](#team)
 1. [Considerações finais](#conclusion)
 



<a name="objs"></a>

## Objetivos
 - Esta plataforma visa sobretudo:
   - Auxiliar funcionários no cuidado de recém-nascidos;
   - Monitorizar simultaneamente diferentes camas e quartos, garantindo a boa regularização das mesmas
   - Manter registo dos bebés e sua relação quer com entidades paternais, quer logísticas (camas, temperatura, etc.)



<a name="user_stories"></a>

## *User stories*
 - A partir dos objetivos acima mencionados, procedemos à criação de algumas *user stories*, de forma a priorizar as tarefas e requisitos do projeto. Desta maneira, de seguir são apresentadas as *user stories* que criamos na primeira iteração do projeto:
   1. **A auxiliar pretende saber qual a temperatura, a humidade e a pressão do ar na sala onde se encontram repousados os bebés** 
      - A auxiliar dirige-se à aplicação do telemóvel ou ao website, seleciona uma das salas, e verifica os dados relativos do ambiente da sala, de forma a verificar se estes se encontram nos limites aceitáveis.
   2. **Definir limites (máximos e mínimos) para os valores detetados pelos sensores** 
      - O gestor pretende definir um limite máximo e mínimo de quando a aplicação a deve notificar as enfermeiras de anomalias nestes dados, para que consigam de forma mais facilitada saber quando devem intervir.
      - Para isso, o gestor entra na aplicação/web interface, carrega no botão de definições, carrega em definir limites das salas e preenche o formulário com os limites de temperatura, humidade e pressão sobre os quais pretende ser notificada.
   3. **Estado de resolução de um problema** 
      - Quando recebe uma notificação, a enfermeira visiona o valor se encontra fora do normal, seleciona esta e clica no botão de indicar que vai controlar a situação (de forma a impedir que outro funcionário também vá desnecessariamente).
   4. **Registar um bebé** 
      - Tendo ocorrido um nascimento, e após os devidos cuidados essenciais ao bem estar da criança no imediato, é altura de preparar o ambiente para os próximos dias.
      - Desta forma, a aplicação apresenta nitidamente uma opção para registar o mais recente bebé, a qual é selecionada.
      - É então apresentada uma página de registo, onde se devem inserir os dados do mesmo, dos quais se pode destacar nome próprio de dos progenitores, sexo, etc;
      - Finalmente, todos estes parâmetros são verificados, sendo apresentada um preview dos mesmos bem como pedida confirmação.
      - Com tudo em ordem, o novo paciente entra assim no sistema.
   5. **Inserção de dados relativas a tarefas/ações para um bebe** 
      - A enfermeira quer registar que o bebé, tomou banho a um dada hora.
      - Abre a aplicação.
      - Seleciona o bebé em causa.
      - Procura pela linha temporal do bebe, a qual mais uma vez se seleciona.
      - Aqui, indica-se a hora a que o banho foi dado, eventuais notas (por exemplo, o bebé ter sido “resmungão”) e finalmente clica-se em guardar.
   6. **Sistema de scheduling, capaz de agilizar futuras tarefas para bebes** 
      - Havendo a necessidade de controlar a hora a que um bebé deve tomar medicação, é possível fazê-lo; acedendo primeiramente à página do bebé.
      - De seguida, seleciona-se a opção de agendar tarefa
      - Aberta uma nova página, começa-se então por definir os parâmetros relativos a esta atividade.
      - Assim atribui-se um título à mesma, uma descrição esclarecedora, e também obviamente, os vários horários às quais esta deve ser realizada.
      - Estes horários estão relacionados com os dias em que devem ser cumpridos, também estes definíveis.
      - Finalmente, é possível definir o intervalo de antecedência em que a enfermeira deve ser notificada de modo a lembrar-se desta obrigação.
      - Com tudo, pronto, confirma-se a agenda.
   7. **Progressão do estado de um recém-nascido** 
      - Cada bebé deve ter um histórico do seu estado. Ao aceder a esse histórico é possível ver informações básicas como Nome, Idade, eventuais problemas/observações, e sala onde se encontra.
      - Ao aceder a este histórico, seleciona-se a opção de observar progresso.
      - Após esta seleção são apresentados, por defeito, vários gráficos onde se mostra, ao longo do tempo, os níveis registados de cada parâmetro medido do bebê.
      - É possível filtrar dados, para uma melhor visualização, de parâmetros individuais, ou num intervalo de tempo.
   8. **Averiguar condições do Quarto** 
      - Ao selecionar uma dada sala, é também possível selecionar a opção de observar o seu histórico.
      - Mais uma vez estão disponíveis vários formatos de visualização de dados, adaptados à preferência de cada utilizador.
      - Adicionalmente, pode alternar entre a fonte de dados, isto é, média das camas ou do sensor para sala mesmo
   
   - Após termos criado estas histórias, introduzimos as mesmas na plataforma [Pivotaltracker](https://www.pivotaltracker.com/dashboard), que tinha sido sugerida pelo professor da disciplina como uma boa ferramenta para fazer o *`Backlogging`* do trabalho. Cada uma destas histórias foi, nesta plataforma, dividida em requisitos mais pequenos e fazíveis numa iteração mínima do trabalho e, a cada um destes, foi definido um ou vários *owners*, isto é, as pessoas responsáveis por realizar essa *feature*, as tarefas que eram necessárias concretizar para que esse requisito estivesse completo, o nível de díficuldade e o nível de prioridade da tarefa. 
   - Desta forma, foi-nos possível usar algumas metodologias **`agile`** de forma a conseguir organizar o trabalho e as tarefas da melhor maneira possível.
   - Apesar de inicialmente termos usado a plataforma `Pivotaltracker` para fazer o *`Backlogging`*, o período gratuito da aplicação findou a duas semanas da concretização do projeto, pelo que tivemos de ser forçados a migrar todas as tarefas a serem feitas para as `Boards do GitLab`.
   - De seguida, é possível vislumbrar a forma como organizamos o nosso *`Backlog`* em cada uma das plataformas acima descritas:
   ![pivotaltracker_backlog](readme_images/pivotaltracker_backlog.png)
   <center>Backlog no Pivotaltracker</center>
   </br>

   ![gitlab_backlog](readme_images/gitlab_backlog.png)
   <center>Backlog no GitLab</center>



<a name="arq"></a>

## Arquitetura
 - Tal como a estrutura proposta no guião do projeto descrevia, também decidimos usar uma arquitetura *multilayer*. Contudo, apesar da do guião apresentar uma *`mobile app`*, por questões de gestão de tempo, não tivemos a possibilidade de implementar uma.
 - É de notar também que todas as ferramentas usadas, com exceção do **`Django`**, nunca tinham sido usadas por nenhum membro integrante do grupo, pelo que foi necessário aprender a usar as mesmas neste trabalho.
 - Apresenta-se de seguida um esquema geral da arquitetura e das ferramentas usadas:
 ![arquitetura](readme_images/Arquitetura.png)
 <center>Desenho da arquitetura</center>
 </br>


<a name="sensors_repr"></a>

### *Sensors representation*
 - Por uma questão de simplicidade e de falta de tempo, decidimos que era desnecessário usarmos sensores físicos neste trabalho. Desta forma, criamos um *script* em **python**, `data_generator/sensors/sensors.py`, que tem como intuito a geração de dados dos vários tipos de sensores que fazem sentido para esta implementação (ou seja, temperatura, humidade, infra vermelhos e som, para cada cama de bebé escolhida como argumento, ou para cada quarto escolhido em argumento (andar e número do quarto)).
 - Exemplo da utilização do *script* de virtualização (é necessária a utilização de um ambiente virtual com os *requirements* instalados):
 ```bash
 $ virtualenv venv                              # criar um ambiente virtual
 $ source venv/bin/activate                     # usar o ambiente virtual criado
 $ pip install -r requirements.txt              # instalar os requirements necessários

 $ python3 sensors.py -h                        # obter os argumentos que podem ser passados e os valores que estes esperam
usage: sensors.py [-h] (-t | -u | -i | -s) -x MAX -n MIN [-f FLOOR] [-r ROOM] [-b BED]

Script to simulate sensors

optional arguments:
  -h, --help            show this help message and exit
  -t, --temperature     Simulate temperature sensor
  -u, --humidity        Simulate humidity sensor
  -i, --infra_reds      Simulate infra reds sensor
  -s, --sound           Simulate sound sensor
  -x MAX, --max MAX     Max value that sensor can mesure
  -n MIN, --min MIN     Min value that sensor can mesure
  -f FLOOR, --floor FLOOR
                        Floor where the sensor is placed
  -r ROOM, --room ROOM  Room where the sensor is placed
  -b BED, --bed BED     Bed where the sensor is placed
 
 $ python3 sensors.py -t -x 40 -n 10 -b 3       # gerar dados de temperatura aleatórios com valores entre 10 e 40 para o bebé de id 3
 ```

 - Num itervalo de 1 em 1 segundo, este script envia um novo valor do sensor escolhido para o quarto/bebé escolhido para o *`middleware`*.


<a name="sensors_midd"></a>

### *Server to receive data from the sensors*
 - Para os dados gerados pelos sensores poderem chegar à base de dados principal, foi necessário implementar um *`middleware`*, para fazer essa ligação entre os sensores e o servidor principal. Ora, este *`middleware`* tem um papel muito importante, porque permite evitar que os dados dos sensores sejam constantemente enviados para o servidor principal, que é o responsável por todas as comunicações com os usuários normais da plataforma, pelo que não seria agradável que este servidor estivesse constantemente a ser bombardeado com mensagens dos sensores.
 - Sendo assim, este *`middleware`* é responsável por tratar de receber a grande quantidade de dados que os sensores enviam continuamente, de os armazenar numa *`queue`* e de esvaziar esta quando chega a um determinado número de mensagens guardadas e enviar estas, duma só vez, para o servidor principal. A *`queue`* foi, no nosso trabalho, implementada usando **`RabbitMQ`**, que é um *message broker* que permite armazenar mensagens numa *queue*.
 - Para permitir que os sensores comuniquem com ele, o *`middleware`* foi desenvolvido usando **`Flask`**, que é um *framework* que permite a criação de serviços RESTful lightweight, sendo esta ultima caracteristica a principal razão para usarmos esta ferramenta neste caso, já que só precisavamos de criar uma REST API com um único caminho para onde os sensores pudessem enviar os dados.
 - Já a comunicação com o servidor principal foi feita usando `python requests`.
 - É também de frisar que o middleware foi feito de forma a que, caso se decidisse por algum motivo, passar-se a usar sensores fisicos, esta mudança fosse suportada, deste que os dados enviados dos sensores estivessem em formato *json*, algo que é possível, por exemplo, usando *arduino*.


<a name="sensors_rest"></a>

### *Server REST API for Sensor Middleware*
 - Como descrito no ponto acima, quando o *`middleware`* se apercebe que a *queue* possui um número elevado de mensagens armazenadas, este manda-as todas duma vez para o servidor principal. Ora, no servidor principal, criamos uma REST API só para tratar da recessão destes dados e para os armazenar na base de dados. 
 - Decidimos não incluir este caminho na REST API principal, para haver uma separação de responablidades, isto é, a REST API principal faz apenas a ligação entre o utilizador normal e a camada de dados, enquanto que esta REST API trata de fazer a ligação entre toda a componente da arquitetura ligada aos sensores e a mesma camada de dados.
 - Pela mesma razão que decidimos usar **`Flask`** para criar o `middleware`, decidimos também usar esta ferramenta para criar esta REST API, já que também esta só possui um caminho.



<a name="database"></a>

### Base de dados

 - Apesar de inicialmente termos começado a implementação do sistema usando apensas uma base de dados relacional, **`MySQL`**, ao discutirmos o modelo da base de que estavamos a usar, deparamo-nos com um problema: as tabelas responsáveis por armazenar os dados dos sensores tinham de armazenar os dados temporalmente. Contudo, usar parâmetros deste género como chave nunca é ideal numa base de dados **`MySQL`**, já que é penosamente ineficiente. Desta feita, falamos com o professor Carlos Costa, já que é do nosso conhecimento a sua sapiência na área de Bases de Dados, sendo que este nos recomendou usar uma `base de dados de séries temporais`, já que estas são extremamente eficientes a resolver o problema descrito. Recomendou-nos também a utilização da base de dados **`InfluxDB`**, muito usada na área do tratamento de dados de sensores, pelo que esta foi a nossa opção quando implementamos o sistema.
 - Desta forma, o nosso sistema possui uma base de dados **`MySQL`**, onde são armazenados, relacionalmente, todas as entidades do nosso sistema, como os quartos, os bebés ou os funcionários, e uma base de dados **`InfluxDB`**, onde são armazenados todos os dados vindos dos sensores (dados estes correlacionados com as entidades da base de dados relacional (quarto e bebé)) por tempo.
 - Abaixo, é possivel observar o diagrama Entidade Relação da base de dados relacional:
 ![bd](readme_images/ER.png)
 <center>Diagrama da base de dados relacional</center>
 </br>


<a name="rest"></a>

### *Service API for Presentation Layer*
 - Este serviço é o responsável por comunicar com a aplicação do lado do cliente e de, a partir das ações deste, criar, remover, alterar ou obter dados das bases de dados. Para a sua criação, foi usado **`Django`**, já que é uma ferramenta bastante poderosa, com uma documentação rica, uma comunidade de programadores que a usam bastante vasta e por ser uma *framework* para *python*, que foi a linguagem de eleição do grupo neste projeto.


<a name="presentation"></a>

### *Presentation Layer*
 - É a componente do projeto responsável por fornecer conteudo visivel ao utilizador e interagir com este, sendo que usa a REST API acima descrita para permitir ao utlizador manusear os dados existentes nas bases de dados do sistema. Foi usada a ferramenta **`React`** a desenvolver.



<a name="server"></a>

## Servidor


* **EndPoints**

  [Rest API documentation](http://www.babypediacare.com/rest_doc.html)

* **EndPoints para obter dados dos sensores**
  - **Obter os nomes dos sensores existentes**
  - **Obter os últimos n valores dum sensor por nome do sensor e por cama**
   - Exemplo: [localhost:8000/last_values_per_baby/humidity/1/3](localhost:8000/last_values_per_baby/humidity/1/3) (o ultimo valor é o número de dados a obter e o penúltimo é o id da cama)
 - **Obter os últimos n valores dum sensor por nome do sensor e por quarto(número do andar e o número do quarto no andar)**
   - Exemplo: [localhost:8000/last_values_per_room/humidity/1/1/10](localhost:8000/last_values_per_room/humidity/1/1/10) (o ultimo valor é o número, o penultimo é o room e o antepenultimo é o floor)
 - **Obter os últimos n valores de todos os sensores por cama**
   - Exemplo: [localhost:8000/all_last_values_per_baby/1/10/](localhost:8000/all_last_values_per_baby/1/10/) (o ultimo valor é o número de valores a serem obtidos e o penultimo é o id da cama)
 - **Obter os últimos n valores de todos os sensores por quarto**
   - Exemplo: [localhost:8000/all_last_values_per_room/1/1/10/](localhost:8000/all_last_values_per_room/1/1/10/) (o ultimo é o número de dados a obter, o penultimo é o id do quarto e o antepenultimo é o id do andar)




<a name="client"></a>

## Cliente


<a name="client_tec"></a>

### Escolha da tecnologia

 - Inicialmente, a *Presentation Layer* ia ser desenvolvida em *Thymeleaf*, mas como uma das ideias principais é ter uma aplicação web e android, então de forma a desenvolver código reutilizável, preferiu-se utilizar `React`, um framework capaz de desenvolver uma aplicação  web *single-page*, sendo fácil a sua conversão para android através da transição do react web para react native.


<a name="client_func"></a>

### Funcionalidades

 - Sistema de login, onde um admin consegue aceder ao sistema e ter acesso a todas as funcionalidades do site. Caso não esteja logado, não poderá realizar qualquer operação.

 ![login](readme_images/login.png)

 - Barra de pesquisa com *auto complete* capaz de procurar por todas as entidades do nosso sistema, como po exemplo, bebé, funcionário... . Ao clicar na pessoa que procura, é feito o redirecionamento para o seu respetivo perfil.

 ![search](readme_images/search.png)

 - Registo de tarefas associados a um funcionário a realizar sobre um bebé. Onde se regista a data e hora a que vai se iniciar a respetiva tarefa.

 ![task](readme_images/task.png)

 - *CRUD* das várias entidades:
    * Adicionar e remover bebé
    * Adicionar, atualizar e eliminar um funcionário
    * Adicionar e atualizar um progenitor
    * Adicionar um quarto e uma cama
    * Adicionar, editar e eliminar tarefas.

 - Zona de *live stream* de cada bebé, ou seja, consegue-se observar um vídeo que transmite em direto o bebé, caso a câmara esteja ligada ao servidor. A ideia é ter uma câmara por cama onde está o respetivo bebé e transmitir para o sistema.

![streaming](readme_images/stream.png)
 
 - Gráficos que ilustra os vários valores recebidos dos sensores em *live*. Estes envia-nos informação sobre temperatura, humidade, radiação infravermelha e ruído sonoro de cada quarto.



<a name="dep"></a>

## *Deployment*
 - Apesar do professor da disciplina nos ter fornecido uma máquina virtual dos STIC, esta revelou-se demasiado lenta para as nossas necessidades.
 - Desta forma, decidimos distribuir cada um dos três principais componentes da nossa plataforma (sensores, *application tier* e *presentation tier*) para três servidores diferentes
 - A *presentation tier* ficou hospedada no serviço português [Amen](https://www.amen.pt/hosting/): [babyCare](http://www.babypediacare.com/)
 - Já a virtualização dos sensores, o seu middleware e *application tier* ficaram respetivamente em duas máquinas virtuais do serviço de hosting da **Amazon**, [aws](https://aws.amazon.com/), já que era do nosso interesse aprender como usar esta plataforma, e sendo que por sermos estudantes, podemos usufruir da utilização de máquinas virtuais gratuitas, decidimos que era o melhor momento para aprender a usar este serviço:
 ![aws_console](readme_images/aws_console.png)
 <center>Consola da AWS</center>

 ![aws_console](readme_images/tmux.png)
 <center>Acesso local por ssh ás máquinas virtuais para fazer deployment</center>

 - Para testar, é possível aceder á REST API principal através do url [rest](http://ec2-54-92-239-252.compute-1.amazonaws.com:8000)

 - É importante também realçar que foram criados *docker containers* para poder fazer um deployment mais facilitado. Os ficheiros de configuração encontram-se em cada um dos diretórios dos componentes que necessitaram dos mesmos, acompanhados dum ficheiro README a explicar como fazer o deployment dos mesmos.

 - Credenciais de acesso:
   - username: *admin*
   - password: *admin*



<a name="dirs"></a>

## Diretórios
 - *application_tier*
   - Implementação da REST API principal em **`django`**
 - *data_generator*
   - Implementação do sistema de virtualização dos sensores, do *middleware* e da geração de entidades para a base de dados relacional
 - *Frontend*
   - Implementação da aplicação do cliente em **`React`**
 - *readme_images*
   - Images deste relatório
 - *rest_sensors*
   - Implementação da REST API que recebe os dados dos sensores



<a name="team"></a>

## Equipa
### *Team Manager*
 - Rafael Simões
   - O que fez:
     - Backend
     - Frontend
     - Unit tests
     - Arquitetura da base de dados

### *Architect*
 - Pedro Escaleira
   - O que fez:
     - Sensores
     - DevOps
     - Backend
     - Arquitetura
     - Arquitetura da base de dados

### *Architect*
 - Pedro Escaleira
   - O que fez:
     - Sensores
     - DevOps
     - Backend
     - Arquitetura
     - Arquitetura da base de dados

### *Product Owner*
 - Luis Fonseca
   - O que fez:
     - Geração de dados
     - Frontend
     - Análise de requisitos
     - Arquitetura da base de dados

### *DevOps Master*
 - André Alves
   - O que fez:
     - Frontend


<a name="conclusion"></a>

## Considerações finais 
 - Apesar de termos tido bastantes dificuldades, principalmente na coordenação e gestão da equipa e por um dos colegas do grupo ter estado fora durante duas semanas de trabalho, ficamos satisfeitos com o resultado final. Apesar de querermos ter implementado mais requisitos, como por exemplo a aplicação de telemóvel conseguimos, no geral, ter um produto final desenvolvido em múltiplas plataformas.
 - Foi também bastante importante no que toca ao nosso desenvolvimento enquanto engenheiros de software, já que foi necessário colocar em prática alguns dos conceitos usados nesta indústria hoje em dia.
