import requests
import json
import sys
import datetime

## login
r = requests.post('http://127.0.0.1:8000/login', data={"username":"admin", "password": "admin123"})

temp_token = r.json()["token"]


## auth
headers = {'Content-Type': 'application/json', 'Authorization': 'Token ' + temp_token}
## add room
with open("SQLroom.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://localhost:8000/add_room', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]


# add_bed
with open("SQLbed.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://localhost:8000/add_bed', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]

## add_worker
with open("SQLworker.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://localhost:8000/add_worker', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]

# add_parent
with open("SQLparent.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://localhost:8000/add_parent', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]

# add_baby
with open("SQLbaby.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://127.0.0.1:8000/add_baby', headers=headers, data =  json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]


### add_task
with open("SQLtask.txt", "r") as file01:
    for line in file01:
        data2 = json.loads(line)
        r = requests.post('http://127.0.0.1:8000/add_task', headers=headers, data =  json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]
