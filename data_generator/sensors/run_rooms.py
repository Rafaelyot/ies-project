import requests
import json

import subprocess
from multiprocessing import Pool


url = 'http://localhost:8000'
r = requests.post(f'{url}/login', data={"username":"rafael", "password": "ies2019bb"})

temp_token = r.json()["token"]


## auth
headers = {'Content-Type': 'application/json', 'Authorization': 'Token ' + temp_token}


def get_all_rooms():
    rooms = []
    print("Getting all rooms",end="\tCode status:")
    r = requests.get(f'{url}/get_rooms', headers=headers)
    print(r.status_code)
    temp_token = r.json()["token"]
    for row in json.loads(r.json()['data']):
        rooms.append((row['floor'],row['room_number']))
        #babies_ids.append()
    
    return rooms

all_rooms = get_all_rooms()

def run_temperature(b):
    command_temperature = f'python3 sensors.py -t -x 30 -n 15 -f {b[0]} -r {b[1]}'
    subprocess.Popen(command_temperature, shell=True)

def run_sound(b):
    command_sound = f'python3 sensors.py -s -x 40 -n 10 -f {b[0]} -r {b[1]}'
    subprocess.Popen(command_sound, shell=True)

def run_infrared(b):
    command_sound = f'python3 sensors.py -i -x 50 -n 10 -f {b[0]} -r {b[1]}'
    subprocess.Popen(command_sound, shell=True)


def run_humidity(b):
    command_sound = f'python3 sensors.py -u -x 90 -n 30 -f {b[0]} -r {b[1]}'
    subprocess.Popen(command_sound, shell=True)

pool = Pool()
pool.map(run_temperature, all_rooms)
pool.map(run_sound, all_rooms)
pool.map(run_infrared, all_rooms)
pool.map(run_humidity, all_rooms)