
# middleware url to add sensors data
URL = "http://localhost:5000/add_sensors_data"

# logger file name
LOGGER_FILE = "sensors.log"

# sensor sleep time
TIME_SLEEP = 1

# constants for sensors
PROBABILITY_CHANGE = {
    "temperature": 0.1,
    "humidity": 0.1,
    "infra_reds": 0.1,
    "sound": 0.1,
}
SENSOR_SUM_MAX = {
    "temperature": 2,
    "humidity": 2,
    "infra_reds": 2,
    "sound": 2,
}
SENSOR_SUM_MIN = {
    "temperature": -2,
    "humidity": -2,
    "infra_reds": -2,
    "sound": -2,
}
