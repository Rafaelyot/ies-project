"""
https://www.rabbitmq.com/tutorials/tutorial-two-python.html
sudo apt install rabbitmq-server
pip3 install pika
pip3 install rabbitmq
sudo service rabbitmq-server restart


sudo rabbitmqctl add_user myUser
rabbitmqctl set_user_tags myUser administrator
cd /etc/rabbitmq/
sudo nano rabbitmq-env.conf
"""

import random
import time
import argparse
import logging
import sys
import requests
import json

from settings import *

logging.basicConfig(level=logging.INFO, 
                    filename=LOGGER_FILE, 
                    filemode='a', 
                    format='%(name)s - %(levelname)s - %(message)s')


def send_message(sensor, value, floor, room, bed=None): 
    data = {
        "sensor": sensor,
        "value": value,
        "floor": floor,
        "room": room,
    }

    if bed:
        data['bed'] = bed

    logging.info(f"Sending new data to middleware: {data}")
    try: 
        return True, json.loads(requests.post(URL, json=data).content)['message']
    except Exception as e:
        return False, e


def generate_data(min_value, max_value, sensor_name, floor, room, bed=None) -> None:
    """
    :param generated values are derived from this initial value:
    :param max: then base value can be hit
    :param min: then base value can be hit
    :param name: used to identified the sensor in messages
    :return: Nothing
    """

    try:
        current_temp = random.uniform(min_value, max_value)
        while True:
            if random.random() < PROBABILITY_CHANGE[sensor_name]:
                temperature_to_sum = random.uniform(SENSOR_SUM_MIN[sensor_name], SENSOR_SUM_MAX[sensor_name])
                if min_value < current_temp + temperature_to_sum < max_value:
                    current_temp += temperature_to_sum

            response_status, response_message = send_message(
                sensor_name, 
                current_temp,
                floor, 
                room,
                bed,
            )
            if response_status:
                logging.info(response_message)
            else:
                logging.error(response_message)

            time.sleep(TIME_SLEEP)
    except Exception as e:
        logging.error(f"Runtime error: {e}")
        return 


def main(args):
    sensor_name = ""
    if args.temperature:
        sensor_name = "temperature"
    elif args.humidity:
        sensor_name = "humidity"
    elif args.infra_reds:
        sensor_name = "infra_reds"
    elif args.sound:
        sensor_name = "sound"

    generate_data(args.min, args.max, sensor_name, args.floor, args.room, args.bed)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to simulate sensors')

    # define which sensor to simulate
    sensors_parser = parser.add_mutually_exclusive_group(required=True)
    sensors_parser.add_argument('-t', '--temperature', help='Simulate temperature sensor', action="store_true")
    sensors_parser.add_argument('-u', '--humidity', help='Simulate humidity sensor', action="store_true")
    sensors_parser.add_argument('-i', '--infra_reds', help='Simulate infra reds sensor', action="store_true")
    sensors_parser.add_argument('-s', '--sound', help='Simulate sound sensor', action="store_true")

    # define measure range
    parser.add_argument('-x', '--max', type=int, help='Max value that sensor can mesure', required=True)
    parser.add_argument('-n', '--min', type=int, help='Min value that sensor can mesure', required=True)

    # define which floor and room (and possibly which bed)
    entity = parser.add_mutually_exclusive_group(required=True)
    entity.add_argument('-f', '--floor', type=int, help='Floor where the sensor is placed')
    parser.add_argument('-r', '--room', type=int, help='Room where the sensor is placed')
    entity.add_argument('-b', '--bed', type=int, help='Bed where the sensor is placed')

    args = parser.parse_args()
    if args.room and args.bed:
        print("You cant select a bed and a room at the same time")
    else:
        main(args)
