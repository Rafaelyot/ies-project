import requests
import json

import subprocess
from multiprocessing import Pool


url = 'http://localhost:8000'
r = requests.post(f'{url}/login', data={"username":"rafael", "password": "ies2019bb"})

temp_token = r.json()["token"]


## auth
headers = {'Content-Type': 'application/json', 'Authorization': 'Token ' + temp_token}


def get_all_babies():
    babies_ids = []
    print("Getting all babies",end="\tCode status:")
    r = requests.get(f'{url}/get_babies/0/0', headers=headers)
    print(r.status_code)
    temp_token = r.json()["token"]
    for row in json.loads(r.json()['data']):
        babies_ids.append(row['id'])
    
    return babies_ids

all_babies = get_all_babies()

def run_temperature(b):
    command_temperature = f'python3 sensors.py -t -x 30 -n 15 -b {b}'
    subprocess.Popen(command_temperature, shell=True)

def run_sound(b):
    command_sound = f'python3 sensors.py -s -x 40 -n 10 -b {b}'
    subprocess.Popen(command_sound, shell=True)

pool = Pool()
pool.map(run_temperature, all_babies)
pool.map(run_sound, all_babies)