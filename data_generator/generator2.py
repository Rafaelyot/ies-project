import string
from datetime import date
import random
import time
import sys
import requests
import json
url = 'http://ec2-54-92-239-252.compute-1.amazonaws.com:8000'

r = requests.post(f'{url}/login', data={"username":"admin", "password": "admin"})

temp_token = r.json()["token"]


## auth
headers = {'Content-Type': 'application/json', 'Authorization': 'Token ' + temp_token}


def compareDatas(d1, d2):
    sd1 = d1.split("-")
    sd2 = d2.split("-")
    # print(sd1 , " -- " , sd2)
    if (int(sd1[0]) > int(sd2[0])):
        return True
    elif (int(sd1[1]) > int(sd2[1])):
        return True
    elif (int(sd1[2]) > int(sd2[2])):
        return True
    return False

def str_time_prop(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))

def random_date(start="2015-1-1 1:00", end="2019-12-11 23:00"):
    return str_time_prop(start, end, '%Y-%m-%d %H:%M', random.random())

def insert(table_name, parameters, values):

    string = "{ "
    for i in range(len(parameters)):
        # print(">>>", values[i], values[i] is list, isinstance(values[i], list))
        temp_value = str((("\"" + (values[i]) + "\"") if not (isinstance(values[i], list) or str(values[i]).isdigit()) else values[i]))
        string += "\"" + (parameters[i]) + "\"" + " : " + temp_value

        if not i == len(parameters) - 1:
            string += ", "
    return string + " }"

def get_all_parents():
    parents_ids = []
    print("Getting all parents",end="\tCode status:")
    r = requests.get(f'{url}/get_parents/0/0', headers=headers)
    print(r.status_code)
    temp_token = r.json()["token"]
    for row in json.loads(r.json()['data']):
        parents_ids.append(row['id'])
    
    return parents_ids


def get_all_babies():
    babies_ids = []
    print("Getting all babies",end="\tCode status:")
    r = requests.get(f'{url}/get_babies/0/0', headers=headers)
    print(r.status_code)
    temp_token = r.json()["token"]
    for row in json.loads(r.json()['data']):
        babies_ids.append(row['id'])
    
    return babies_ids


def get_all_workers():
    workers_ids = []
    print("Getting all babies",end="\tCode status:")
    r = requests.get(f'{url}/get_workers/0/0', headers=headers)
    print(r.status_code)
    temp_token = r.json()["token"]
    for row in json.loads(r.json()['data']):
        workers_ids.append(row['id'])
    
    return workers_ids
# Configurations
person_no = 125
worker_no = 50
parent_no = 50
baby_no = parent_no // 2
task_no = baby_no * 3
room_no = 50
bed_no = 50

assert worker_no + parent_no + baby_no == person_no

print("-------ROOM------------")
# room_floor = []
# room_number = []
room_dict = {}

for room in range(room_no):

    temp_floor = random.randint(1, 3)
    # room_floor.append(temp_floor)

    if temp_floor not in room_dict:
        room_dict[temp_floor] = [0]
    else:
        room_dict[temp_floor].append(room_dict[temp_floor][-1] + 1)

# print("Room floor: ", room_floor)
# print("Room number: ", room_number)

print("Rooms (Floor: Number): ", room_dict)


print("END-------ROOM------------\n")

print("-------BED------------")
bed_room = []
for bed in range(bed_no):
    print(">>", len(room_dict))
    bed_room.append(random.randint(1, len(room_dict) ))

print("Beds Room: ", bed_room)
print("END-------BED------------\n")

print("-------PERSON------------")
person_f_nomes = ["João", "José", "Maria", "Luís", "Miguel", "Rafael", "Rafaela", "Pedro", "Rita", "Inês", "Marta",
                  "Margarida", "Francisca", "Leonor", "Ana", "Lara", "Alice", "Mafalda", "Helena", "Teresa", "Carla", "Filipa",
                  "Soraia", "Rosa", "Vera", "Santiago", "Rodrigo", "Dinis", "Tiago", "Diogo", "Guilherme", "Vasco",
                  "Luís", "Isaac", "Kevin", "Bruno", "Manuel", "Micael", "Artur", "Igor", "Edgar"]

person_l_nomes = ["Silva", "Santos", "Ferreira", "Pereira", "Oliveira", "Costa", "Rodrigues", "Martins", "Jesus",
                  "Sousa", "Fernandes", "Gonçalves", "Gomes", "Marques", "Almeira", "Ribeiro", "Pinto", "Carvalho",
                  "Teixeira", "Moreira", "Rocha"]

person_street_i = ['Rua de', 'Rua', "Estrada", "Entrada", "Travessa", "Bairro", "Penedo", "Street da"]
person_street_f = ["Piedade", "Principal", "Grilo", "Serragem", "Creoulo", "Malta", "Social", "Santiago"]

index_Person = 0
person_name = []
person_birth = []
person_sex = []
person_address = []

while index_Person < person_no:

    ## Name
    nome = random.choice(person_f_nomes) + " " + random.choice(person_l_nomes)
    person_name.append(nome)

    ## Birth_Date
    if index_Person > person_no - baby_no - 1:
        temp_birth = random_date('2018-01-01 00:01')
    else:
        temp_birth = random_date('1975-04-18 00:04', '1999-09-11 00:04')
    person_birth.append(temp_birth)

    ## Sex
    person_sex.append(random.choice(["M", "F"]))

    ## Address
    person_address.append(random.choice(person_street_i) + " " + random.choice(person_street_f))

    index_Person += 1

print(person_name)
print(person_birth)
print(person_sex)
print(person_address)

print("END-----PERSON-----\n")

print("-----PARENT-----")
index_parent = 0
parent_contact = []

for parent in range(parent_no):
    parent_contact.append("2" + "3" + "2" + ''.join(random.choice(string.digits) for i in range(6)))
print(parent_contact)
print("END-----PARENT-----\n")

print("-----WORKER-----")
index_worker = 0
worker_contact = []
worker_role = []

worker_roles = ['Médico(a)', 'Enfermeiro(a)', "Auxiliar"]

for worker in range(worker_no):
    worker_role.append(random.choice(worker_roles))
    worker_contact.append( "2"+"3" + "3"+ ''.join(random.choice(string.digits) for i in range(6)))

print(worker_contact)
print(worker_role)

print("END-----WORKER-----\n")

print("-----BABY-----")

index_baby = 0
baby_weight = []
baby_height = []
baby_bed = []
baby_parent_id = {}

available_beds = [i for i in range(bed_no)]
last_beds = []

for baby in range(person_no - baby_no , person_no):
    baby_weight.append(random.randint(330, 370))
    baby_height.append(random.randint(47, 60))
    # print(person_no - baby_no - parent_no, person_no - baby_no)
    temp_parents = [i for i in range(person_no - baby_no - parent_no, person_no - baby_no ) if i % 2 != 0]

    if len(available_beds) == 0:
        available_beds = last_beds[:]
        last_beds = []
    t_bed = (available_beds.pop( random.randint(1, len(available_beds) - 1 ) ))
    last_beds.append(t_bed)
    baby_bed.append(t_bed)

    baby_parent_id[baby] = [temp_parents.pop(random.randint(0, len(temp_parents) - 1))]
    baby_parent_id[baby] += [baby_parent_id[baby][0] + 1]

print(baby_weight)
print(baby_height)
print(baby_bed)
print(baby_parent_id)

print("END-----BABY-----\n")

index_task = 0
task_worker_id = []
task_baby_id = []
task_title = []
task_description = []
task_date_start = []
task_date_end = []

descriptions = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consectetur gravida sapien nec imperdiet.",
                "In hac habitasse platea dictumst. Fusce porta venenatis congue. Fusce tempor tincidunt dictum.",
                "Mauris id pellentesque tortor. Aenean faucibus felis nec feugiat pretium. Mauris ac dolor sed purus tincidunt bibendum.",
                " Nulla dapibus, mauris quis egestas gravida, mauris justo scelerisque diam, sed ornare lacus est quis sem.",
                " Donec porttitor pretium ex, vel malesuada enim tincidunt vel. Suspendisse eu gravida velit. ",
                "Nunc blandit, mi id lobortis eleifend, justo lacus placerat elit, id iaculis mi lacus sit amet nisi."
                ]

titles_1 = ["Banho", "Massagem", "Embalar", "Cuidar", "Dormir", "Assustar", "Soluçar", "Tratar", "Amamentar", "Alimentar", "Observar"]
titles_2 = ["Bebe", "Lijeiro", "Profudamente", "Profundo", "Urgente", "Necessário", "Perigoso", "Inquietante", "Lamentável", "Preciso"]


for task in range(task_no):

    task_worker_id.append(random.randint(0, person_no - baby_no - parent_no - 1) + 1)

    temp_baby_id = random.randint(person_no - baby_no , person_no - 1) + 1
    task_baby_id.append(temp_baby_id)

    task_title.append(random.choice(titles_1) + " " + random.choice(titles_2))

    task_description.append(random.choice(descriptions))

    if len(task_date_start) == 0:
        temp_last_data = person_birth[temp_baby_id - 1]
    else:
        temp_last_data = task_date_start[-1]

    to_finish = random_date(temp_last_data)
    task_date_start.append(to_finish)

    temp_day = int(to_finish.split(" ")[0].split("-")[2])
    temp_month = int(to_finish.split(" ")[0].split("-")[1])
    temp_year = int(to_finish.split(" ")[0].split("-")[0])
    temp_hour = int(to_finish.split(" ")[1].split(":")[0])
    temp_min = int(to_finish.split(" ")[1].split(":")[1])

    temp_min += random.randint(2, 15)
    if temp_min >= 60:
        temp_min = random.randint(2, 8)
        temp_hour += 1
    if temp_hour >= 24:
        temp_hour = 0
        temp_day += 1

    raised = False
    if temp_day == 28:
        temp_day = 1
        temp_month += 1
        raised = True
    if temp_month == 12 and raised:
        temp_month = 1
        temp_year += 1

    finish_date = str(temp_year) + "-" + str(temp_month).zfill(2) + "-" + str(temp_day).zfill(2) + " " + str(temp_hour).zfill(2) + ":" + str(temp_min).zfill(2)

    task_date_end.append(finish_date)


print(task_worker_id)
print(task_baby_id)
print(task_title)
# print(task_description)
print(task_date_start)
print(task_date_end)

print("END-----TASK-----\n")


print("-------ROOM>>>>>")
with open("SQLroom.txt", "w") as file01:
    for key in room_dict:
        for i in room_dict[key]:
            # print(insert("add_room", ("floor", "room_number"), (key, i)) )
            row = (insert("add_room", ("floor", "room_number"), (key, i)) )
            file01.write(row)
            file01.write("\n")
            data2 = json.loads(row)
            r = requests.post(f'{url}/add_room', headers=headers, data = json.dumps(data2))
            print(r.status_code, r.json())
            temp_token = r.json()["token"]

print("-------BED>>>>>")
bed_counter = 0
with open("SQLbed.txt", "w") as file01:
    for key in room_dict:
        for i in room_dict[key]:
            bed_counter+=1
        # print(insert("add_bed", ("room",), (bed_room[bed],) ) )
            row = (insert("add_bed", ("floor", "room_number"), (key, i)) )
            file01.write(row)
            file01.write("\n")
            data2 = json.loads(row)
            r = requests.post(f'{url}/add_bed', headers=headers, data = json.dumps(data2))
            print(r.status_code, r.json())
            temp_token = r.json()["token"]
            

print("-------PARENT>>>>>")
print(person_no - parent_no - baby_no, person_no - baby_no)
parent_controler = 0
with open("SQLparent.txt", "w") as file01:
    for i in range(person_no - parent_no - baby_no, person_no - baby_no ):
        # print( insert("add_parent", ("name", "birth_date", "sex", "address", "phone_number"), (person_name[i], person_birth[i], person_sex[i], person_address[i], "+" + parent_contact[parent_controler]  ) ) )
        row = ( insert("add_parent", ("name", "birth_date", "sex", "address", "phone_number"), (person_name[i], person_birth[i], person_sex[i], person_address[i], "+" + parent_contact[parent_controler]  ) ) )
        file01.write(row)
        file01.write("\n")
        data2 = json.loads(row)
        r = requests.post(f'{url}/add_parent', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]
        parent_controler += 1

print("-------WORKER>>>>>")
with open("SQLworker.txt", "w") as file01:
    for i in range(worker_no):
        # print( insert("insert_worker", ("name", "birth_date", "sex", "address", "phone_number", "role"), (person_name[i], person_birth[i], person_sex[i], person_address[i], "+" + worker_contact[i], worker_role[i] )  ) )
        row = ( insert("insert_worker", ("name", "birth_date", "sex", "address", "phone_number", "role"), (person_name[i], person_birth[i], person_sex[i], person_address[i], "+" + worker_contact[i], worker_role[i] )  ) )
        file01.write(row)
        file01.write("\n")
        data2 = json.loads(row)
        r = requests.post(f'{url}/add_worker', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]

print("-------BABY>>>>>")
baby_p_key_list = list(baby_parent_id.keys())
baby_controler = 0
indexs = [i for i in range(1,bed_counter + 1)]
parents = get_all_parents()
with open("SQLbaby.txt", "w") as file01:
    for i in range(person_no - baby_no, person_no):
        
        random_index = random.randint(0,len(indexs)-1)
        bed_id = indexs.pop(random_index)
        # print(insert("add_baby", ("name", "birth_date", "sex", "address", "weight", "height", "bed", "parents"), (person_name[i], person_birth[i], person_sex[i], person_address[i], baby_weight[baby_controler], baby_height[baby_controler],2 ,baby_parent_id[ baby_p_key_list[baby_controler] ] ) ))
        row = (insert("add_baby", ("name", "birth_date", "sex", "address", "weight", "height", "bed", "parents"), (person_name[i], person_birth[i], person_sex[i], person_address[i], baby_weight[baby_controler], baby_height[baby_controler],bed_id ,[random.choice(parents) for i in range(2)] ) ))
        file01.write(row)
        file01.write("\n")
        baby_controler += 1
        data2 = json.loads(row)
        r = requests.post(f'{url}/add_baby', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]
        parent_controler += 1

babies = get_all_babies()
workers = get_all_workers()
with open("SQLtask.txt", "w") as file01:
    for i in range(task_no):
        # print( insert("add_task", ( "worker", "baby", "title", "description", "time_start", "time_end" ), (task_worker_id[i], task_baby_id[i], task_title[i], task_description[i], task_date_start[i], task_date_end[i]  )  ) )
        row = ( insert("add_task", ( "worker", "baby", "title", "description", "time_start", "time_end" ), (random.choice(workers), random.choice(babies), task_title[i], task_description[i], task_date_start[i], task_date_end[i]  )  ) )
        file01.write(row)
        file01.write("\n")
        data2 = json.loads(row)
        r = requests.post(f'{url}/add_task', headers=headers, data = json.dumps(data2))
        print(r.status_code, r.json())
        temp_token = r.json()["token"]
