# Criação das imagens docker necessárias
```bash
$ sudo docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /home/renatogroffe/Desenvolvimento/Portainer/data:/data portainer/portainer     # caso ainda não se possua o portainer
$ docker build -t=middleware .
$ sudo docker run -d --hostname my-rabbit --name rabbit13 -p 8080:15672 -p 5672:5672 -p 25676:25676 rabbitmq:latest
```

# Controlo gráfico do rabbitmq
 - No portainer, aceder ao container do rabbitmq, aceder ao terminal desse container e inserir o seguinte comando:
```bash
$ rabbitmq-plugins enable rabbitmq_management
```
