
# credentials for rabbitmq
USERNAME = "guest"
PASSWORD = "guest"
URL = "rabbitmq-server" #
PORT = 5672
ROUTING_KEY = "task_queue"

# credentials for rest api
REST_URL = "http://ec2-54-92-239-252.compute-1.amazonaws.com:5001/add_data"

# logger file name
LOGGER_FILE = "middleware.log"

# number of messages when the middleware should empty the queue and send all messages saved to the REST API
MAX_NUMBER_MESSAGES = 1000