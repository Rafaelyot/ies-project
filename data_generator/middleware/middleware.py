import pika
import time
import json
import logging
import requests
from datetime import datetime
from threading import Thread
from flask import Flask, request, g as variables

from settings import *


logging.basicConfig(level=logging.INFO, 
                    filename=LOGGER_FILE, 
                    filemode='a', 
                    format='%(name)s - %(levelname)s - %(message)s')


app = Flask(__name__) #create the Flask app
data = []

@app.route('/add_sensors_data', methods=['POST'])
def add_sensors_data():
    # disabling pika and flask log
    logging.getLogger("pika").propagate = False
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)


    logging.info(f" [x] Received {request.data}")

    data = json.loads(request.data) 
    data['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    variables.channel.basic_publish(
        exchange='',
        routing_key=ROUTING_KEY,
        body=json.dumps(data),
    )

    logging.info(f"Number of messages in queue: {variables.my_queue.method.message_count}")
    
    # thread to clear message queue and send all messages to the REST API
    thread = Thread(target=clear_queue, args=(variables.connection, ))
    thread.daemon = True
    thread.start()

    return {
        'status': True, 
        'message': "Sensor info added with success!"
    }


def clear_queue(connection):
    global data
    data = []

    channel = connection.channel()
    my_queue = channel.queue_declare(queue=ROUTING_KEY, durable=True)

    if my_queue.method.message_count > MAX_NUMBER_MESSAGES:
        channel.basic_consume(ROUTING_KEY, on_message, True)
        try:
            channel.start_consuming()
        except:
            channel.stop_consuming()
            send_data_to_server(data)

def on_message(channel, method, properties, message):
    try:
        channel.basic_ack(delivery_tag=method.delivery_tag)
        data.append(json.loads(message))
    except Exception as e:
        logging.error(f"Error: {e}")

def send_data_to_server(data):
    try:
        response = json.loads(
            requests.post(REST_URL, json=data).content
        )

        if response['status']:
            logging.info("Data sent and saved with success on the REST API")
        else:
            logging.error("Error saving data on REST API!")
    except Exception as e:
        logging.error(f"Error sending data to REST API! Error: {e}")

    data = []


@app.before_request
def connect_rabbit():
    credentials = pika.PlainCredentials(USERNAME, PASSWORD)
    variables.connection = pika.BlockingConnection(pika.ConnectionParameters(URL, PORT, '/', credentials))

    variables.channel = variables.connection.channel()
    variables.my_queue = variables.channel.queue_declare(queue=ROUTING_KEY, durable=True)



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
