#!/usr/bin/env python

#sudo apt install rabbitmq-server
#pip3 install pika
#pip3 install rabbitmq
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue')

message = "Hello World!"
channel.basic_publish(
    exchange='',
    routing_key='task_queue',
    body=message,
    )
print(" [x] Sent %r" % message)
connection.close()