# Criação das imagens docker necessárias
```bash
$ sudo docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /home/renatogroffe/Desenvolvimento/Portainer/data:/data portainer/portainer     # caso ainda não se possua o portainer
$ docker build -t=rest_api .
$ sudo docker run mysql
```

# Executar os containers
```bash
$ docker-compose up
```
 - Acesso em localhost
   - A REST API pode ser acedida através do endereço [localhost:8000](localhost:8000)
   - O servidor MySQL pode ser acedido através do endereço [localhost:3306](localhost:3306)

# Executar em localhost sem docker
```bash
$ pipenv shell
$ pipenv install djangorestframework==2.2.5 ; pipenv install django-rest-swagger ; pipenv install requests ; pipenv install django ; pipenv install mysqlclient ; pipenv install python3-docutils; pipenv install pillow; pipenv install django-cors-headers;
$ python manage.py mikemigrations app
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
``` 
