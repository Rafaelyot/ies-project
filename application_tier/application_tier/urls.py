"""application_tier URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from rest_framework_swagger.views import get_swagger_view

from app import views

schema_view = get_swagger_view(title='API')


urlpatterns = [

    url(r'^docs/', schema_view),

    path('admin/', admin.site.urls),
    path('login', views.login, name='login'),

    path('add_baby', views.add_baby, name='add_baby'),
    path('add_worker', views.add_worker, name='add_worker'),
    path('add_parent', views.add_parent, name='add_parent'),
    path('add_bed', views.add_bed, name='add_bed'),
    path('add_room', views.add_room, name='add_room'),
    path('add_task', views.add_task, name='add_task'),

    url(r'^get_baby/(?P<baby_id>\w+)$', views.get_baby, name='get_baby'),
    url(r'^get_babyByName/(?P<name>[\w\s()]+)$', views.get_baby_by_name, name="get_baby_by_name"),
    url(r'^get_parentByName/(?P<name>[\w\s()]+)$', views.get_parent_by_name, name="get_baby_by_name"),
    url(r'^get_babies/(?P<start>\w+)/(?P<end>\w+)$', views.get_babies, name='get_babies'),
    url(r'^get_babyByParent/(?P<parent_id>\w+)$', views.get_baby_by_parent, name='get_baby_by_parent'),
    url(r'^get_workers/(?P<start>\w+)/(?P<end>\w+)$', views.get_workers, name='get_workers'),
    url(r'^get_parents/(?P<start>\w+)/(?P<end>\w+)$', views.get_parents, name='get_parents'),
    url(r'^get_personsByName/(?P<name>[\w\s()]+)$', views.get_persons_by_name, name='get_persons'),
    path('get_rooms', views.get_rooms, name='get_rooms'),
    path('get_available_rooms', views.get_available_rooms, name='get_available_rooms'),
    url(r'^get_tasks_by_baby/(?P<baby_id>\w+)$', views.get_tasks_by_baby, name='get_tasks_by_baby'),
    url(r'^get_worker/(?P<worker_id>\w+)$', views.get_worker, name='get_worker'),
    url(r'^get_parents_by_baby/(?P<baby_id>\w+)$', views.get_parents_by_baby, name='get_parents_by_baby'),
    url(r'^get_parent_by_id/(?P<parent_id>\w+)$', views.get_parent_by_id, name='get_parent_by_id'),

    url(r'^remove_person/(?P<person_id>\w+)$', views.remove_person, name='remove_person'),
    url(r'^remove_room/(?P<room_id>\w+)$', views.remove_room, name='remove_room'),
    url(r'^remove_bed/(?P<bed_id>\w+)$', views.remove_bed, name='remove_bed'),
    url(r'^remove_task/(?P<task_id>\w+)$', views.remove_task, name='remove_task'),

    url(r'^update_baby/(?P<baby_id>\w+)$', views.update_baby, name='update_baby'),
    url(r'^update_worker/(?P<worker_id>\w+)$', views.update_worker, name='update_worker'),
    url(r'^update_parent/(?P<parent_id>\w+)$', views.update_parent, name='update_parent'),
    url(r'^update_room/(?P<room_id>\w+)$', views.update_room, name='update_room'),
    url(r'^update_bed/(?P<bed_id>\w+)$', views.update_bed, name='update_bed'),
    url(r'^update_task/(?P<task_id>\w+)$', views.update_task, name='update_task'),

    # Sensores
    path('sensors_names/', views.sensors_names, name='sensors_names'),
    url(r'^last_values_per_baby/(?P<sensor>\w+)/(?P<bed>\w+)/(?P<n>\w+)/$',
        views.last_values_per_baby,
        name='last_values_per_baby'),
    url(r'^last_values_per_room/(?P<sensor>\w+)/(?P<floor>\w+)/(?P<room>\w+)/(?P<n>\w+)/$',
        views.last_values_per_room,
        name='last_values_per_room'),
    url(r'^all_last_values_per_baby/(?P<bed>\w+)/(?P<n>\w+)/$',
        views.all_last_values_per_baby,
        name='last_values_per_room'),
    url(r'^all_last_values_per_room/(?P<floor>\w+)/(?P<room>\w+)/(?P<n>\w+)/$',
        views.all_last_values_per_room,
        name='all_last_values_per_room'),

    # Help Queries
    url(r'^get_available_beds_by_floor_and_number/(?P<floor>\w+)/(?P<room_number>\w+)$', views.get_available_beds_by_floor_and_number, name='get_available_beds'),
    path('login_status', views.login_status, name='login_status'),
    # To simulate
    path('get_temperature', views.get_temperature, name='add_temperature'),
]
