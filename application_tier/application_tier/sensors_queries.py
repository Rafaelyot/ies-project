from influxdb import InfluxDBClient
from .settings import SENSORS_DATABASE


def connect_db():
    client_db = InfluxDBClient(host=SENSORS_DATABASE['host'], port=SENSORS_DATABASE['port'])

    db_name = SENSORS_DATABASE['name']
    if db_name not in [db['name'] for db in client_db.get_list_database()]:
        client_db.create_database(db_name)

    client_db.switch_database(db_name)

    return client_db


def get_sensors_names():
    try:
        client_db = connect_db()

        return True, [sensor['name'] for sensor in client_db.get_list_measurements()], "Dados obtidos com sucesso"
    except Exception as e:
        print(e)
        return False, None, "Erro na base de dados a obter os nomes dos sensores!"


def do_query(sensor=None, room=None, floor=None, bed=None, limit=None):
    client_db = connect_db()

    bind_params = {}
    where_value = "where "
    limit_value = ""
    if limit:
        if not limit.isdigit():
            return None, "Valor do limite tem de ser um número interiro!"
        limit_value = f"limit {limit}"

    if room and floor and bed:
        return None, "Não se pode obter ao mesmo tempo o valor dum sensor para um quarto e uma cama!"
    if room and floor:
        if not room.isdigit() or not floor.isdigit():
            return None, "Valores do quarto e do andar têm de ser um número interiro!"
        where_value += f"room = $room and floor = $floor"
        bind_params['room'] = int(room)
        bind_params['floor'] = int(floor)
    elif bed:
        if not bed.isdigit():
            return None, "Valor do bebé tem de ser um número interiro!"
        where_value += f"bed = $bed"
        bind_params['bed'] = int(bed)
    else:
        return None, "Argumentos mal especificados"

    result = {}
    sensors = [sensor] if sensor else get_sensors_names()[1]
    for sensor in sensors:
        query = f"select * from {sensor} {where_value} order by time desc {limit_value};"
        result[sensor] = [
            {
                'time': d['time'],
                'value': d['value']
            } for d in client_db.query(query, bind_params=bind_params).get_points()
        ]

    return result, "Valores obtidos com sucesso"


def last_values_per_baby(sensor, bed, n):
    try:
        return do_query(sensor=sensor, bed=bed, limit=n)
    except Exception as e:
        print(e)
        return None, "Erro na base de dados a obter os últimos valores dos sensores para o bebé pedido!"


def last_values_per_room(sensor, floor, room, n):
    try:
        return do_query(sensor=sensor, floor=floor, room=room, limit=n)
    except Exception as e:
        print(e)
        return None, "Erro na base de dados a obter os últimos valores dos sensores para o quarto pedido!"


def all_last_values_per_baby(bed, n):
    try:
        return do_query(bed=bed, limit=n)
    except Exception as e:
        print(e)
        return None, "Erro na base de dados a obter os últimos valores de todos os sensores para o bebé pedido!"


def all_last_values_per_room(floor, room, n):
    try:
        return do_query(floor=floor, room=room, limit=n)
    except Exception as e:
        print(e)
        return None, "Erro na base de dados a obter os últimos valores de todos os sensores para o quarto pedido!"
