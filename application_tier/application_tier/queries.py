from django.db import transaction

from app.models import *
from datetime import datetime
from django.forms.models import model_to_dict
from app.serializers import ParentSerializer, BabySerializer, WorkerSerializer
from django.db.models import Max, Q


def next_id(model):
    max_id = model.objects.aggregate(Max('id'))['id__max']
    if not max_id:
        max_id = 0

    return max_id + 1


def normalize_baby(baby_obj):
    baby = model_to_dict(baby_obj)
    baby['bed'] = baby_obj.bed.id
    baby['parents'] = [p.id for p in baby['parents']]
    return baby


def normalize_task(task_obj):
    task = model_to_dict(task_obj)
    task['worker'] = task_obj.worker.id
    task['baby'] = task_obj.baby.id
    return task


def getTypes(obj):
    if Baby.objects.filter(id=obj.id).exists():
        return BabySerializer, Baby, Baby.objects.get(id=obj.id)
    elif Parent.objects.filter(id=obj.id).exists():
        return ParentSerializer, Parent, Parent.objects.get(id=obj.id)
    else:
        return WorkerSerializer, Worker, Worker.objects.get(id=obj.id)


# ---------------------------------------------------------------Get---------------------------------------------------------------
def get_baby(baby_id):  # OK
    try:
        baby = Baby.objects.get(id=baby_id)
        return True, "Informação do bebe obtida com sucesso", normalize_baby(baby)
    except Baby.DoesNotExist:
        return False, "Bebe nao existe!", None
    except Exception as e:
        print(e)
        return False, "Erro ao obter informaçao do Bebe", None


def get_baby_by_name(baby_name):
    try:
        babies_obj = Baby.objects.filter(name__icontains=baby_name)

        babies = []
        for baby in babies_obj:
            model_dict = model_to_dict(baby)
            model_dict['parents'] = [p.id for p in model_dict['parents']]
            babies.append(model_dict)

        return True, "Informação do bebe obtida com sucesso", babies
    except Exception as e:
        print(e)
        return False, "Erro ao obter informaçao do Bebe", None


def get_baby_by_parent(parent_id):
    try:
        Parent.objects.get(id=parent_id)
        babies_obj = Baby.objects.filter(parents__id=parent_id)

        babies = []
        for baby in babies_obj:
            model_dict = model_to_dict(baby)
            model_dict['parents'] = [p.id for p in model_dict['parents']]
            model_dict['bed'] = baby.bed.id
            babies.append(model_dict)

        return True, "Informação do bebe obtida com sucesso", babies
    except Parent.DoesNotExist:
        return False, "O progenitor referido não existe!"
    except Exception as e:
        print(e)
        return False, "Erro ao obter informaçao do Bebe", None


def get_parent_by_name(baby_name):
    try:
        parents_obj = Parent.objects.filter(name__icontains=baby_name)

        parents = []
        for p in parents_obj:
            parents.append(p)

        return True, "Informações obtidas com sucesso", parents
    except Exception as e:
        print(e)
        return False, "Erro ao obter progenitores pelo nome", None


def get_babies(start, end):
    try:
        babies_ = Baby.objects.order_by("name")
        len_ = babies_.count()

        if end > len_:
            end = len_

        if end == 0:
            babies_obj = babies_[start:]
        else:
            babies_obj = babies_[start:end]

        babies = []
        for baby in babies_obj:
            b = normalize_baby(baby)
            parents = []
            for p in get_parents_by_baby(baby.id)[-1]:
                parents.append(ParentSerializer(p).data)
            b['parents'] = parents
            babies.append(b)

        return True, "Informações dos bebés obtidas com sucesso", babies
    except Exception as e:
        print(e)
        return False, "Erro ao obter as informações dos bebés!", None


def get_workers(start, end):  # Ok
    try:
        workers_ = Worker.objects.order_by("name")

        len_ = workers_.count()

        if end > len_:
            end = len_

        if end == 0:
            workers = workers_[start:]
        else:
            workers = workers_[start:end]

        return True, "Informações dos trabalhadores obtidas com sucesso", workers
    except Exception as e:
        print(e)
        return False, "Erro ao obter as informações dos trabalhadores!", None


def get_rooms():  # Ok
    try:
        rooms = Room.objects.all()
        return True, "Informações dos quartos obtida com sucesso", rooms
    except Exception as e:
        print(e)
        return False, "Erro ao obter todos os quartos!", None


def get_available_rooms():  # Ok
    try:
        rooms = []

        for r in Room.objects.all():
            rb = Bed.objects.filter(room_id=r.id)
            if rb.exists():
                if rb.filter(baby=None).exists():
                    rooms.append({'floor': r.floor, 'room_number': r.room_number, 'id': r.id})

        return True, "Informações dos quartos obtida com sucesso", rooms
    except Exception as e:
        print(e)
        return False, "Erro ao obter todos os quartos!", None


def get_parents(start, end):  # Ok
    try:
        parents_ = Parent.objects.order_by("name")
        len_ = parents_.count()

        if end > len_:
            end = len_

        if end == 0:
            parents = parents_[start:]
        else:
            parents = parents_[start:end]

        return True, "Informações dos progenitores obtida com sucesso", parents
    except Exception as e:
        print(e)
        return False, "Erro ao obter todos os progenitores!", None


def get_parents_by_baby(baby_id):
    try:
        parents = Baby.objects.get(id=baby_id).parents.all()

        return True, "Informações dos progenitores de um bebe obtida com sucesso", parents
    except Exception as e:
        print(e)
        return False, "Erro ao obter os progenitores de um bebe!", None

def get_parent_by_id(parent_id):
    try:
        parent = Parent.objects.get(id=parent_id)
        return True, "Informações do parent pelo id obtidas com sucesso", parent
    except Exception as e:
        print(e)
        return False, "Erro ao obter dados de um parent pelo id", None


def get_tasks_by_baby(baby_id):
    try:
        tasks_obj = Task.objects.filter(baby__id=baby_id)
        tasks = []
        for task in tasks_obj:
            tasks.append(normalize_task(task))

        return True, "Informações das tarefas de um bebe obtida com sucesso", tasks
    except Exception as e:
        print(e)
        return False, "Erro ao obter as tarefas de um bebe!", None


def get_worker(worker_id):
    try:
        worker = Worker.objects.get(id=worker_id)

        return True, "Informações de um trabalhador obtido com sucesso", worker

    except Worker.DoesNotExist:
        return False, "Trabalhador nao existente!", None
    except Exception as e:
        print(e)
        return False, "Erro ao obter um trabalhador!", None


def get_persons_by_name(name):
    try:
        persons_ = Person.objects.filter(name__icontains=name)

        persons = []
        for p in persons_:
            serializer_type, object_type, object_ = getTypes(p)
            normalized_person = model_to_dict(object_)
            normalized_person['serializer'] = serializer_type
            normalized_person['type'] = object_.__class__.__name__

            if normalized_person['type'] == 'Baby':
                parents = []
                for parent in get_parents_by_baby(object_.id)[-1]:
                    parents.append(ParentSerializer(parent).data['id'])
                normalized_person['parents'] = parents

            persons.append(normalized_person)

        return True, "Informações de pessoas obtidas com sucesso", persons

    except Exception as e:
        print(e)
        return False, "Erro ao obter pessoas!", None


# ---------------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------Add---------------------------------------------------------------
def add_baby(data):  # OK
    try:

        is_bed_available = Bed.objects.filter(id=data['bed'], baby=None).exists()
        if not is_bed_available:
            return False, "A cama nao está disponivel!"

        if len(data['parents']) == 0:
            return False, "O bebé não tem pais associados!"

        baby = Baby.objects.create(
            id=next_id(Person),
            name=data["name"],
            birth_date=data["birth_date"],
            sex=data["sex"],
            address=data.get("address", None),
            weight=data["weight"],
            height=data["height"],
            bed=Bed.objects.get(id=data['bed']),
            photo=data.get('photo', None)
        )

        for p in list(set(data['parents'])):
            parent = Parent.objects.get(id=p)
            baby.parents.add(parent)

        return True, "Bebe adicionado com sucesso"
    except Parent.DoesNotExist:
        return False, "Progenitor nao existe!"
    except Bed.DoesNotExist:
        return False, "Cama nao existe!"

    except Exception as e:
        print(e)
        return False, "Erro na base de dados ao adicionar novo bebe"


def add_worker(data):  # Ok
    try:
        Worker.objects.create(
            id=next_id(Person),
            name=data["name"],
            birth_date=data["birth_date"],
            sex=data["sex"],
            address=data.get("address", None),
            role=data["role"],
            phone_number=data["phone_number"]
        )

        return True, "Trabalhador adicionado com sucesso"
    except Exception as e:
        print(e)
        return False, "Erro na base de dados ao adicionar novo trabalhador"


def add_temperature(data, room_id):
    try:
        return True, "Temperature adicionada com sucesso!"
    except Exception as e:
        print(e)
        return False, "Erro ao enviar informação do estado do quarto!"


def add_parent(data):  # Ok
    try:
        Parent.objects.create(
            id=next_id(Person),
            name=data["name"],
            birth_date=data["birth_date"],
            sex=data["sex"],
            address=data.get("address", None),
            phone_number=data['phone_number']
        )
        return True, "Progenitor adicionado com sucesso!"
    except Exception as e:
        print(e)
        return False, "Erro ao adicionar progenitor!"


def add_room(data):  # Ok
    try:

        if Room.objects.filter(floor=data['floor'], room_number=data['room_number']).exists():
            r = Room.objects.filter(floor=data['floor']).order_by("-room_number")
            return False, f"Já existe um quarto com o numero associado nesse piso (ID disponivel para adição  :{r[0].room_number + 1} )"

        Room.objects.create(id=next_id(Room), floor=data['floor'], room_number=data['room_number'])

        return True, "Quarto adicionado com sucesso!"
    except Exception as e:
        print(e)
        return False, "Erro ao adicionar quarto!"


def add_bed(data):  # Ok
    try:
        Bed.objects.create(id=next_id(Bed), room=Room.objects.get(floor=data['floor'], room_number=data['room_number']))
        return True, "Cama adicionada com sucesso!"
    except Room.DoesNotExist:
        return False, "Quarto nao existe!"
    except Exception as e:
        print(e)
        return False, "Erro ao adicionar cama!"


def add_task(data):  # Ok
    try:
        time_start_obj = datetime.strptime(data['time_start'], '%Y-%m-%dT%H:%M:%Sz')
        time_end_obj = None
        time_end_defined = False

        if 'time_end' in data:
            time_end_defined = True
            time_end_obj = datetime.strptime(data['time_end'], '%Y-%m-%dT%H:%M:%Sz')

        if time_end_defined and time_start_obj > time_end_obj:
            return False, "Datas invalidas!"

        Task.objects.create(
            id=next_id(Task),
            worker=Worker.objects.get(id=data['worker']),
            baby=Baby.objects.get(id=data['baby']),
            title=data['title'],
            description=data['description'],
            time_start=data['time_start'],
            time_end=data.get('time_end', None)
        )
        return True, "Tarefa adicionada com sucesso!"

    except Baby.DoesNotExist:
        return False, "Bebe nao existe!"

    except Worker.DoesNotExist:
        return False, "Trabalhador nao existe!"

    except Exception as e:
        print(e)
        return False, "Erro ao adicionar tarefa!"


# ---------------------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------Delete------------------------------------------------------------

def remove_person(id):
    try:
        p = Person.objects.get(id=id)
        babies = Baby.objects.filter(parents__id=id)

        for baby in babies:
            if len(baby.parents.all()) == 1:
                baby.delete()

        p.delete()
        return True, "Sucesso!"
    except Person.DoesNotExist:
        return False, "A pessoa a eliminar nao existe!"
    except Exception as e:
        print(e)
        return False, f"Erro ao eliminar a pessoa com ID  {id}"


def remove_room(room_id):
    try:
        r = Room.objects.get(id=room_id)
        r.delete()

        return True, "Sucesso!"
    except Person.DoesNotExist:
        return False, "O quarto  a eliminar nao existe!"
    except Exception as e:
        print(e)
        return False, f"Erro ao eliminar o quarto com ID  {room_id}"


def remove_bed(bed_id):
    try:
        b = Bed.objects.get(id=bed_id)
        b.delete()

        return True, "Sucesso!"
    except Person.DoesNotExist:
        return False, "A cama a eliminar nao existe!"
    except Exception as e:
        print(e)
        return False, f"Erro ao eliminar a cama com ID  {bed_id}"


def remove_task(task_id):
    try:
        t = Task.objects.get(id=task_id)
        t.delete()

        return True, "Sucesso!"
    except Person.DoesNotExist:
        return False, "A tarefa a eliminar nao existe!"
    except Exception as e:
        print(e)
        return False, f"Erro ao eliminar a tarefa com ID  {task_id}"


# ---------------------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------Update------------------------------------------------------------
def update_baby(data, baby_id):  # OK
    transaction.set_autocommit(False)

    try:
        data = dict([(key, value) for key, value in data.items()])
        baby = Baby.objects.get(id=baby_id)
        baby.__dict__.update(data)
        baby.save()

        transaction.set_autocommit(True)
        return True, "Bebe editado com sucesso!"

    except Baby.DoesNotExist:
        transaction.rollback()
        return False, "Bebe nao existente!"
    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar um bebe!"


def update_worker(data, worker_id):
    transaction.set_autocommit(False)
    try:
        data = dict([(key, value) for key, value in data.items()])
        worker = Worker.objects.get(id=worker_id)
        worker.__dict__.update(data)
        worker.save()

        transaction.set_autocommit(True)
        return True, "Trabalhador editado com sucesso!"

    except Worker.DoesNotExist:
        transaction.rollback()
        return False, "Trabalhador nao existente!"
    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar um trabalhador!"


def update_parent(data, parent_id):
    transaction.set_autocommit(False)
    try:
        data = dict([(key, value) for key, value in data.items()])
        parent = Parent.objects.get(id=parent_id)
        parent.__dict__.update(data)
        parent.save()

        transaction.set_autocommit(True)
        return True, "Progenitor editado com sucesso!"

    except Parent.DoesNotExist:
        transaction.rollback()
        return False, "Progenitor nao existente!"
    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar um progenitor!"


def update_room(data, room_id):
    transaction.set_autocommit(False)
    try:
        data = dict([(key, value) for key, value in data.items()])
        room = Room.objects.get(id=room_id)
        room.__dict__.update(data)
        room.save()

        transaction.set_autocommit(True)
        return True, "Quarto editado com sucesso!"

    except Room.DoesNotExist:
        transaction.rollback()
        return False, "Quarto nao existente!"
    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar um quarto!"


def update_bed(data, bed_id):
    transaction.set_autocommit(False)
    try:
        data = dict([(key, value) for key, value in data.items()])
        bed = Bed.objects.get(id=bed_id)
        bed.__dict__.update(data)
        bed.save()

        transaction.set_autocommit(True)
        return True, "Cama editado com sucesso!"

    except Bed.DoesNotExist:
        transaction.rollback()
        return False, "Cama nao existente!"

    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar um cama!"


def update_task(data, task_id):
    transaction.set_autocommit(False)
    try:
        data = dict([(key, value) for key, value in data.items()])
        task = Task.objects.get(id=task_id)
        task.__dict__.update(data)
        task.save()

        transaction.set_autocommit(True)
        return True, "Tarefa editado com sucesso!"

    except Task.DoesNotExist:
        transaction.rollback()
        return False, "Tarefa nao existente!"

    except Exception as e:
        print(e)
        transaction.rollback()
        return False, "Erro na base de dados ao editar uma tarefa!"
# --------------------------------------------------------------------------------------------------------------------------------
