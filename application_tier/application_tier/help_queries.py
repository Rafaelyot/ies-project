from app.models import *
from django.forms.models import model_to_dict


def get_available_beds_by_floor_and_number(floor, room_number):
    beds_obj = Bed.objects.filter(baby=None, room__floor=floor, room__room_number=room_number).order_by("id")
    beds = []

    for bed in beds_obj:
        model_dict = model_to_dict(bed)
        model_dict['room'] = model_to_dict(Room.objects.get(id=bed.room.id))
        beds.append(model_dict)
    
    return "Camas disponiveis obtidas com sucesso!", beds
