from django.test import TestCase, Client
from django.contrib.auth.models import User
from rest_framework.status import *

client = Client()

class LoginTestCase(TestCase):

    def setUp(self):
        user = User.objects.create(username='rafael')
        user.set_password('ies2019bb')
        user.save()
        self.valid_login = client.post('/login', {'username': 'rafael', 'password': 'ies2019bb'})
        self.invalid_login = client.post('/login', {'username': 'rafaele', 'password': 'ies2019bb'})

    def test_valid_login(self):
        self.assertEqual(self.valid_login.status_code, HTTP_200_OK)

    def test_invalid_login(self):
        self.assertNotEqual(self.invalid_login.status_code, HTTP_200_OK)

