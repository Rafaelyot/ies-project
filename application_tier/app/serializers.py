from rest_framework import serializers


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class PersonSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField()
    birth_date = serializers.DateTimeField()
    sex = serializers.ChoiceField(choices=['F', 'M'])
    address = serializers.CharField(required=False)
    photo = serializers.CharField(required=False)
    type = serializers.CharField(required=False)


class ParentSerializer(PersonSerializer):
    phone_number = serializers.CharField(max_length=200)


class BabySerializer(PersonSerializer):
    weight = serializers.FloatField()
    height = serializers.FloatField()
    bed = serializers.IntegerField()
    parents = serializers.ListField()


class WorkerSerializer(PersonSerializer):
    role = serializers.ChoiceField(choices=['Médico(a)', 'Enfermeiro(a)', "Auxiliar"])
    phone_number = serializers.CharField(max_length=200)


class RoomSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    floor = serializers.IntegerField()
    room_number = serializers.IntegerField()


class BedSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    floor = serializers.IntegerField()
    room_number = serializers.IntegerField()


class TaskSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    worker = serializers.IntegerField()
    baby = serializers.IntegerField()
    title = serializers.CharField(max_length=200)
    description = serializers.CharField(max_length=200)
    time_start = serializers.DateTimeField()
    time_end = serializers.DateTimeField(required=False)
