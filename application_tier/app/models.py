from django.db import models
from django.core.validators import MinValueValidator


class Person(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    birth_date = models.DateTimeField()
    sex = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True, blank=True)
    photo = models.TextField(blank=True, null=True)


class Parent(Person):
    phone_number = models.CharField(max_length=200, unique=True)


class Worker(Person):
    role = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200, unique=True)


class Room(models.Model):
    id = models.IntegerField(primary_key=True)
    floor = models.IntegerField()
    room_number = models.IntegerField()
    """
    temperature = models.FloatField(null=True, blank=True)
    humidity = models.FloatField(null=True, blank=True)
    sound = models.FloatField(null=True, blank=True)
    infrared = models.FloatField(null=True, blank=True)

    """


class Bed(models.Model):
    id = models.IntegerField(primary_key=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    # bed_status = models.ForeignKey(Status, on_delete = models.CASCADE)
    """
    temperature = models.FloatField(null=True, blank=True)
    sound = models.FloatField(null=True, blank=True)
    """


class Baby(Person):
    weight = models.FloatField(validators=[MinValueValidator(0)])
    height = models.FloatField(validators=[MinValueValidator(0)])
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE)
    parents = models.ManyToManyField(Parent, related_name="baby_parents")


class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    worker = models.ForeignKey(Worker, related_name="worker_task", on_delete=models.CASCADE)
    baby = models.ForeignKey(Baby, related_name="worker_task_baby", on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    time_start = models.DateTimeField()
    time_end = models.DateTimeField(null=True, blank=True)
