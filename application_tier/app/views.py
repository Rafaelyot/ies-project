from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import *
from app.serializers import *
from application_tier import queries
from application_tier import sensors_queries
from application_tier import help_queries


def verify_if_admin(user):
    username = user.username

    try:
        user = User.objects.get(username=username)
        if user.is_staff:
            return True
    except Exception as e:
        # when the user doesn't exist
        print(e)
        return False
    return False


def create_response(message, status, token=None, data=None):
    return Response({
        "message": message,
        "data": data,
        "token": token,
    }, status=status)


@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    login_serializer = UserLoginSerializer(data=request.data)
    if not login_serializer.is_valid():
        return create_response("Dados inválidos!", HTTP_404_NOT_FOUND, data=login_serializer.errors)

    user = authenticate(
        username=login_serializer.data['username'],
        password=login_serializer.data['password']
    )

    if not user:
        message = "Login inválido!"
        return create_response(message, HTTP_404_NOT_FOUND)

    # TOKEN STUFF
    token, _ = Token.objects.get_or_create(user=user)

    return create_response("Login feito com sucesso", HTTP_200_OK, token=token.key)


# ---------------------------------------------------------------Get---------------------------------------------------------------
@csrf_exempt
@api_view(["GET"])
def get_baby(request, baby_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, message, baby_object = queries.get_baby(baby_id)

        data = BabySerializer(baby_object).data

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter informação do  bebe!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_babies(request, start, end):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        get_status, message, babies = queries.get_babies(int(start), int(end))

        data = []
        for baby in babies:
            data.append(BabySerializer(baby).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter bebes !", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_workers(request, start, end):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, message, workers = queries.get_workers(int(start), int(end))

        data = []
        for worker in workers:
            data.append(WorkerSerializer(worker).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os trabalhadores!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_baby_by_name(request, name):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, babies = queries.get_baby_by_name(name)

        data = []
        for baby in babies:
            baby_data = BabySerializer(baby).data
            baby_data['id'] = baby['id']
            data.append(baby_data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os bebes!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_baby_by_parent(request, parent_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, message, babies = queries.get_baby_by_parent(parent_id)
        data = [BabySerializer(baby).data for baby in babies]

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os bebes!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_parent_by_name(request, name):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, parents = queries.get_parent_by_name(name)

        data = []
        for p in parents:
            data.append(ParentSerializer(p).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter progenitores!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_parent_by_id(request, parent_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, message, parent = queries.get_parent_by_id(parent_id)
        data = ParentSerializer(parent).data

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter progenitores!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_rooms(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, rooms = queries.get_rooms()

        data = []
        for room in rooms:
            data.append(RoomSerializer(room).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os quartos!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_available_rooms(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, rooms = queries.get_available_rooms()

        data = []
        for room in rooms:
            data.append(RoomSerializer(room).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os quartos!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_parents(request, start, end):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, parents = queries.get_parents(int(start), int(end))

        data = []
        for p in parents:
            data.append(ParentSerializer(p).data)
        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os progenitores!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_parents_by_baby(request, baby_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, parents = queries.get_parents_by_baby(baby_id)

        data = []
        for p in parents:
            data.append(ParentSerializer(p).data)
        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os progenitores de um bebe!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_tasks_by_baby(request, baby_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, tasks = queries.get_tasks_by_baby(baby_id)
        data = []
        for task in tasks:
            data.append(TaskSerializer(task).data)

        print(str(data[0]).replace('\'', '\"'))

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND, data=JSONRenderer().render(data), token=token)

    except Exception as e:
        print(e)
        return create_response("Erro ao obter as tarefas por bebé", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_available_beds_by_floor_and_number(request, floor, room_number):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        message, beds = help_queries.get_available_beds_by_floor_and_number(floor, room_number)

        return create_response(message, HTTP_200_OK, data=JSONRenderer().render(beds), token=token)

    except Exception as e:
        print(e)
        return create_response("Erro ao obter as camas disponíveis", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_worker(request, worker_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:

        get_status, message, worker = queries.get_worker(worker_id)

        data = WorkerSerializer(worker).data if get_status else []

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)

    except Exception as e:
        print(e)
        return create_response("Erro ao obter um trabalhador", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def get_persons_by_name(request, name):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, message, persons = queries.get_persons_by_name(name)

        data = []
        for p in persons:
            serializer = p['serializer']
            data.append(serializer(p).data)

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=JSONRenderer().render(data), token=token)

    except Exception as e:
        print(e)
        return create_response("Erro ao obter pessoas", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def login_status(request):
    try:
        return create_response("Estado do login obtido com sucesso", HTTP_200_OK, data=request.user.is_authenticated)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter o estado do login", HTTP_403_FORBIDDEN)


@csrf_exempt
@api_view(["GET"])
def sensors_names(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        get_status, data, message = sensors_queries.get_sensors_names()

        return create_response(message, HTTP_200_OK if get_status else HTTP_404_NOT_FOUND,
                               data=data, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao obter os nomes dos sensores", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["GET"])
def last_values_per_baby(request, sensor, bed, n):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        data, message = sensors_queries.last_values_per_baby(sensor, bed, n)

        return create_response(message, HTTP_200_OK if data else HTTP_404_NOT_FOUND,
                               data=data, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a obter os últimos valores dos sensores para o bebé pedido",
                               HTTP_403_FORBIDDEN,
                               token=token)


@csrf_exempt
@api_view(["GET"])
def last_values_per_room(request, sensor, floor, room, n):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        data, message = sensors_queries.last_values_per_room(sensor, floor, room, n)

        return create_response(message, HTTP_200_OK if data else HTTP_404_NOT_FOUND,
                               data=data, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a obter os últimos valores dos sensores para o quarto pedido",
                               HTTP_403_FORBIDDEN,
                               token=token)


@csrf_exempt
@api_view(["GET"])
def all_last_values_per_baby(request, bed, n):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        data, message = sensors_queries.all_last_values_per_baby(bed, n)

        return create_response(message, HTTP_200_OK if data else HTTP_404_NOT_FOUND,
                               data=data, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a obter os últimos valores de todos os sensores para o quarto pedido",
                               HTTP_403_FORBIDDEN,
                               token=token)


@csrf_exempt
@api_view(["GET"])
def all_last_values_per_room(request, room, floor, n):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        data, message = sensors_queries.all_last_values_per_room(floor, room, n)

        return create_response(message, HTTP_200_OK if data else HTTP_404_NOT_FOUND,
                               data=data, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a obter os últimos valores de todos os sensores para o quarto pedido",
                               HTTP_403_FORBIDDEN,
                               token=token)


# ---------------------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------Add---------------------------------------------------------------
@csrf_exempt
@api_view(["POST"])
def add_baby(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        # request.data['birth_date'] = dateparse.parse_datetime(request.data['birth_date'])
        baby_serializer = BabySerializer(data=request.data)
        if not baby_serializer.is_valid():
            return create_response("Dados inválidos!", HTTP_400_BAD_REQUEST, token=token, data=baby_serializer.errors)

        add_status, message = queries.add_baby(baby_serializer.data)

        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a adicionar Bebe!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["POST"])
def add_worker(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        worker_serializer = WorkerSerializer(data=request.data)
        if not worker_serializer.is_valid():
            return create_response("Dados inválidos!", HTTP_400_BAD_REQUEST, token=token, data=worker_serializer.errors)

        add_status, message = queries.add_worker(worker_serializer.data)

        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a adicionar trabalhador!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["POST"])
def add_parent(request):
    if not verify_if_admin(request.user):
        return create_response("Login invalido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        parent_serializer = ParentSerializer(data=request.data)
        if not parent_serializer.is_valid():
            return create_response("Dados invalidos!", HTTP_400_BAD_REQUEST, token=token, data=parent_serializer.errors)

        add_status, message = queries.add_parent(parent_serializer.data)
        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro na adição de um progenitor!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["POST"])
def add_room(request):
    if not verify_if_admin(request.user):
        return create_response("Login invalido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        room_serializer = RoomSerializer(data=request.data)
        if not room_serializer.is_valid():
            return create_response("Dados invalidos!", HTTP_400_BAD_REQUEST, token=token, data=room_serializer.errors)

        add_status, message = queries.add_room(room_serializer.data)
        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro na adição de um progenitor!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["POST"])
def add_bed(request):
    if not verify_if_admin(request.user):
        return create_response("Login invalido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        bed_serializer = BedSerializer(data=request.data)
        if not bed_serializer.is_valid():
            return create_response("Dados invalidos!", HTTP_400_BAD_REQUEST, token=token, data=bed_serializer.errors)

        add_status, message = queries.add_bed(bed_serializer.data)
        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro na adição de uma cama!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["POST"])
def add_task(request):
    if not verify_if_admin(request.user):
        return create_response("Login invalido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        task_serializer = TaskSerializer(data=request.data)

        if not task_serializer.is_valid():
            return create_response("Dados invalidos!", HTTP_400_BAD_REQUEST, token=token, data=task_serializer.errors)

        add_status, message = queries.add_task(task_serializer.data)

        return create_response(message, HTTP_200_OK if add_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro na adição de um progenitor!", HTTP_403_FORBIDDEN, token=token)


# ---------------------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------Delete------------------------------------------------------------


@csrf_exempt
@api_view(["DELETE"])
def remove_person(request, person_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        remove_status, message = queries.remove_person(person_id)
        return create_response(message, HTTP_200_OK if remove_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao eliminar a pessoa!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["DELETE"])
def remove_room(request, room_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        remove_status, message = queries.remove_room(room_id)
        return create_response(message, HTTP_200_OK if remove_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao eliminar o quarto!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["DELETE"])
def remove_bed(request, bed_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        remove_status, message = queries.remove_bed(bed_id)
        return create_response(message, HTTP_200_OK if remove_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao eliminar a cama!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["DELETE"])
def remove_task(request, task_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key

    try:
        remove_status, message = queries.remove_task(task_id)
        return create_response(message, HTTP_200_OK if remove_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro ao eliminar a tarefa!", HTTP_403_FORBIDDEN, token=token)


# ---------------------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------Update------------------------------------------------------------


@csrf_exempt
@api_view(["PUT"])
def update_baby(request, baby_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_baby(request.data, baby_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar Bebe!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["PUT"])
def update_worker(request, worker_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_worker(request.data, worker_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar trabalhador!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["PUT"])
def update_parent(request, parent_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_parent(request.data, parent_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar progenitor!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["PUT"])
def update_room(request, room_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_room(request.data, room_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar quarto!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["PUT"])
def update_bed(request, bed_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_bed(request.data, bed_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar cama!", HTTP_403_FORBIDDEN, token=token)


@csrf_exempt
@api_view(["PUT"])
def update_task(request, task_id):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    try:
        update_status, message = queries.update_task(request.data, task_id)

        return create_response(message, HTTP_200_OK if update_status else HTTP_404_NOT_FOUND, token=token)
    except Exception as e:
        print(e)
        return create_response("Erro a editar tarefa!", HTTP_403_FORBIDDEN, token=token)


# --------------------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------Simulation-------------------------------------------------------------------
@csrf_exempt
@api_view(["GET"])
def get_temperature(request):
    if not verify_if_admin(request.user):
        return create_response("Login inválido!", HTTP_401_UNAUTHORIZED)

    token = Token.objects.get(user=request.user).key
    import random
    return create_response("", data=random.uniform(10, 35), status=HTTP_200_OK, token=token)
